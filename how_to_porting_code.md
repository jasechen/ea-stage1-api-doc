## Porting Code - Example TEST env.

### \# Database

1. 新增資料庫 `ea_stage1_main`

        CREATE DATABASE `ea_stage1_main` CHARACTER SET `utf8` COLLATE `utf8_general_ci` 

2. 新增資料庫 `ea_stage1_log`

        CREATE DATABASE `ea_stage1_log` CHARACTER SET `utf8` COLLATE `utf8_general_ci` 


### \# Porting API Code

> + 目標資料夾 => `/data/www/api.english.agency`
> + 欲 git push 自動 deploy 的分支 => `test`
> + API URL => `http://api.stage1.ea.psycomputing.com` 
> + Web URL => `http://stage1.ea.psycomputing.com` 

1. git clone 欲自動 deploy 的分支程式 至 `/data/www/api.english.agency`

        % git clone -b test https://gitlab.com/PsyComputing/EA/stage1-api /data/www/api.english.agency

2. cd 至 `/data/www/api.english.agency`

        % cd /data/www/api.english.agency   

3. 新增 `.env`

        % joe .env
        
        # add
        # 
        APP_NAME=English.Agency
        APP_ENV=local
        APP_KEY={LARAVEL_KEY}
        APP_DEBUG=true
        APP_LOG_LEVEL=debug
        APP_URL={TEST_API_URL}
        WEB_URL={TEST_WEB_URL}
        APP_TIMEZONE=Asia/Taipei
        CROS_DISABLE=false
        
        LOG_CHANNEL=stack
        
        DB_CONNECTION=mysql
        DB_HOST={MAIN_DATABASE_HOST}
        DB_PORT=3306
        DB_DATABASE=ea_stage1_main
        DB_USERNAME={MAIN_DATABASE_USERNAME}
        DB_PASSWORD={MAIN_DATABASE_PASSWORD}
        DB_STRICT_MODE=true
        
        DB_CONNECTION_READ=main-read
        DB_HOST_READ={SLAVE_DATABASE_HOST}
        DB_PORT_READ=3306
        DB_DATABASE_READ=ea_stage1_main
        DB_USERNAME_READ={SLAVE_DATABASE_USERNAME}
        DB_PASSWORD_READ={SLAVE_DATABASE_PASSWORD}
        DB_STRICT_MODE_READ=false
        
        DB_CONNECTION_LOG=log
        DB_HOST_LOG={MAIN_DATABASE_HOST}
        DB_PORT_LOG=3306
        DB_DATABASE_LOG=ea_stage1_log
        DB_USERNAME_LOG={MAIN_DATABASE_USERNAME}
        DB_PASSWORD_LOG={MAIN_DATABASE_PASSWORD}
        DB_STRICT_MODE=true
        
        DB_CONNECTION_LOG_READ=log-read
        DB_HOST_LOG_READ={SLAVE_DATABASE_HOST}
        DB_PORT_LOG_READ=3306
        DB_DATABASE_LOG_READ=ea_stage1_log
        DB_USERNAME_LOG_READ={SLAVE_DATABASE_USERNAME}
        DB_PASSWORD_LOG_READ={SLAVE_DATABASE_PASSWORD}
        DB_STRICT_MODE_READ=false
        
        SESSION_DRIVER=file
        SESSION_LIFETIME=86400
        SESSION_DOMAIN=english_agency
        SESSION_SECURE_COOKIE=false
        
        BROADCAST_DRIVER=log
        CACHE_DRIVER=file
        QUEUE_DRIVER=sync
        
        REDIS_HOST={REDIS_HOST}
        REDIS_PASSWORD=null
        REDIS_PORT=6379
        
        MAIL_DRIVER=smtp
        MAIL_HOST=smtp.gmail.com
        MAIL_PORT=587
        MAIL_USERNAME={GMAIL_USERNAME}
        MAIL_PASSWORD={GMAIL_PASSWORD}
        MAIL_ENCRYPTION=tls
        MAIL_FROM_ADDRESS=service@english.agency
        MAIL_FROM_NAME=English.Agency
        
        PUSHER_APP_ID=
        PUSHER_APP_KEY=
        PUSHER_APP_SECRET=
        PUSHER_APP_CLUSTER=mt1
        
        MIX_PUSHER_APP_KEY=${PUSHER_APP_KEY}
        MIX_PUSHER_APP_CLUSTER=${PUSHER_APP_CLUSTER}
        
        SNOWFLAKE_WORKER_ID=1
        SNOWFLAKE_DATACENTER_ID=1
        
        TWILIO_SID={TWILIO_SID}
        TWILIO_TOKEN={TWILIO_TOKEN}
        TWILIO_FROM={TWILIO_FROM}
        
        AWS_ACCESS_KEY_ID={AWS_ACCESS_KEY_ID}
        AWS_SECRET_ACCESS_KEY={SECRET_ACCESS_KEY}
        AWS_DEFAULT_REGION={AWS_REGION}
        AWS_DEBUG=true
        AWS_BUCKET=ea-stage1-test-gallery
        AWS_BUCKET_URL=https://s3.${AWS_DEFAULT_REGION}.amazonaws.com/${AWS_BUCKET}


4. `composer` 安裝所需程式

        % php composer.phar install
    
5. 利用 `artisan` 產生資料表

        % php artisan migrate

6. 利用 `artisan` 新增預設資料

        % php artisan db:seed

7. edit vendor/twilio/sdk/Services/Twilio/Resource.php

    section 1 =>
    public static function decamelize($word)
    {
        // $callback = create_function(‘$matches’,
        //     ‘return strtolower(strlen(“$matches[1]“) ? “$matches[1]_$matches[2]” : “$matches[2]“);‘);
        
        // return preg_replace_callback(
        //     ‘/(^|[a-z])([A-Z])/‘,
        //     $callback,
        //     $word
        // );
        return preg_replace_callback(‘/(^|[a-z])([A-Z])/‘, function ($matches) {
            return strtolower(strlen($matches[1]) ? $matches[1].$matches[2] : $matches[2]);
        }, $word);
    }

    section 2 =>
    public static function camelize($word) 
    {
        // $callback = create_function(‘$matches’, ‘return strtoupper(“$matches[2]“);‘);
        
        // return preg_replace_callback(’/(^|_)([a-z])/‘,
        //     $callback,
        //     $word);
        return preg_replace_callback(’/(^|_)([a-z])/’, function ($matches) {
            return ucfirst(strtolower($matches[2]));
        }, $word);
    }


    

### \# 備註

+ 自動 deploy 機制，請看 `Deploy code automatically from Repo to server` [![](https://png.icons8.com/external-link/win/16/2980b9)](./Deploy code automatically from Repo to server.md) 