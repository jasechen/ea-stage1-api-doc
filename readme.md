#### \# Something Before Using API

+ **SESSION**
> 1. 使用所有 API，必須在 Request 的 header 傳 `session`，來辨別是哪次傳輸。
> 2. 使用者登入的話，也會紀錄是使用哪個 `session`。
> 3. 目前預設 `session` 存活時間為 86400 秒 (24 hours)，所以當第一次或 `session` 已過期，須使用 `/session/init` 來產生及得到 `session`。

+ **SESSION on MOBILE**
> 1. app (移動裝置) 使用 `/session/init` 時，需傳入 `device_code` 來記錄哪個裝置產生 `session`。
> 2. 之後 AWS SNS 會用到 `device_code` 、 `device_token` 及 `device_channel` 傳訊息至 手機的訊息中心。

+ **TOKEN**
> 1. 所有需要登入後才可使用的 api，需在 Request 的 header 傳 `token` 來識別使用者是否有權限使用該 api。
> 2. 使用 `/login` (登入) 之後，可得到 `token`。


#### \# LOGIN

|  No.  | Route     | Method | Header Params     | Body Params                                                                                                                            | Permission | Description |
|:-----:|-----------|:------:|-------------------|----------------------------------------------------------------------------------------------------------------------------------------|------------|-------------|
|  01.  | /login    |  POST  | session           | account / password                                                                         |            | Login       |


#### \# LOGOUT

|  No.  | Route     | Method | Header Params     | Body Params                                                                                                                            | Permission | Description |
|:-----:|-----------|:------:|-------------------|----------------------------------------------------------------------------------------------------------------------------------------|------------|-------------|
|  01.  | /logout   |  POST  | session / [token] |                                                                                                                                        |            | Logout      |


#### \# POST

| No. | Route                                                              | Method | Header Params | Body Params                                                                                                                                                                                              | Permission | Description                                      |
|:---:|:-------------------------------------------------------------------|:------:|:--------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------|:-------------------------------------------------|
| 01. | /post                     |  POST  | session / token | title / content / cover / [privacy\_status] / [status] / [recommended] / postcategory\_id / owner\_id / slug / excerpt / [canonical\_url] / og\_title / og\_description / meta\_title / meta\_description / cover\_title / cover\_alt |            | Create the post. |
| 02. | /post/{id}                |  PUT   | session / token | id / [title] / [content] / [cover] / [privacy\_status] / [status] / [recommended] / [postcategory\_id] / [owner\_id] |            | Update the post. |
| 03. | /post/{id}                |  DELETE | session / token | id |            | Delete the post. |
| 04. | /post/{id}                                                         |  GET   | session       | id                                                                                                                                                                                                       |            | Find the post by id.                             |
| 05. | /post/list/{status?}/{order\_way?}/{page?}                                      |  GET   | session       | [status] / [order\_way] / [page]                                                                                                                                                                                     |            | Find the post list.                              |
| 06. | /post/slug/{slug}                                                  |  GET   | session       | slug                                                                                                                                                                                                     |            | Find the post by slug.                           |
| 07. | /post/recommended/postcategory/{postcategory\_id}/{status?}/{order\_way?}/{page?} |  GET   | session       | postcategory\_id / [status] / [order\_way] / [page]                                                                                                                                                           |            | Find the recommended post list by postcategory_id. |
| 08. | /post/postcategory/{postcategory\_id}/{status?}/{order\_way?}/{page?} |  GET   | session       | postcategory\_id / [status] / [order\_way] / [page]                                                                                                                                                           |            | Find the post list by postcategory_id. |
| 09. | /post/postcategory/slug/{postcategory\_slug}/{status?}/{order\_way?}/{page?} |  GET   | session       | postcategory\_slug / [status] / [order\_way] / [page]                                                                                                                                                           |            | Find the post list by postcategory_slug. |
| 10. | /post/cover         |  POST   | session / token | owner\_id / [privacy_status] / [status] / file |            | Upload the post cover. |
| 11. | /post/{id}/seo |  PUT   | session / token | id / [slug] / [excerpt] / [canonical\_url] / [og\_title] / [og\_description] / [meta\_title] / [meta\_description] / [cover\_title] / [cover\_alt] |            | Update the post seo. |


#### \# POSTCATEGORY

| No. | Route          | Method | Header Params | Body Params                                                                                                                                                        | Permission | Description              |
|:---:|:---------------|:------:|:--------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------|:-------------------------|
| 01. | /postcategory                     |  POST  | session / token | [type] / name / cover / [status] / slug / excerpt / og\_title / og\_description / meta\_title / meta\_description / cover\_title / cover\_alt / [owner_id] |            | Create the postcategory. |
| 02. | /postcategory/{id}                |  PUT   | session / token | id / [type] / [name] / [cover] / [status] |            | Update the postcategory. |
| 03. | /postcategory/{id}                |  DELETE | session / token | id |            | Delete the postcategory. |
| 04. | /postcategory/{id} |  GET   | session       | id                                                                                                                                                                 |            | Find the postcategory by id. |
| 05. | /postcategory/list/{status?}/{order\_way?}/{page?} |  GET   | session       | [status] / [order\_by] / [page]                                                                                                                                                |            | Find the postcategory list.  |
| 06. | /postcategory/slug/{slug} |  GET   | session       | slug                                                                                                                                                                 |            | Find the postcategory by slug. |
| 07. | /postcategory/type/{type}/{status?}/{order\_way?}/{page?} |  GET   | session       | type / [status] / [order\_by] / [page]                                                                                                                                                |            | Find the postcategory list by type.  |
| 08. | /postcategory/owner/{owner\_id}/{status?}/{order\_way?}/{page?} |  GET   | session       | owner\_id / [status] / [order\_by] / [page]                                                                                                                                                |            | Find the postcategory list by owner_id.  |
| 09. | /postcategory/type/{type}/owner/{owner\_id}/{status?}/{order\_way?}/{page?} |  GET   | session       | type / owner\_id / [status] / [order\_by] / [page]                                                                                                                                                |            | Find the postcategory list by type and owner_id.  |
| 10. | /postcategory/cover         |  POST   | session / token | [owner\_id] / [privacy_status] / [status] / file |            | Upload the postcategory cover. |
| 11. | /postcategory/{id}/seo |  PUT   | session / token | id / [slug] / [excerpt] / [og\_title] / [og\_description] / [meta\_title] / [meta\_description] / [cover\_title] / [cover\_alt] |            | Update the postcategory seo. |


#### \# REGISTER

|  No.  | Route     | Method | Header Params     | Body Params                                                                                              | Permission | Description |
|:-----:|-----------|:------:|-------------------|----------------------------------------------------------------------------------------------------------|------------|-------------|
|  01.  | /register |  POST  | session           | account / password / password\_repeat / first\_name / last\_name / mobile\_country\_code / mobile\_phone |            | Register    |


#### \# ROLE

| No. | Route                                                              | Method | Header Params | Body Params                     | Permission | Description                                      |
|:---:|:-------------------------------------------------------------------|:------:|:--------------|:--------------------------------|:-----------|:-------------------------------------------------|
| 04. | /role/{id}                                                         |  GET   | session       | id                              |            | Find the post by id.                             |
| 05. | /role/list/{order\_way?}/{page?}        |  GET   | session       | [order\_way] / [page]      |            | Find the post list.                              |
| 06. | /role/type/{type}/{order\_way?}/{page?} |  GET   | session       | type / [order\_way] / [page] |            | Find the post list by type.                           |
| 07. | /role/code/{code}/{order\_way?}/{page?} |  GET   | session       | code / [order\_way] / [page] |            | Find the post list by code.                          |
| 08. | /role/type/{type}/code/{code}          |  GET   | session       | type / code                 |            | Find the post by type and code.                          |


#### \# SESSION

| No. | Route           | Method | Header Params | Body Params                                                                                         | Permission | Description       |
|:---:|:----------------|:------:|:--------------|:----------------------------------------------------------------------------------------------------|:-----------|:------------------|
| 01. | /session/init   |  POST  |               | [device\_code] / [device\_os] / [device\_type] / [device\_lang] / [device\_token] / [device\_channel] |            | Init the session. |
| 02. | /session/{code} |  GET   |               | code                                                                                                |            | Find the session. |


#### \# SUBSCRIBE

| No. | Route                  | Method | Header Params | Body Params | Permission | Description           |
|:---:|:-----------------------|:------:|:--------------|:------------|:-----------|:----------------------|
| 01. | /subscribe             |  POST  | session       | name / email       |            | Subscribe the E-Mail. |
| 02. | /subscribe/subscribers |  GET   | session       |             |            | Find the Subscribers. |


#### \# USER

| No. | Route                                                              | Method | Header Params | Body Params                     | Permission | Description                                      |
|:---:|:-------------------------------------------------------------------|:------:|:--------------|:--------------------------------|:-----------|:-------------------------------------------------|
| 01. | /user                     |  POST  | session / token | account / password / password\_repeat / [status] / first\_name / last\_name / mobile\_country\_code / mobile\_phone / slug / excerpt / og\_title / og\_description / meta\_title / meta\_description / avatar\_title / avatar\_alt / [role_ids] |            | Create the user. |
| 02. | /user/{id}                |  PUT   | session / token | id / status |            | Update the user. |
| 03. | /user/{id}                |  DELETE | session / token | id |            | Delete the user. |
| 04. | /user/{id}                |  GET   | session       | id                              |            | Find the post by id.                             |
| 05. | /user/list/{status?}/{order\_way?}/{page?} |  GET   | session       | [status] / [order\_way] / [page] |            | Find the post list.                              |
| 06. | /user/slug/{slug}         |  GET   | session       | slug                            |            | Find the post by slug.                           |
| 07. | /user/token/{token}       |  GET   | session       | token                           |            | Find the post by token.                          |
| 08. | /user/{id}/password       |  PUT   | session / token | id / password / password\_repeat |            | Update the user password. |
| 09. | /user/{id}/profile        |  PUT   | session / token | id / [first\_name] / [last\_name] / [nickname] / [email] / [mobile\_country\_code] / [mobile\_phone] / [birth] / [bio] |            | Update the user profile. |
| 10. | /user/{id}/avatar         |  PUT   | session / token | id / [privacy_status] / [status] / file / \_method=PUT |            | Update the user avatar. |
| 11. | /user/{id}/seo            |  PUT   | session / token | id / [slug] / [excerpt] / [og\_title] / [og\_description] / [meta\_title] / [meta\_description] / [avatar\_title] / [avatar\_alt] |            | Update the user seo. |
| 12. | /user/{id}/role           |  PUT   | session / token | id / role_ids |            | Update the user role. |
| 13. | /user/{id}/email/notify   |  GET   | session         | id                           |            | Notify the verify email address.                          |
| 14. | /user/{id}/email/verify   |  PUT   | session         | id / check_code |            | Verify the user email address. |
| 15. | /user/{id}/mobile/notify  |  GET   | session         | id                           |            | Notify the verify mobile.                          |
| 16. | /user/{id}/mobile/verify  |  PUT   | session         | id / check_code |            | Verify the user mobile. |
| 17. | /user/password/notify     |  POST  | session         | account                      |            | Notify the forgot password email.                          |
| 18. | /user/{id}/password/reset |  PUT   | session         | id / account / password / password\_repeat / check_code |            | Reset the user password. |







