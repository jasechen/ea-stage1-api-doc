define({ "api": [
  {
    "type": "DELETE",
    "url": "/file/{filename}",
    "title": "03. 刪除檔案 by filename",
    "version": "0.1.0",
    "description": "<p>・ 刪除檔案 by filename</p>",
    "name": "DeleteFileDelete",
    "group": "File",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "filename",
            "description": "<p>檔名                                        [slug]             位址</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"filename\" : \"36946497446744064\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.filename",
            "description": "<p>檔名</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"delete success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"filename\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"filename empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"file error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"delete error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/File.03.Delete.php",
    "groupTitle": "File"
  },
  {
    "type": "GET",
    "url": "/filter/{id}",
    "title": "04. 查詢 單篇過濾條件",
    "version": "0.1.0",
    "description": "<p>・ 查詢 單篇過濾條件</p>",
    "name": "GetFilterFind",
    "group": "Filter",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>過濾條件 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.filter",
            "description": "<p>過濾條件</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"filter\": {\n            \"id\": \"13292119993225216\",\n            \"type\": \"school\",\n            \"parent_type\": \"country\",\n            \"parent_id\": \"0\",\n            \"code\": \"ph\",\n            \"creator_id\": \"0\",\n            \"updated_at\": \"2018-04-06 16:18:08\",\n            \"created_at\": \"2018-04-06 16:18:08\",\n            \"child\": [\n                {\n                    \"id\": \"13292120068722688\",\n                    \"type\": \"school\",\n                    \"parent_type\": \"location\",\n                    \"parent_id\": \"13292119993225216\",\n                    \"code\": \"ceb\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-04-06 16:18:08\",\n                    \"created_at\": \"2018-04-06 16:18:08\"\n                },\n                {\n                    \"id\": \"13292120081305600\",\n                    \"type\": \"school\",\n                    \"parent_type\": \"location\",\n                    \"parent_id\": \"13292119993225216\",\n                    \"code\": \"bag\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-04-06 16:18:08\",\n                    \"created_at\": \"2018-04-06 16:18:08\"\n                },\n                {\n                    \"id\": \"13292120089694208\",\n                    \"type\": \"school\",\n                    \"parent_type\": \"location\",\n                    \"parent_id\": \"13292119993225216\",\n                    \"code\": \"crk\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-04-06 16:18:08\",\n                    \"created_at\": \"2018-04-06 16:18:08\"\n                }\n            ]\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Filter.04.Find.php",
    "groupTitle": "Filter"
  },
  {
    "type": "GET",
    "url": "/filter/code/{code}",
    "title": "06. 查詢 單篇過濾條件 By Code",
    "version": "0.1.0",
    "description": "<p>・ 查詢 單篇過濾條件 By Code</p>",
    "name": "GetFilterFindByCode",
    "group": "Filter",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "code",
            "description": "<p>過濾條件 code</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"code\" : \"ph\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.filter",
            "description": "<p>過濾條件</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"filter\": {\n            \"id\": \"13292119993225216\",\n            \"type\": \"school\",\n            \"parent_type\": \"country\",\n            \"parent_id\": \"0\",\n            \"code\": \"ph\",\n            \"creator_id\": \"0\",\n            \"updated_at\": \"2018-04-06 16:18:08\",\n            \"created_at\": \"2018-04-06 16:18:08\",\n            \"child\": [\n                {\n                    \"id\": \"13292120068722688\",\n                    \"type\": \"school\",\n                    \"parent_type\": \"location\",\n                    \"parent_id\": \"13292119993225216\",\n                    \"code\": \"ceb\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-04-06 16:18:08\",\n                    \"created_at\": \"2018-04-06 16:18:08\"\n                },\n                {\n                    \"id\": \"13292120081305600\",\n                    \"type\": \"school\",\n                    \"parent_type\": \"location\",\n                    \"parent_id\": \"13292119993225216\",\n                    \"code\": \"bag\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-04-06 16:18:08\",\n                    \"created_at\": \"2018-04-06 16:18:08\"\n                },\n                {\n                    \"id\": \"13292120089694208\",\n                    \"type\": \"school\",\n                    \"parent_type\": \"location\",\n                    \"parent_id\": \"13292119993225216\",\n                    \"code\": \"crk\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-04-06 16:18:08\",\n                    \"created_at\": \"2018-04-06 16:18:08\"\n                }\n            ]\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"code empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Filter.06.FindByCode.php",
    "groupTitle": "Filter"
  },
  {
    "type": "GET",
    "url": "/filter/school/type/{parent_type}/{order_way?}/{page?}",
    "title": "07. 查詢 所有過濾條件 By parent_type",
    "version": "0.1.0",
    "description": "<p>・ 查詢 所有過濾條件 By parent_type</p>",
    "name": "GetFilterFindByParentType",
    "group": "Filter",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "country",
              "location",
              "school-type",
              "program-type",
              "program",
              "program-ta"
            ],
            "optional": false,
            "field": "parent_type",
            "description": "<p>parent type</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"parent_type\" : \"school-type\",\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.filters",
            "description": "<p>過濾條件s</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"filters\": [\n            {\n                \"id\": \"13292120144220160\",\n                \"type\": \"school\",\n                \"parent_type\": \"school-type\",\n                \"parent_id\": \"0\",\n                \"code\": \"middle\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-04-06 16:18:08\",\n                \"created_at\": \"2018-04-06 16:18:08\"\n            },\n            {\n                \"id\": \"13292120152608768\",\n                \"type\": \"school\",\n                \"parent_type\": \"school-type\",\n                \"parent_id\": \"0\",\n                \"code\": \"senior\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-04-06 16:18:08\",\n                \"created_at\": \"2018-04-06 16:18:08\"\n            },\n            {\n                \"id\": \"13292120160997376\",\n                \"type\": \"school\",\n                \"parent_type\": \"school-type\",\n                \"parent_id\": \"0\",\n                \"code\": \"collage\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-04-06 16:18:08\",\n                \"created_at\": \"2018-04-06 16:18:08\"\n            },\n            {\n                \"id\": \"13292120169385984\",\n                \"type\": \"school\",\n                \"parent_type\": \"school-type\",\n                \"parent_id\": \"0\",\n                \"code\": \"community\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-04-06 16:18:08\",\n                \"created_at\": \"2018-04-06 16:18:08\"\n            },\n            {\n                \"id\": \"13292120177774592\",\n                \"type\": \"school\",\n                \"parent_type\": \"school-type\",\n                \"parent_id\": \"0\",\n                \"code\": \"graduate\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-04-06 16:18:08\",\n                \"created_at\": \"2018-04-06 16:18:08\"\n            },\n            {\n                \"id\": \"13292120181968896\",\n                \"type\": \"school\",\n                \"parent_type\": \"school-type\",\n                \"parent_id\": \"0\",\n                \"code\": \"language\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-04-06 16:18:08\",\n                \"created_at\": \"2018-04-06 16:18:08\"\n            }\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"parent_type empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"parent_type error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Filter.07.FindSchoolByParentType.php",
    "groupTitle": "Filter"
  },
  {
    "type": "GET",
    "url": "/filter/school/type/{parent_type}/code/{code}/{order_way?}/{page?}",
    "title": "09. 查詢 所有過濾條件 By parent_type and code",
    "version": "0.1.0",
    "description": "<p>・ 查詢 所有過濾條件 By parent_type and code</p>",
    "name": "GetFilterFindByParentTypeAndCode",
    "group": "Filter",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "country",
              "location",
              "school-type",
              "program-type",
              "program",
              "program-ta"
            ],
            "optional": false,
            "field": "parent_type",
            "description": "<p>parent type</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "code",
            "description": "<p>code</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"parent_type\" : \"location\",\n    \"code\" : \"ph\",\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.filters",
            "description": "<p>過濾條件s</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"filters\": [\n            {\n                \"id\": \"13292120068722688\",\n                \"type\": \"school\",\n                \"parent_type\": \"location\",\n                \"parent_id\": \"13292119993225216\",\n                \"code\": \"ceb\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-04-06 16:18:08\",\n                \"created_at\": \"2018-04-06 16:18:08\"\n            },\n            {\n                \"id\": \"13292120081305600\",\n                \"type\": \"school\",\n                \"parent_type\": \"location\",\n                \"parent_id\": \"13292119993225216\",\n                \"code\": \"bag\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-04-06 16:18:08\",\n                \"created_at\": \"2018-04-06 16:18:08\"\n            },\n            {\n                \"id\": \"13292120089694208\",\n                \"type\": \"school\",\n                \"parent_type\": \"location\",\n                \"parent_id\": \"13292119993225216\",\n                \"code\": \"crk\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-04-06 16:18:08\",\n                \"created_at\": \"2018-04-06 16:18:08\"\n            }\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"parent_type empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"code empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"parent_type error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"code error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Filter.09.FindSchoolByParentTypeAndCode.php",
    "groupTitle": "Filter"
  },
  {
    "type": "GET",
    "url": "/filter/school/type/{parent_type}/id/{parent_id}/{order_way?}/{page?}",
    "title": "08. 查詢 所有過濾條件 By parent_type and parent_id",
    "version": "0.1.0",
    "description": "<p>・ 查詢 所有過濾條件 By parent_type and parent_id</p>",
    "name": "GetFilterFindByParentTypeAndParentId",
    "group": "Filter",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "country",
              "location",
              "school-type",
              "program-type",
              "program",
              "program-ta"
            ],
            "optional": false,
            "field": "parent_type",
            "description": "<p>parent type</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "parent_id",
            "description": "<p>parent id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"parent_type\" : \"location\",\n    \"parent_id\" : \"13292119993225216\",\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.filters",
            "description": "<p>過濾條件s</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"filters\": [\n            {\n                \"id\": \"13292120068722688\",\n                \"type\": \"school\",\n                \"parent_type\": \"location\",\n                \"parent_id\": \"13292119993225216\",\n                \"code\": \"ceb\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-04-06 16:18:08\",\n                \"created_at\": \"2018-04-06 16:18:08\"\n            },\n            {\n                \"id\": \"13292120081305600\",\n                \"type\": \"school\",\n                \"parent_type\": \"location\",\n                \"parent_id\": \"13292119993225216\",\n                \"code\": \"bag\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-04-06 16:18:08\",\n                \"created_at\": \"2018-04-06 16:18:08\"\n            },\n            {\n                \"id\": \"13292120089694208\",\n                \"type\": \"school\",\n                \"parent_type\": \"location\",\n                \"parent_id\": \"13292119993225216\",\n                \"code\": \"crk\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-04-06 16:18:08\",\n                \"created_at\": \"2018-04-06 16:18:08\"\n            }\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"parent_type empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"parent_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"parent_type error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Filter.08.FindSchoolByParentTypeAndParentId.php",
    "groupTitle": "Filter"
  },
  {
    "type": "GET",
    "url": "/filter/list/{order_way?}/{page?}",
    "title": "05. 查詢 所有過濾條件",
    "version": "0.1.0",
    "description": "<p>・ 查詢 所有過濾條件</p>",
    "name": "GetFilterFindList",
    "group": "Filter",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.filters",
            "description": "<p>過濾條件s</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"filters\": [\n            {\n                \"id\": \"13292119993225216\",\n                \"type\": \"school\",\n                \"parent_type\": \"country\",\n                \"parent_id\": \"0\",\n                \"code\": \"ph\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-04-06 16:18:08\",\n                \"created_at\": \"2018-04-06 16:18:08\",\n                \"child\": [\n                    {\n                        \"id\": \"13292120068722688\",\n                        \"type\": \"school\",\n                        \"parent_type\": \"location\",\n                        \"parent_id\": \"13292119993225216\",\n                        \"code\": \"ceb\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-04-06 16:18:08\",\n                        \"created_at\": \"2018-04-06 16:18:08\"\n                    },\n                    {\n                        \"id\": \"13292120081305600\",\n                        \"type\": \"school\",\n                        \"parent_type\": \"location\",\n                        \"parent_id\": \"13292119993225216\",\n                        \"code\": \"bag\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-04-06 16:18:08\",\n                        \"created_at\": \"2018-04-06 16:18:08\"\n                    },\n                    {\n                        \"id\": \"13292120089694208\",\n                        \"type\": \"school\",\n                        \"parent_type\": \"location\",\n                        \"parent_id\": \"13292119993225216\",\n                        \"code\": \"crk\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-04-06 16:18:08\",\n                        \"created_at\": \"2018-04-06 16:18:08\"\n                    }\n                ]\n            },\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Filter.05.FindList.php",
    "groupTitle": "Filter"
  },
  {
    "type": "POST",
    "url": "/login",
    "title": "01. 登入",
    "version": "0.1.0",
    "description": "<p>・ 使用者登入</p>",
    "name": "PostUserLogin",
    "group": "Login",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "account",
            "description": "<p>使用者帳號</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>密碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"account\"     : \"service@showhi.co\",\n    \"password\"    : \"12345678\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.token",
            "description": "<p>使用者 token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"login success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"token\": \"33a909a7-22d2-5c0e-90c6-833a5e5b30b0\",\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"account empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"password empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"account error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"password' length < 8\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"account error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"password error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"user status error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Login.01.Login.php",
    "groupTitle": "Login"
  },
  {
    "type": "POST",
    "url": "/logout",
    "title": "01. 登出",
    "version": "0.1.0",
    "description": "<p>・ 使用者登出</p>",
    "name": "PostUserLogout",
    "group": "Logout",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": true,
            "field": "token",
            "description": "<p>使用者 token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\" : \"33a909a7-22d2-5c0e-90c6-833a5e5b30b0\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"logout success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Logout.01.Logout.php",
    "groupTitle": "Logout"
  },
  {
    "type": "DELETE",
    "url": "/post/{id}",
    "title": "03. 刪除文章",
    "version": "0.1.0",
    "description": "<p>・ 刪除文章</p>",
    "name": "DeletePostDelete",
    "group": "Post",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>文章 ID                                        [slug]             位址</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"36946497446744064\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.post_id",
            "description": "<p>文章 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"delete success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"post_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"post error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"delete error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Post.03.Delete.php",
    "groupTitle": "Post"
  },
  {
    "type": "GET",
    "url": "/post/{id}",
    "title": "04. 查詢 單篇文章",
    "version": "0.1.0",
    "description": "<p>・ 查詢 單篇文章</p>",
    "name": "GetPostFind",
    "group": "Post",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>文章 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.post",
            "description": "<p>文章</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"post\": {\n            \"id\": \"8220321387778048\",\n            \"title\": \"【英國留遊學】倫敦大學金匠學院\",\n            \"content\": \"<p>倫敦大學在台灣應該是個耳熟能詳的英國大學，畢竟民進黨的黨主席蔡英文女士就是畢業於倫敦大學的其中一個學院 &ndash; 倫敦政治經濟學院 （London School of Economics and Political Science），但是除了這個幾乎家喻戶曉的學院之外，其實聯邦制體系的倫敦大學還有其他專攻不同領域的院校，而我今天要介紹的就是專攻藝文與傳媒的金匠學院(Goldsmiths)。</p>\\n<p>如果你喜歡英國當代藝術，或是你有些許研究的話，你應該會有聽過The Young British Artists（簡稱YBAs）這個名稱，他們是一群崛起於80年代的藝術家們，並且在90年代在藝術界造成了一陣轟動，也深深影響了現在的當代藝術。在這群藝術家中有多位都是畢業於金匠學院的藝術科系，例如，在2007年推出全世界最貴的藝術作品 For the love of God的Damien Hirst跟以諷刺手法來表現兩性觀點的Sarah Lucas，他們都是金匠學院的校友。如果你喜歡時尚的話，你一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，你不知道的是，Vivian Westwood也是畢業於金匠學院的設計科系。另外，金匠學院的大眾傳播學系(Media and Communication)也是國際知名的學校，像韓國的導演也都曾來此就讀。可見金匠學院在藝文界真的是有舉足輕重的地位，也培育出了許多赫赫有名的人才。</p>\\n<p>在當年就是衝著這股濃濃藝文味，於是我選擇到了金匠學院，主修大眾傳播媒體，在就讀的期間也感受到了學校的優點與缺點，在這裡就跟可能有計畫來就讀的你分享一下身為校友的個人的經驗吧！</p>\\n<p>首先，有一件事你一定要先知道，在英國，大學的<strong>就讀時間大部分為三年</strong>，也就是讀完三年沒被當的話就能夠順利畢業，另外學校採用的是<strong>三學期制</strong>，跟在台灣熟悉的二學期制不太一樣，雖然這樣假期似乎看起來會比較多，但是其實課業量反而更繁重，因為相同的課程就變成要在較短的時間內學會，而且每一次的假期也就代表著一次的報告dead line，所以，想玩就必須好好做時間規劃，把時間規劃好，才能夠順利繳交報告跟同時享有假期啊！</p>\\n<p>另外，我覺得倫敦大學的學校，相對於倫敦藝術大學，會顯得比較著重於理論，因為我有認識的朋友是就讀於倫敦藝術大學，他們時常都是忙於繳交作品，個人覺得是比較偏向實作方面，但是在金匠學院他們是採取<strong>理論與實作並重</strong>的方式來教課，也就是說作品量的累積跟實作的經驗比起倫敦藝術大學的同學可能會有些落差，但是在理論上知識的基礎可能會更加深厚。會這樣著重理論，是因為他們認為在知識上有著較深厚的基礎就能夠培養出批判性的思考能力(critical thinking)，創造出來的作品會更有內涵並能利用學到的知識來引導觀者，藉以得到更大的共鳴。</p>\\n<p>在大眾傳播的實作課程中，你可以從八種不同的課程來做選擇，分別為：</p>\\n<p>Journalism</p>\\n<p>Photography</p>\\n<p>Filmmaking</p>\\n<p>Illustration</p>\\n<p>Script and Props</p>\\n<p>Animation</p>\\n<p>Radio</p>\\n<p>Interactive Media</p>\\n<p>在一年級的時候你可以選修四種實作課程，但是到了二年級只能選修兩種，到了三年級就必須專精於其中一項，應該是希望透過嘗試讓學生找到自己有興趣或是有潛能的課程，再加以訓練跟培養。</p>\\n<p>我一年級時嘗試了animation，photography，filmmaking跟illustration，二年級的時候選擇了動畫跟攝影，到三年級就專精於攝影。其中動畫課在一二年級的時候，我們學習到的是stop-motion的製作方式，還有如何製作story board，但是到了三年級會學習到使用After Effect這個軟體來製作動畫。filmmaking其實很有趣，你會跟著一個團隊一起合作，可以嘗試到像採訪陌生人，或是自己尋找演員、找場地拍攝，甚至是製作配樂等經驗。在攝影課我們使用的幾乎都是底片相機，學校都有設備可以出借，但是有規定的出借跟歸還時間。在課堂上你會學到關於攝影的理論跟實際操作器具的體驗，不只是相機的操作，還有燈光的調整，底片的沖洗等等。基本上每學年你都需要製作一個自己的project，主題很自由，老師可能會給你一個大方向，但是你可以自己決定主題去製作。</p>\\n<p>不過，專精於其中一項不代表你只能侷限自己做某一個課程，相反的，學校反而鼓勵學生互相合作、學習，甚至跟不同科系的學生也都能夠互相合作來籌畫活動，推行企劃。但是，這相互間的合作通常是要靠個人去籌劃跟聯繫，學校並不會當中間人或指導人，<strong>主動性的培養</strong>，我覺得這是臺灣跟國外學校的一個最主要的差別吧！老師們，可能會提出你可以籌辦活動的想法，但是做與不做就全權交給你去決定，如果要做了，你有問題都可以主動提出，他們也都會很樂意幫助你，但比較不會主動去督促你的進度。像上必修課也都是一樣的，基本上都是lecture接著上seminar，書籍都是一開學就發放給你，在上課前你必須自己先研讀，到學校講師在講課，講課的內容會以書籍裡的內容為基礎，然後延伸出其他更深更廣的理論，有問題再在seminar上提出討論，老師並不會帶著你唸課文，所以如果你事前預習沒做好，在講師講課的時候可能就已經被搞得一頭霧水了。</p>\\n<p>如果你在大學裡期待的是非常多的實作經驗，還有學校幫你選擇好的實習課程，那金匠大學可能不是屬於你的選擇，但是如果你想把底子學好跟喜歡挑戰非商業性的創作，靠自己去尋找實習機會，金匠大學就會是屬於你的不二選擇喔～</p>\",\n            \"cover\": \"1519267601292148\",\n            \"privacy_status\": \"public\",\n            \"status\": \"publish\",\n            \"recommended\": \"0\",\n            \"blog_id\": \"8217810404773888\",\n            \"postcategory_id\": \"7859246410633216\",\n            \"owner_id\": \"8217810346053632\",\n            \"creator_id\": \"0\",\n            \"updated_at\": \"2018-03-23 16:24:37\",\n            \"created_at\": \"2018-03-23 16:24:37\",\n            \"slug\": \"英國留遊學-倫敦大學金匠學院\",\n            \"excerpt\": \"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。\",\n            \"og_title\": \"【英國留遊學】倫敦大學金匠學院\",\n            \"og_description\": \"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。\",\n            \"meta_title\": \"【英國留遊學】倫敦大學金匠學院\",\n            \"meta_description\": \"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。\",\n            \"cover_title\": \"倫敦大學金匠學院\",\n            \"cover_alt\": \"倫敦大學金匠學院\",\n            \"postcategory\": {\n                \"id\": \"7859246410633216\",\n                \"type\": \"global\",\n                \"name\": \"英國留學專欄\",\n                \"cover\": \"1519872393361401\",\n                \"status\": \"enable\",\n                \"num_items\": \"0\",\n                \"owner_id\": \"0\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-22 16:29:50\",\n                \"created_at\": \"2018-03-22 16:29:50\",\n                \"slug\": \"study-aboard-UK\",\n                \"excerpt\": \"出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。\",\n                \"og_title\": \"《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！\",\n                \"og_description\": \"出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。\",\n                \"meta_title\": \"《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！\",\n                \"meta_description\": \"出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。\",\n                \"cover_title\": \"出國留學\",\n                \"cover_alt\": \"出國留學\"\n            },\n            \"owner\": {\n                \"id\": \"8217810346053632\",\n                \"account\": \"david@english.agency\",\n                \"password\": \"$2y$10$5zcXhEAiHzYwfy5Vs7m5tObjcu3cgbvciFA5Myz4cmWgpP.ISSPWW\",\n                \"status\": \"enable\",\n                \"token\": \"070c01d7-a9c5-5614-b9ac-8f14b53ee5a7\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:14:38\",\n                \"created_at\": \"2018-03-23 16:14:38\",\n                \"slug\": \"david_b\",\n                \"excerpt\": \"曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。\",\n                \"og_title\": \"\\\"David B\\\"\",\n                \"og_description\": \"曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。\",\n                \"meta_title\": \"\\\"David B\\\"\",\n                \"meta_description\": \"曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。\",\n                \"avatar_title\": \"\\\"David B\\\"\",\n                \"avatar_alt\": \"\\\"David B\\\"\"\n            },\n            \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n            \"url_path\": \"/英國留遊學-倫敦大學金匠學院-p8220321387778048\"\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Post.04.Find.php",
    "groupTitle": "Post"
  },
  {
    "type": "GET",
    "url": "/post/postcategory/{postcategory_id}/{status?}/{order_way?}/{page?}",
    "title": "08. 查詢 文章 By PostcategoryId",
    "version": "0.1.0",
    "description": "<p>・ 查詢 文章 By PostcategoryId</p>",
    "name": "GetPostFindByPostcategoryId",
    "group": "Post",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "postcategory_id",
            "description": "<p>文章分類 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "all",
              "draft",
              "publish",
              "delete",
              "disable"
            ],
            "optional": true,
            "field": "status",
            "defaultValue": "publish",
            "description": "<p>文章狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"postcategory_id\" : \"7859246410633216\",\n    \"status\" : \"publish\",\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.posts",
            "description": "<p>文章</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"posts\": [\n            {\n                \"id\": \"8220321387778048\",\n                \"title\": \"【英國留遊學】倫敦大學金匠學院\",\n                \"content\": \"<p>倫敦大學在台灣應該是個耳熟能詳的英國大學，畢竟民進黨的黨主席蔡英文女士就是畢業於倫敦大學的其中一個學院 &ndash; 倫敦政治經濟學院 （London School of Economics and Political Science），但是除了這個幾乎家喻戶曉的學院之外，其實聯邦制體系的倫敦大學還有其他專攻不同領域的院校，而我今天要介紹的就是專攻藝文與傳媒的金匠學院(Goldsmiths)。</p>\\n<p>如果你喜歡英國當代藝術，或是你有些許研究的話，你應該會有聽過The Young British Artists（簡稱YBAs）這個名稱，他們是一群崛起於80年代的藝術家們，並且在90年代在藝術界造成了一陣轟動，也深深影響了現在的當代藝術。在這群藝術家中有多位都是畢業於金匠學院的藝術科系，例如，在2007年推出全世界最貴的藝術作品 For the love of God的Damien Hirst跟以諷刺手法來表現兩性觀點的Sarah Lucas，他們都是金匠學院的校友。如果你喜歡時尚的話，你一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，你不知道的是，Vivian Westwood也是畢業於金匠學院的設計科系。另外，金匠學院的大眾傳播學系(Media and Communication)也是國際知名的學校，像韓國的導演也都曾來此就讀。可見金匠學院在藝文界真的是有舉足輕重的地位，也培育出了許多赫赫有名的人才。</p>\\n<p>在當年就是衝著這股濃濃藝文味，於是我選擇到了金匠學院，主修大眾傳播媒體，在就讀的期間也感受到了學校的優點與缺點，在這裡就跟可能有計畫來就讀的你分享一下身為校友的個人的經驗吧！</p>\\n<p>首先，有一件事你一定要先知道，在英國，大學的<strong>就讀時間大部分為三年</strong>，也就是讀完三年沒被當的話就能夠順利畢業，另外學校採用的是<strong>三學期制</strong>，跟在台灣熟悉的二學期制不太一樣，雖然這樣假期似乎看起來會比較多，但是其實課業量反而更繁重，因為相同的課程就變成要在較短的時間內學會，而且每一次的假期也就代表著一次的報告dead line，所以，想玩就必須好好做時間規劃，把時間規劃好，才能夠順利繳交報告跟同時享有假期啊！</p>\\n<p>另外，我覺得倫敦大學的學校，相對於倫敦藝術大學，會顯得比較著重於理論，因為我有認識的朋友是就讀於倫敦藝術大學，他們時常都是忙於繳交作品，個人覺得是比較偏向實作方面，但是在金匠學院他們是採取<strong>理論與實作並重</strong>的方式來教課，也就是說作品量的累積跟實作的經驗比起倫敦藝術大學的同學可能會有些落差，但是在理論上知識的基礎可能會更加深厚。會這樣著重理論，是因為他們認為在知識上有著較深厚的基礎就能夠培養出批判性的思考能力(critical thinking)，創造出來的作品會更有內涵並能利用學到的知識來引導觀者，藉以得到更大的共鳴。</p>\\n<p>在大眾傳播的實作課程中，你可以從八種不同的課程來做選擇，分別為：</p>\\n<p>Journalism</p>\\n<p>Photography</p>\\n<p>Filmmaking</p>\\n<p>Illustration</p>\\n<p>Script and Props</p>\\n<p>Animation</p>\\n<p>Radio</p>\\n<p>Interactive Media</p>\\n<p>在一年級的時候你可以選修四種實作課程，但是到了二年級只能選修兩種，到了三年級就必須專精於其中一項，應該是希望透過嘗試讓學生找到自己有興趣或是有潛能的課程，再加以訓練跟培養。</p>\\n<p>我一年級時嘗試了animation，photography，filmmaking跟illustration，二年級的時候選擇了動畫跟攝影，到三年級就專精於攝影。其中動畫課在一二年級的時候，我們學習到的是stop-motion的製作方式，還有如何製作story board，但是到了三年級會學習到使用After Effect這個軟體來製作動畫。filmmaking其實很有趣，你會跟著一個團隊一起合作，可以嘗試到像採訪陌生人，或是自己尋找演員、找場地拍攝，甚至是製作配樂等經驗。在攝影課我們使用的幾乎都是底片相機，學校都有設備可以出借，但是有規定的出借跟歸還時間。在課堂上你會學到關於攝影的理論跟實際操作器具的體驗，不只是相機的操作，還有燈光的調整，底片的沖洗等等。基本上每學年你都需要製作一個自己的project，主題很自由，老師可能會給你一個大方向，但是你可以自己決定主題去製作。</p>\\n<p>不過，專精於其中一項不代表你只能侷限自己做某一個課程，相反的，學校反而鼓勵學生互相合作、學習，甚至跟不同科系的學生也都能夠互相合作來籌畫活動，推行企劃。但是，這相互間的合作通常是要靠個人去籌劃跟聯繫，學校並不會當中間人或指導人，<strong>主動性的培養</strong>，我覺得這是臺灣跟國外學校的一個最主要的差別吧！老師們，可能會提出你可以籌辦活動的想法，但是做與不做就全權交給你去決定，如果要做了，你有問題都可以主動提出，他們也都會很樂意幫助你，但比較不會主動去督促你的進度。像上必修課也都是一樣的，基本上都是lecture接著上seminar，書籍都是一開學就發放給你，在上課前你必須自己先研讀，到學校講師在講課，講課的內容會以書籍裡的內容為基礎，然後延伸出其他更深更廣的理論，有問題再在seminar上提出討論，老師並不會帶著你唸課文，所以如果你事前預習沒做好，在講師講課的時候可能就已經被搞得一頭霧水了。</p>\\n<p>如果你在大學裡期待的是非常多的實作經驗，還有學校幫你選擇好的實習課程，那金匠大學可能不是屬於你的選擇，但是如果你想把底子學好跟喜歡挑戰非商業性的創作，靠自己去尋找實習機會，金匠大學就會是屬於你的不二選擇喔～</p>\",\n                \"cover\": \"1519267601292148\",\n                \"privacy_status\": \"public\",\n                \"status\": \"publish\",\n                \"recommended\": \"0\",\n                \"blog_id\": \"8217810404773888\",\n                \"postcategory_id\": \"7859246410633216\",\n                \"owner_id\": \"8217810346053632\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:24:37\",\n                \"created_at\": \"2018-03-23 16:24:37\",\n                \"slug\": \"英國留遊學-倫敦大學金匠學院\",\n                \"excerpt\": \"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。\",\n                \"og_title\": \"【英國留遊學】倫敦大學金匠學院\",\n                \"og_description\": \"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。\",\n                \"meta_title\": \"【英國留遊學】倫敦大學金匠學院\",\n                \"meta_description\": \"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。\",\n                \"cover_title\": \"倫敦大學金匠學院\",\n                \"cover_alt\": \"倫敦大學金匠學院\",\n                \"postcategory\": {\n                    \"id\": \"7859246410633216\",\n                    \"type\": \"global\",\n                    \"name\": \"英國留學專欄\",\n                    \"cover\": \"1519872393361401\",\n                    \"status\": \"enable\",\n                    \"num_items\": \"0\",\n                    \"owner_id\": \"0\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-22 16:29:50\",\n                    \"created_at\": \"2018-03-22 16:29:50\",\n                    \"slug\": \"study-aboard-UK\",\n                    \"excerpt\": \"出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。\",\n                    \"og_title\": \"《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！\",\n                    \"og_description\": \"出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。\",\n                    \"meta_title\": \"《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！\",\n                    \"meta_description\": \"出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。\",\n                    \"cover_title\": \"出國留學\",\n                    \"cover_alt\": \"出國留學\"\n                },\n                \"owner\": {\n                    \"id\": \"8217810346053632\",\n                    \"account\": \"david@english.agency\",\n                    \"password\": \"$2y$10$5zcXhEAiHzYwfy5Vs7m5tObjcu3cgbvciFA5Myz4cmWgpP.ISSPWW\",\n                    \"status\": \"enable\",\n                    \"token\": \"070c01d7-a9c5-5614-b9ac-8f14b53ee5a7\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-23 16:14:38\",\n                    \"created_at\": \"2018-03-23 16:14:38\",\n                    \"slug\": \"david_b\",\n                    \"excerpt\": \"曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。\",\n                    \"og_title\": \"\\\"David B\\\"\",\n                    \"og_description\": \"曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。\",\n                    \"meta_title\": \"\\\"David B\\\"\",\n                    \"meta_description\": \"曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。\",\n                    \"avatar_title\": \"\\\"David B\\\"\",\n                    \"avatar_alt\": \"\\\"David B\\\"\"\n                },\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"/英國留遊學-倫敦大學金匠學院-p8220321387778048\"\n            },\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"postcategory_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Post.08.FindByPostcategoryId.php",
    "groupTitle": "Post"
  },
  {
    "type": "GET",
    "url": "/post/postcategory/slug/{postcategory_slug}/{status?}/{order_way?}/{page?}",
    "title": "09. 查詢 文章 By PostcategorySlug",
    "version": "0.1.0",
    "description": "<p>・ 查詢 文章 By PostcategorySlug</p>",
    "name": "GetPostFindByPostcategorySlug",
    "group": "Post",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "postcategory_slug",
            "description": "<p>文章分類 Slug</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "all",
              "draft",
              "publish",
              "delete",
              "disable"
            ],
            "optional": true,
            "field": "status",
            "defaultValue": "publish",
            "description": "<p>文章狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"postcategory_slug\" : \"study-aboard-canada\",\n    \"status\" : \"publish\",\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.posts",
            "description": "<p>文章</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"posts\": [\n            {\n                \"id\": \"8220321387778048\",\n                \"title\": \"【英國留遊學】倫敦大學金匠學院\",\n                \"content\": \"<p>倫敦大學在台灣應該是個耳熟能詳的英國大學，畢竟民進黨的黨主席蔡英文女士就是畢業於倫敦大學的其中一個學院 &ndash; 倫敦政治經濟學院 （London School of Economics and Political Science），但是除了這個幾乎家喻戶曉的學院之外，其實聯邦制體系的倫敦大學還有其他專攻不同領域的院校，而我今天要介紹的就是專攻藝文與傳媒的金匠學院(Goldsmiths)。</p>\\n<p>如果你喜歡英國當代藝術，或是你有些許研究的話，你應該會有聽過The Young British Artists（簡稱YBAs）這個名稱，他們是一群崛起於80年代的藝術家們，並且在90年代在藝術界造成了一陣轟動，也深深影響了現在的當代藝術。在這群藝術家中有多位都是畢業於金匠學院的藝術科系，例如，在2007年推出全世界最貴的藝術作品 For the love of God的Damien Hirst跟以諷刺手法來表現兩性觀點的Sarah Lucas，他們都是金匠學院的校友。如果你喜歡時尚的話，你一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，你不知道的是，Vivian Westwood也是畢業於金匠學院的設計科系。另外，金匠學院的大眾傳播學系(Media and Communication)也是國際知名的學校，像韓國的導演也都曾來此就讀。可見金匠學院在藝文界真的是有舉足輕重的地位，也培育出了許多赫赫有名的人才。</p>\\n<p>在當年就是衝著這股濃濃藝文味，於是我選擇到了金匠學院，主修大眾傳播媒體，在就讀的期間也感受到了學校的優點與缺點，在這裡就跟可能有計畫來就讀的你分享一下身為校友的個人的經驗吧！</p>\\n<p>首先，有一件事你一定要先知道，在英國，大學的<strong>就讀時間大部分為三年</strong>，也就是讀完三年沒被當的話就能夠順利畢業，另外學校採用的是<strong>三學期制</strong>，跟在台灣熟悉的二學期制不太一樣，雖然這樣假期似乎看起來會比較多，但是其實課業量反而更繁重，因為相同的課程就變成要在較短的時間內學會，而且每一次的假期也就代表著一次的報告dead line，所以，想玩就必須好好做時間規劃，把時間規劃好，才能夠順利繳交報告跟同時享有假期啊！</p>\\n<p>另外，我覺得倫敦大學的學校，相對於倫敦藝術大學，會顯得比較著重於理論，因為我有認識的朋友是就讀於倫敦藝術大學，他們時常都是忙於繳交作品，個人覺得是比較偏向實作方面，但是在金匠學院他們是採取<strong>理論與實作並重</strong>的方式來教課，也就是說作品量的累積跟實作的經驗比起倫敦藝術大學的同學可能會有些落差，但是在理論上知識的基礎可能會更加深厚。會這樣著重理論，是因為他們認為在知識上有著較深厚的基礎就能夠培養出批判性的思考能力(critical thinking)，創造出來的作品會更有內涵並能利用學到的知識來引導觀者，藉以得到更大的共鳴。</p>\\n<p>在大眾傳播的實作課程中，你可以從八種不同的課程來做選擇，分別為：</p>\\n<p>Journalism</p>\\n<p>Photography</p>\\n<p>Filmmaking</p>\\n<p>Illustration</p>\\n<p>Script and Props</p>\\n<p>Animation</p>\\n<p>Radio</p>\\n<p>Interactive Media</p>\\n<p>在一年級的時候你可以選修四種實作課程，但是到了二年級只能選修兩種，到了三年級就必須專精於其中一項，應該是希望透過嘗試讓學生找到自己有興趣或是有潛能的課程，再加以訓練跟培養。</p>\\n<p>我一年級時嘗試了animation，photography，filmmaking跟illustration，二年級的時候選擇了動畫跟攝影，到三年級就專精於攝影。其中動畫課在一二年級的時候，我們學習到的是stop-motion的製作方式，還有如何製作story board，但是到了三年級會學習到使用After Effect這個軟體來製作動畫。filmmaking其實很有趣，你會跟著一個團隊一起合作，可以嘗試到像採訪陌生人，或是自己尋找演員、找場地拍攝，甚至是製作配樂等經驗。在攝影課我們使用的幾乎都是底片相機，學校都有設備可以出借，但是有規定的出借跟歸還時間。在課堂上你會學到關於攝影的理論跟實際操作器具的體驗，不只是相機的操作，還有燈光的調整，底片的沖洗等等。基本上每學年你都需要製作一個自己的project，主題很自由，老師可能會給你一個大方向，但是你可以自己決定主題去製作。</p>\\n<p>不過，專精於其中一項不代表你只能侷限自己做某一個課程，相反的，學校反而鼓勵學生互相合作、學習，甚至跟不同科系的學生也都能夠互相合作來籌畫活動，推行企劃。但是，這相互間的合作通常是要靠個人去籌劃跟聯繫，學校並不會當中間人或指導人，<strong>主動性的培養</strong>，我覺得這是臺灣跟國外學校的一個最主要的差別吧！老師們，可能會提出你可以籌辦活動的想法，但是做與不做就全權交給你去決定，如果要做了，你有問題都可以主動提出，他們也都會很樂意幫助你，但比較不會主動去督促你的進度。像上必修課也都是一樣的，基本上都是lecture接著上seminar，書籍都是一開學就發放給你，在上課前你必須自己先研讀，到學校講師在講課，講課的內容會以書籍裡的內容為基礎，然後延伸出其他更深更廣的理論，有問題再在seminar上提出討論，老師並不會帶著你唸課文，所以如果你事前預習沒做好，在講師講課的時候可能就已經被搞得一頭霧水了。</p>\\n<p>如果你在大學裡期待的是非常多的實作經驗，還有學校幫你選擇好的實習課程，那金匠大學可能不是屬於你的選擇，但是如果你想把底子學好跟喜歡挑戰非商業性的創作，靠自己去尋找實習機會，金匠大學就會是屬於你的不二選擇喔～</p>\",\n                \"cover\": \"1519267601292148\",\n                \"privacy_status\": \"public\",\n                \"status\": \"publish\",\n                \"recommended\": \"0\",\n                \"blog_id\": \"8217810404773888\",\n                \"postcategory_id\": \"7859246410633216\",\n                \"owner_id\": \"8217810346053632\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:24:37\",\n                \"created_at\": \"2018-03-23 16:24:37\",\n                \"slug\": \"英國留遊學-倫敦大學金匠學院\",\n                \"excerpt\": \"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。\",\n                \"og_title\": \"【英國留遊學】倫敦大學金匠學院\",\n                \"og_description\": \"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。\",\n                \"meta_title\": \"【英國留遊學】倫敦大學金匠學院\",\n                \"meta_description\": \"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。\",\n                \"cover_title\": \"倫敦大學金匠學院\",\n                \"cover_alt\": \"倫敦大學金匠學院\",\n                \"postcategory\": {\n                    \"id\": \"7859246410633216\",\n                    \"type\": \"global\",\n                    \"name\": \"英國留學專欄\",\n                    \"cover\": \"1519872393361401\",\n                    \"status\": \"enable\",\n                    \"num_items\": \"0\",\n                    \"owner_id\": \"0\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-22 16:29:50\",\n                    \"created_at\": \"2018-03-22 16:29:50\",\n                    \"slug\": \"study-aboard-UK\",\n                    \"excerpt\": \"出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。\",\n                    \"og_title\": \"《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！\",\n                    \"og_description\": \"出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。\",\n                    \"meta_title\": \"《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！\",\n                    \"meta_description\": \"出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。\",\n                    \"cover_title\": \"出國留學\",\n                    \"cover_alt\": \"出國留學\"\n                },\n                \"owner\": {\n                    \"id\": \"8217810346053632\",\n                    \"account\": \"david@english.agency\",\n                    \"password\": \"$2y$10$5zcXhEAiHzYwfy5Vs7m5tObjcu3cgbvciFA5Myz4cmWgpP.ISSPWW\",\n                    \"status\": \"enable\",\n                    \"token\": \"070c01d7-a9c5-5614-b9ac-8f14b53ee5a7\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-23 16:14:38\",\n                    \"created_at\": \"2018-03-23 16:14:38\",\n                    \"slug\": \"david_b\",\n                    \"excerpt\": \"曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。\",\n                    \"og_title\": \"\\\"David B\\\"\",\n                    \"og_description\": \"曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。\",\n                    \"meta_title\": \"\\\"David B\\\"\",\n                    \"meta_description\": \"曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。\",\n                    \"avatar_title\": \"\\\"David B\\\"\",\n                    \"avatar_alt\": \"\\\"David B\\\"\"\n                },\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"/英國留遊學-倫敦大學金匠學院-p8220321387778048\"\n            },\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"postcategory_slug empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"postcategory_slug error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Post.09.FindByPostcategorySlug.php",
    "groupTitle": "Post"
  },
  {
    "type": "GET",
    "url": "/post/slug/{slug}",
    "title": "06. 查詢 單篇文章 By Slug",
    "version": "0.1.0",
    "description": "<p>・ 查詢 單篇文章 By Slug</p>",
    "name": "GetPostFindBySlug",
    "group": "Post",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "slug",
            "description": "<p>文章 Slug</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"slug\" : \"英國留遊學-倫敦大學金匠學院\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.post",
            "description": "<p>文章</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"post\": {\n            \"id\": \"8220321387778048\",\n            \"title\": \"【英國留遊學】倫敦大學金匠學院\",\n            \"content\": \"<p>倫敦大學在台灣應該是個耳熟能詳的英國大學，畢竟民進黨的黨主席蔡英文女士就是畢業於倫敦大學的其中一個學院 &ndash; 倫敦政治經濟學院 （London School of Economics and Political Science），但是除了這個幾乎家喻戶曉的學院之外，其實聯邦制體系的倫敦大學還有其他專攻不同領域的院校，而我今天要介紹的就是專攻藝文與傳媒的金匠學院(Goldsmiths)。</p>\\n<p>如果你喜歡英國當代藝術，或是你有些許研究的話，你應該會有聽過The Young British Artists（簡稱YBAs）這個名稱，他們是一群崛起於80年代的藝術家們，並且在90年代在藝術界造成了一陣轟動，也深深影響了現在的當代藝術。在這群藝術家中有多位都是畢業於金匠學院的藝術科系，例如，在2007年推出全世界最貴的藝術作品 For the love of God的Damien Hirst跟以諷刺手法來表現兩性觀點的Sarah Lucas，他們都是金匠學院的校友。如果你喜歡時尚的話，你一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，你不知道的是，Vivian Westwood也是畢業於金匠學院的設計科系。另外，金匠學院的大眾傳播學系(Media and Communication)也是國際知名的學校，像韓國的導演也都曾來此就讀。可見金匠學院在藝文界真的是有舉足輕重的地位，也培育出了許多赫赫有名的人才。</p>\\n<p>在當年就是衝著這股濃濃藝文味，於是我選擇到了金匠學院，主修大眾傳播媒體，在就讀的期間也感受到了學校的優點與缺點，在這裡就跟可能有計畫來就讀的你分享一下身為校友的個人的經驗吧！</p>\\n<p>首先，有一件事你一定要先知道，在英國，大學的<strong>就讀時間大部分為三年</strong>，也就是讀完三年沒被當的話就能夠順利畢業，另外學校採用的是<strong>三學期制</strong>，跟在台灣熟悉的二學期制不太一樣，雖然這樣假期似乎看起來會比較多，但是其實課業量反而更繁重，因為相同的課程就變成要在較短的時間內學會，而且每一次的假期也就代表著一次的報告dead line，所以，想玩就必須好好做時間規劃，把時間規劃好，才能夠順利繳交報告跟同時享有假期啊！</p>\\n<p>另外，我覺得倫敦大學的學校，相對於倫敦藝術大學，會顯得比較著重於理論，因為我有認識的朋友是就讀於倫敦藝術大學，他們時常都是忙於繳交作品，個人覺得是比較偏向實作方面，但是在金匠學院他們是採取<strong>理論與實作並重</strong>的方式來教課，也就是說作品量的累積跟實作的經驗比起倫敦藝術大學的同學可能會有些落差，但是在理論上知識的基礎可能會更加深厚。會這樣著重理論，是因為他們認為在知識上有著較深厚的基礎就能夠培養出批判性的思考能力(critical thinking)，創造出來的作品會更有內涵並能利用學到的知識來引導觀者，藉以得到更大的共鳴。</p>\\n<p>在大眾傳播的實作課程中，你可以從八種不同的課程來做選擇，分別為：</p>\\n<p>Journalism</p>\\n<p>Photography</p>\\n<p>Filmmaking</p>\\n<p>Illustration</p>\\n<p>Script and Props</p>\\n<p>Animation</p>\\n<p>Radio</p>\\n<p>Interactive Media</p>\\n<p>在一年級的時候你可以選修四種實作課程，但是到了二年級只能選修兩種，到了三年級就必須專精於其中一項，應該是希望透過嘗試讓學生找到自己有興趣或是有潛能的課程，再加以訓練跟培養。</p>\\n<p>我一年級時嘗試了animation，photography，filmmaking跟illustration，二年級的時候選擇了動畫跟攝影，到三年級就專精於攝影。其中動畫課在一二年級的時候，我們學習到的是stop-motion的製作方式，還有如何製作story board，但是到了三年級會學習到使用After Effect這個軟體來製作動畫。filmmaking其實很有趣，你會跟著一個團隊一起合作，可以嘗試到像採訪陌生人，或是自己尋找演員、找場地拍攝，甚至是製作配樂等經驗。在攝影課我們使用的幾乎都是底片相機，學校都有設備可以出借，但是有規定的出借跟歸還時間。在課堂上你會學到關於攝影的理論跟實際操作器具的體驗，不只是相機的操作，還有燈光的調整，底片的沖洗等等。基本上每學年你都需要製作一個自己的project，主題很自由，老師可能會給你一個大方向，但是你可以自己決定主題去製作。</p>\\n<p>不過，專精於其中一項不代表你只能侷限自己做某一個課程，相反的，學校反而鼓勵學生互相合作、學習，甚至跟不同科系的學生也都能夠互相合作來籌畫活動，推行企劃。但是，這相互間的合作通常是要靠個人去籌劃跟聯繫，學校並不會當中間人或指導人，<strong>主動性的培養</strong>，我覺得這是臺灣跟國外學校的一個最主要的差別吧！老師們，可能會提出你可以籌辦活動的想法，但是做與不做就全權交給你去決定，如果要做了，你有問題都可以主動提出，他們也都會很樂意幫助你，但比較不會主動去督促你的進度。像上必修課也都是一樣的，基本上都是lecture接著上seminar，書籍都是一開學就發放給你，在上課前你必須自己先研讀，到學校講師在講課，講課的內容會以書籍裡的內容為基礎，然後延伸出其他更深更廣的理論，有問題再在seminar上提出討論，老師並不會帶著你唸課文，所以如果你事前預習沒做好，在講師講課的時候可能就已經被搞得一頭霧水了。</p>\\n<p>如果你在大學裡期待的是非常多的實作經驗，還有學校幫你選擇好的實習課程，那金匠大學可能不是屬於你的選擇，但是如果你想把底子學好跟喜歡挑戰非商業性的創作，靠自己去尋找實習機會，金匠大學就會是屬於你的不二選擇喔～</p>\",\n            \"cover\": \"1519267601292148\",\n            \"privacy_status\": \"public\",\n            \"status\": \"publish\",\n            \"recommended\": \"0\",\n            \"blog_id\": \"8217810404773888\",\n            \"postcategory_id\": \"7859246410633216\",\n            \"owner_id\": \"8217810346053632\",\n            \"creator_id\": \"0\",\n            \"updated_at\": \"2018-03-23 16:24:37\",\n            \"created_at\": \"2018-03-23 16:24:37\",\n            \"slug\": \"英國留遊學-倫敦大學金匠學院\",\n            \"excerpt\": \"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。\",\n            \"og_title\": \"【英國留遊學】倫敦大學金匠學院\",\n            \"og_description\": \"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。\",\n            \"meta_title\": \"【英國留遊學】倫敦大學金匠學院\",\n            \"meta_description\": \"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。\",\n            \"cover_title\": \"倫敦大學金匠學院\",\n            \"cover_alt\": \"倫敦大學金匠學院\",\n            \"postcategory\": {\n                \"id\": \"7859246410633216\",\n                \"type\": \"global\",\n                \"name\": \"英國留學專欄\",\n                \"cover\": \"1519872393361401\",\n                \"status\": \"enable\",\n                \"num_items\": \"0\",\n                \"owner_id\": \"0\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-22 16:29:50\",\n                \"created_at\": \"2018-03-22 16:29:50\",\n                \"slug\": \"study-aboard-UK\",\n                \"excerpt\": \"出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。\",\n                \"og_title\": \"《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！\",\n                \"og_description\": \"出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。\",\n                \"meta_title\": \"《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！\",\n                \"meta_description\": \"出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。\",\n                \"cover_title\": \"出國留學\",\n                \"cover_alt\": \"出國留學\"\n            },\n            \"owner\": {\n                \"id\": \"8217810346053632\",\n                \"account\": \"david@english.agency\",\n                \"password\": \"$2y$10$5zcXhEAiHzYwfy5Vs7m5tObjcu3cgbvciFA5Myz4cmWgpP.ISSPWW\",\n                \"status\": \"enable\",\n                \"token\": \"070c01d7-a9c5-5614-b9ac-8f14b53ee5a7\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:14:38\",\n                \"created_at\": \"2018-03-23 16:14:38\",\n                \"slug\": \"david_b\",\n                \"excerpt\": \"曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。\",\n                \"og_title\": \"\\\"David B\\\"\",\n                \"og_description\": \"曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。\",\n                \"meta_title\": \"\\\"David B\\\"\",\n                \"meta_description\": \"曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。\",\n                \"avatar_title\": \"\\\"David B\\\"\",\n                \"avatar_alt\": \"\\\"David B\\\"\"\n            },\n            \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n            \"url_path\": \"/英國留遊學-倫敦大學金匠學院-p8220321387778048\"\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"slug empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Post.06.FindBySlug.php",
    "groupTitle": "Post"
  },
  {
    "type": "GET",
    "url": "/post/list/{status?}/{order_way?}/{page?}",
    "title": "05. 查詢 所有文章",
    "version": "0.1.0",
    "description": "<p>・ 查詢 所有文章</p>",
    "name": "GetPostFindList",
    "group": "Post",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "all",
              "draft",
              "publish",
              "delete",
              "disable"
            ],
            "optional": true,
            "field": "status",
            "defaultValue": "publish",
            "description": "<p>文章狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"status\" : \"publish\",\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.posts",
            "description": "<p>文章</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"posts\": [\n            {\n                \"id\": \"8220321387778048\",\n                \"title\": \"【英國留遊學】倫敦大學金匠學院\",\n                \"content\": \"<p>倫敦大學在台灣應該是個耳熟能詳的英國大學，畢竟民進黨的黨主席蔡英文女士就是畢業於倫敦大學的其中一個學院 &ndash; 倫敦政治經濟學院 （London School of Economics and Political Science），但是除了這個幾乎家喻戶曉的學院之外，其實聯邦制體系的倫敦大學還有其他專攻不同領域的院校，而我今天要介紹的就是專攻藝文與傳媒的金匠學院(Goldsmiths)。</p>\\n<p>如果你喜歡英國當代藝術，或是你有些許研究的話，你應該會有聽過The Young British Artists（簡稱YBAs）這個名稱，他們是一群崛起於80年代的藝術家們，並且在90年代在藝術界造成了一陣轟動，也深深影響了現在的當代藝術。在這群藝術家中有多位都是畢業於金匠學院的藝術科系，例如，在2007年推出全世界最貴的藝術作品 For the love of God的Damien Hirst跟以諷刺手法來表現兩性觀點的Sarah Lucas，他們都是金匠學院的校友。如果你喜歡時尚的話，你一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，你不知道的是，Vivian Westwood也是畢業於金匠學院的設計科系。另外，金匠學院的大眾傳播學系(Media and Communication)也是國際知名的學校，像韓國的導演也都曾來此就讀。可見金匠學院在藝文界真的是有舉足輕重的地位，也培育出了許多赫赫有名的人才。</p>\\n<p>在當年就是衝著這股濃濃藝文味，於是我選擇到了金匠學院，主修大眾傳播媒體，在就讀的期間也感受到了學校的優點與缺點，在這裡就跟可能有計畫來就讀的你分享一下身為校友的個人的經驗吧！</p>\\n<p>首先，有一件事你一定要先知道，在英國，大學的<strong>就讀時間大部分為三年</strong>，也就是讀完三年沒被當的話就能夠順利畢業，另外學校採用的是<strong>三學期制</strong>，跟在台灣熟悉的二學期制不太一樣，雖然這樣假期似乎看起來會比較多，但是其實課業量反而更繁重，因為相同的課程就變成要在較短的時間內學會，而且每一次的假期也就代表著一次的報告dead line，所以，想玩就必須好好做時間規劃，把時間規劃好，才能夠順利繳交報告跟同時享有假期啊！</p>\\n<p>另外，我覺得倫敦大學的學校，相對於倫敦藝術大學，會顯得比較著重於理論，因為我有認識的朋友是就讀於倫敦藝術大學，他們時常都是忙於繳交作品，個人覺得是比較偏向實作方面，但是在金匠學院他們是採取<strong>理論與實作並重</strong>的方式來教課，也就是說作品量的累積跟實作的經驗比起倫敦藝術大學的同學可能會有些落差，但是在理論上知識的基礎可能會更加深厚。會這樣著重理論，是因為他們認為在知識上有著較深厚的基礎就能夠培養出批判性的思考能力(critical thinking)，創造出來的作品會更有內涵並能利用學到的知識來引導觀者，藉以得到更大的共鳴。</p>\\n<p>在大眾傳播的實作課程中，你可以從八種不同的課程來做選擇，分別為：</p>\\n<p>Journalism</p>\\n<p>Photography</p>\\n<p>Filmmaking</p>\\n<p>Illustration</p>\\n<p>Script and Props</p>\\n<p>Animation</p>\\n<p>Radio</p>\\n<p>Interactive Media</p>\\n<p>在一年級的時候你可以選修四種實作課程，但是到了二年級只能選修兩種，到了三年級就必須專精於其中一項，應該是希望透過嘗試讓學生找到自己有興趣或是有潛能的課程，再加以訓練跟培養。</p>\\n<p>我一年級時嘗試了animation，photography，filmmaking跟illustration，二年級的時候選擇了動畫跟攝影，到三年級就專精於攝影。其中動畫課在一二年級的時候，我們學習到的是stop-motion的製作方式，還有如何製作story board，但是到了三年級會學習到使用After Effect這個軟體來製作動畫。filmmaking其實很有趣，你會跟著一個團隊一起合作，可以嘗試到像採訪陌生人，或是自己尋找演員、找場地拍攝，甚至是製作配樂等經驗。在攝影課我們使用的幾乎都是底片相機，學校都有設備可以出借，但是有規定的出借跟歸還時間。在課堂上你會學到關於攝影的理論跟實際操作器具的體驗，不只是相機的操作，還有燈光的調整，底片的沖洗等等。基本上每學年你都需要製作一個自己的project，主題很自由，老師可能會給你一個大方向，但是你可以自己決定主題去製作。</p>\\n<p>不過，專精於其中一項不代表你只能侷限自己做某一個課程，相反的，學校反而鼓勵學生互相合作、學習，甚至跟不同科系的學生也都能夠互相合作來籌畫活動，推行企劃。但是，這相互間的合作通常是要靠個人去籌劃跟聯繫，學校並不會當中間人或指導人，<strong>主動性的培養</strong>，我覺得這是臺灣跟國外學校的一個最主要的差別吧！老師們，可能會提出你可以籌辦活動的想法，但是做與不做就全權交給你去決定，如果要做了，你有問題都可以主動提出，他們也都會很樂意幫助你，但比較不會主動去督促你的進度。像上必修課也都是一樣的，基本上都是lecture接著上seminar，書籍都是一開學就發放給你，在上課前你必須自己先研讀，到學校講師在講課，講課的內容會以書籍裡的內容為基礎，然後延伸出其他更深更廣的理論，有問題再在seminar上提出討論，老師並不會帶著你唸課文，所以如果你事前預習沒做好，在講師講課的時候可能就已經被搞得一頭霧水了。</p>\\n<p>如果你在大學裡期待的是非常多的實作經驗，還有學校幫你選擇好的實習課程，那金匠大學可能不是屬於你的選擇，但是如果你想把底子學好跟喜歡挑戰非商業性的創作，靠自己去尋找實習機會，金匠大學就會是屬於你的不二選擇喔～</p>\",\n                \"cover\": \"1519267601292148\",\n                \"privacy_status\": \"public\",\n                \"status\": \"publish\",\n                \"recommended\": \"0\",\n                \"blog_id\": \"8217810404773888\",\n                \"postcategory_id\": \"7859246410633216\",\n                \"owner_id\": \"8217810346053632\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:24:37\",\n                \"created_at\": \"2018-03-23 16:24:37\",\n                \"slug\": \"英國留遊學-倫敦大學金匠學院\",\n                \"excerpt\": \"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。\",\n                \"og_title\": \"【英國留遊學】倫敦大學金匠學院\",\n                \"og_description\": \"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。\",\n                \"meta_title\": \"【英國留遊學】倫敦大學金匠學院\",\n                \"meta_description\": \"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。\",\n                \"cover_title\": \"倫敦大學金匠學院\",\n                \"cover_alt\": \"倫敦大學金匠學院\",\n                \"postcategory\": {\n                    \"id\": \"7859246410633216\",\n                    \"type\": \"global\",\n                    \"name\": \"英國留學專欄\",\n                    \"cover\": \"1519872393361401\",\n                    \"status\": \"enable\",\n                    \"num_items\": \"0\",\n                    \"owner_id\": \"0\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-22 16:29:50\",\n                    \"created_at\": \"2018-03-22 16:29:50\",\n                    \"slug\": \"study-aboard-UK\",\n                    \"excerpt\": \"出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。\",\n                    \"og_title\": \"《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！\",\n                    \"og_description\": \"出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。\",\n                    \"meta_title\": \"《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！\",\n                    \"meta_description\": \"出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。\",\n                    \"cover_title\": \"出國留學\",\n                    \"cover_alt\": \"出國留學\"\n                },\n                \"owner\": {\n                    \"id\": \"8217810346053632\",\n                    \"account\": \"david@english.agency\",\n                    \"password\": \"$2y$10$5zcXhEAiHzYwfy5Vs7m5tObjcu3cgbvciFA5Myz4cmWgpP.ISSPWW\",\n                    \"status\": \"enable\",\n                    \"token\": \"070c01d7-a9c5-5614-b9ac-8f14b53ee5a7\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-23 16:14:38\",\n                    \"created_at\": \"2018-03-23 16:14:38\",\n                    \"slug\": \"david_b\",\n                    \"excerpt\": \"曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。\",\n                    \"og_title\": \"\\\"David B\\\"\",\n                    \"og_description\": \"曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。\",\n                    \"meta_title\": \"\\\"David B\\\"\",\n                    \"meta_description\": \"曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。\",\n                    \"avatar_title\": \"\\\"David B\\\"\",\n                    \"avatar_alt\": \"\\\"David B\\\"\"\n                },\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"/英國留遊學-倫敦大學金匠學院-p8220321387778048\"\n            },\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Post.05.FindList.php",
    "groupTitle": "Post"
  },
  {
    "type": "GET",
    "url": "/post/recommended/postcategory/{postcategory_id}/{status?}/{order_way?}/{page?}",
    "title": "07. 查詢 被推薦文章 By PostcategoryId",
    "version": "0.1.0",
    "description": "<p>・ 查詢 被推薦文章 By PostcategoryId</p>",
    "name": "GetPostFindRecommendedByPostcategoryId",
    "group": "Post",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "postcategory_id",
            "description": "<p>文章分類 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "all",
              "draft",
              "publish",
              "delete",
              "disable"
            ],
            "optional": true,
            "field": "status",
            "defaultValue": "publish",
            "description": "<p>文章狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"postcategory_id\" : \"7859246410633216\",\n    \"status\" : \"publish\",\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.posts",
            "description": "<p>文章</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"posts\": [\n            {\n                \"id\": \"8220321387778048\",\n                \"title\": \"【英國留遊學】倫敦大學金匠學院\",\n                \"content\": \"<p>倫敦大學在台灣應該是個耳熟能詳的英國大學，畢竟民進黨的黨主席蔡英文女士就是畢業於倫敦大學的其中一個學院 &ndash; 倫敦政治經濟學院 （London School of Economics and Political Science），但是除了這個幾乎家喻戶曉的學院之外，其實聯邦制體系的倫敦大學還有其他專攻不同領域的院校，而我今天要介紹的就是專攻藝文與傳媒的金匠學院(Goldsmiths)。</p>\\n<p>如果你喜歡英國當代藝術，或是你有些許研究的話，你應該會有聽過The Young British Artists（簡稱YBAs）這個名稱，他們是一群崛起於80年代的藝術家們，並且在90年代在藝術界造成了一陣轟動，也深深影響了現在的當代藝術。在這群藝術家中有多位都是畢業於金匠學院的藝術科系，例如，在2007年推出全世界最貴的藝術作品 For the love of God的Damien Hirst跟以諷刺手法來表現兩性觀點的Sarah Lucas，他們都是金匠學院的校友。如果你喜歡時尚的話，你一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，你不知道的是，Vivian Westwood也是畢業於金匠學院的設計科系。另外，金匠學院的大眾傳播學系(Media and Communication)也是國際知名的學校，像韓國的導演也都曾來此就讀。可見金匠學院在藝文界真的是有舉足輕重的地位，也培育出了許多赫赫有名的人才。</p>\\n<p>在當年就是衝著這股濃濃藝文味，於是我選擇到了金匠學院，主修大眾傳播媒體，在就讀的期間也感受到了學校的優點與缺點，在這裡就跟可能有計畫來就讀的你分享一下身為校友的個人的經驗吧！</p>\\n<p>首先，有一件事你一定要先知道，在英國，大學的<strong>就讀時間大部分為三年</strong>，也就是讀完三年沒被當的話就能夠順利畢業，另外學校採用的是<strong>三學期制</strong>，跟在台灣熟悉的二學期制不太一樣，雖然這樣假期似乎看起來會比較多，但是其實課業量反而更繁重，因為相同的課程就變成要在較短的時間內學會，而且每一次的假期也就代表著一次的報告dead line，所以，想玩就必須好好做時間規劃，把時間規劃好，才能夠順利繳交報告跟同時享有假期啊！</p>\\n<p>另外，我覺得倫敦大學的學校，相對於倫敦藝術大學，會顯得比較著重於理論，因為我有認識的朋友是就讀於倫敦藝術大學，他們時常都是忙於繳交作品，個人覺得是比較偏向實作方面，但是在金匠學院他們是採取<strong>理論與實作並重</strong>的方式來教課，也就是說作品量的累積跟實作的經驗比起倫敦藝術大學的同學可能會有些落差，但是在理論上知識的基礎可能會更加深厚。會這樣著重理論，是因為他們認為在知識上有著較深厚的基礎就能夠培養出批判性的思考能力(critical thinking)，創造出來的作品會更有內涵並能利用學到的知識來引導觀者，藉以得到更大的共鳴。</p>\\n<p>在大眾傳播的實作課程中，你可以從八種不同的課程來做選擇，分別為：</p>\\n<p>Journalism</p>\\n<p>Photography</p>\\n<p>Filmmaking</p>\\n<p>Illustration</p>\\n<p>Script and Props</p>\\n<p>Animation</p>\\n<p>Radio</p>\\n<p>Interactive Media</p>\\n<p>在一年級的時候你可以選修四種實作課程，但是到了二年級只能選修兩種，到了三年級就必須專精於其中一項，應該是希望透過嘗試讓學生找到自己有興趣或是有潛能的課程，再加以訓練跟培養。</p>\\n<p>我一年級時嘗試了animation，photography，filmmaking跟illustration，二年級的時候選擇了動畫跟攝影，到三年級就專精於攝影。其中動畫課在一二年級的時候，我們學習到的是stop-motion的製作方式，還有如何製作story board，但是到了三年級會學習到使用After Effect這個軟體來製作動畫。filmmaking其實很有趣，你會跟著一個團隊一起合作，可以嘗試到像採訪陌生人，或是自己尋找演員、找場地拍攝，甚至是製作配樂等經驗。在攝影課我們使用的幾乎都是底片相機，學校都有設備可以出借，但是有規定的出借跟歸還時間。在課堂上你會學到關於攝影的理論跟實際操作器具的體驗，不只是相機的操作，還有燈光的調整，底片的沖洗等等。基本上每學年你都需要製作一個自己的project，主題很自由，老師可能會給你一個大方向，但是你可以自己決定主題去製作。</p>\\n<p>不過，專精於其中一項不代表你只能侷限自己做某一個課程，相反的，學校反而鼓勵學生互相合作、學習，甚至跟不同科系的學生也都能夠互相合作來籌畫活動，推行企劃。但是，這相互間的合作通常是要靠個人去籌劃跟聯繫，學校並不會當中間人或指導人，<strong>主動性的培養</strong>，我覺得這是臺灣跟國外學校的一個最主要的差別吧！老師們，可能會提出你可以籌辦活動的想法，但是做與不做就全權交給你去決定，如果要做了，你有問題都可以主動提出，他們也都會很樂意幫助你，但比較不會主動去督促你的進度。像上必修課也都是一樣的，基本上都是lecture接著上seminar，書籍都是一開學就發放給你，在上課前你必須自己先研讀，到學校講師在講課，講課的內容會以書籍裡的內容為基礎，然後延伸出其他更深更廣的理論，有問題再在seminar上提出討論，老師並不會帶著你唸課文，所以如果你事前預習沒做好，在講師講課的時候可能就已經被搞得一頭霧水了。</p>\\n<p>如果你在大學裡期待的是非常多的實作經驗，還有學校幫你選擇好的實習課程，那金匠大學可能不是屬於你的選擇，但是如果你想把底子學好跟喜歡挑戰非商業性的創作，靠自己去尋找實習機會，金匠大學就會是屬於你的不二選擇喔～</p>\",\n                \"cover\": \"1519267601292148\",\n                \"privacy_status\": \"public\",\n                \"status\": \"publish\",\n                \"recommended\": \"0\",\n                \"blog_id\": \"8217810404773888\",\n                \"postcategory_id\": \"7859246410633216\",\n                \"owner_id\": \"8217810346053632\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:24:37\",\n                \"created_at\": \"2018-03-23 16:24:37\",\n                \"slug\": \"英國留遊學-倫敦大學金匠學院\",\n                \"excerpt\": \"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。\",\n                \"og_title\": \"【英國留遊學】倫敦大學金匠學院\",\n                \"og_description\": \"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。\",\n                \"meta_title\": \"【英國留遊學】倫敦大學金匠學院\",\n                \"meta_description\": \"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。\",\n                \"cover_title\": \"倫敦大學金匠學院\",\n                \"cover_alt\": \"倫敦大學金匠學院\",\n                \"postcategory\": {\n                    \"id\": \"7859246410633216\",\n                    \"type\": \"global\",\n                    \"name\": \"英國留學專欄\",\n                    \"cover\": \"1519872393361401\",\n                    \"status\": \"enable\",\n                    \"num_items\": \"0\",\n                    \"owner_id\": \"0\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-22 16:29:50\",\n                    \"created_at\": \"2018-03-22 16:29:50\",\n                    \"slug\": \"study-aboard-UK\",\n                    \"excerpt\": \"出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。\",\n                    \"og_title\": \"《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！\",\n                    \"og_description\": \"出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。\",\n                    \"meta_title\": \"《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！\",\n                    \"meta_description\": \"出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。\",\n                    \"cover_title\": \"出國留學\",\n                    \"cover_alt\": \"出國留學\"\n                },\n                \"owner\": {\n                    \"id\": \"8217810346053632\",\n                    \"account\": \"david@english.agency\",\n                    \"password\": \"$2y$10$5zcXhEAiHzYwfy5Vs7m5tObjcu3cgbvciFA5Myz4cmWgpP.ISSPWW\",\n                    \"status\": \"enable\",\n                    \"token\": \"070c01d7-a9c5-5614-b9ac-8f14b53ee5a7\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-23 16:14:38\",\n                    \"created_at\": \"2018-03-23 16:14:38\",\n                    \"slug\": \"david_b\",\n                    \"excerpt\": \"曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。\",\n                    \"og_title\": \"\\\"David B\\\"\",\n                    \"og_description\": \"曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。\",\n                    \"meta_title\": \"\\\"David B\\\"\",\n                    \"meta_description\": \"曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。\",\n                    \"avatar_title\": \"\\\"David B\\\"\",\n                    \"avatar_alt\": \"\\\"David B\\\"\"\n                },\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"/英國留遊學-倫敦大學金匠學院-p8220321387778048\"\n            },\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"postcategory_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Post.07.FindRecommendedByPostcategoryId.php",
    "groupTitle": "Post"
  },
  {
    "type": "POST",
    "url": "/post",
    "title": "01. 新增文章",
    "version": "0.1.0",
    "description": "<p>・ 新增文章</p>",
    "name": "PostPostCreate",
    "group": "Post",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "title",
            "description": "<p>標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>內容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "cover",
            "description": "<p>封面</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "public",
              "private"
            ],
            "optional": true,
            "field": "privacy_status",
            "defaultValue": "public",
            "description": "<p>隱私狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "draft",
              "publish",
              "delete",
              "disable"
            ],
            "optional": true,
            "field": "status",
            "defaultValue": "draft",
            "description": "<p>狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "false",
              "true"
            ],
            "optional": true,
            "field": "recommended",
            "defaultValue": "false",
            "description": "<p>被推薦</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "postcategory_id",
            "description": "<p>文章分類 id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "owner_id",
            "description": "<p>擁有者 id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "slug",
            "description": "<p>位址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "excerpt",
            "description": "<p>摘要</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "canonical_url",
            "description": "<p>來源位址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "og_title",
            "description": "<p>og 標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "og_description",
            "description": "<p>og 描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "meta_title",
            "description": "<p>meta 標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "meta_description",
            "description": "<p>meta 描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "cover_title",
            "description": "<p>封面標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "cover_alt",
            "description": "<p>封面 alt</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"title\": \"【英國留遊學】倫敦大學金匠學院\",\n    \"content\": \"<p>倫敦大學在台灣應該是個耳熟能詳的英國大學，畢竟民進黨的黨主席蔡英文女士就是畢業於倫敦大學的其中一個學院 &ndash; 倫敦政治經濟學院 （London School of Economics and Political Science），但是除了這個幾乎家喻戶曉的學院之外，其實聯邦制體系的倫敦大學還有其他專攻不同領域的院校，而我今天要介紹的就是專攻藝文與傳媒的金匠學院(Goldsmiths)。</p>\\n<p>如果你喜歡英國當代藝術，或是你有些許研究的話，你應該會有聽過The Young British Artists（簡稱YBAs）這個名稱，他們是一群崛起於80年代的藝術家們，並且在90年代在藝術界造成了一陣轟動，也深深影響了現在的當代藝術。在這群藝術家中有多位都是畢業於金匠學院的藝術科系，例如，在2007年推出全世界最貴的藝術作品 For the love of God的Damien Hirst跟以諷刺手法來表現兩性觀點的Sarah Lucas，他們都是金匠學院的校友。如果你喜歡時尚的話，你一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，你不知道的是，Vivian Westwood也是畢業於金匠學院的設計科系。另外，金匠學院的大眾傳播學系(Media and Communication)也是國際知名的學校，像韓國的導演也都曾來此就讀。可見金匠學院在藝文界真的是有舉足輕重的地位，也培育出了許多赫赫有名的人才。</p>\\n<p>在當年就是衝著這股濃濃藝文味，於是我選擇到了金匠學院，主修大眾傳播媒體，在就讀的期間也感受到了學校的優點與缺點，在這裡就跟可能有計畫來就讀的你分享一下身為校友的個人的經驗吧！</p>\\n<p>首先，有一件事你一定要先知道，在英國，大學的<strong>就讀時間大部分為三年</strong>，也就是讀完三年沒被當的話就能夠順利畢業，另外學校採用的是<strong>三學期制</strong>，跟在台灣熟悉的二學期制不太一樣，雖然這樣假期似乎看起來會比較多，但是其實課業量反而更繁重，因為相同的課程就變成要在較短的時間內學會，而且每一次的假期也就代表著一次的報告dead line，所以，想玩就必須好好做時間規劃，把時間規劃好，才能夠順利繳交報告跟同時享有假期啊！</p>\\n<p>另外，我覺得倫敦大學的學校，相對於倫敦藝術大學，會顯得比較著重於理論，因為我有認識的朋友是就讀於倫敦藝術大學，他們時常都是忙於繳交作品，個人覺得是比較偏向實作方面，但是在金匠學院他們是採取<strong>理論與實作並重</strong>的方式來教課，也就是說作品量的累積跟實作的經驗比起倫敦藝術大學的同學可能會有些落差，但是在理論上知識的基礎可能會更加深厚。會這樣著重理論，是因為他們認為在知識上有著較深厚的基礎就能夠培養出批判性的思考能力(critical thinking)，創造出來的作品會更有內涵並能利用學到的知識來引導觀者，藉以得到更大的共鳴。</p>\\n<p>在大眾傳播的實作課程中，你可以從八種不同的課程來做選擇，分別為：</p>\\n<p>Journalism</p>\\n<p>Photography</p>\\n<p>Filmmaking</p>\\n<p>Illustration</p>\\n<p>Script and Props</p>\\n<p>Animation</p>\\n<p>Radio</p>\\n<p>Interactive Media</p>\\n<p>在一年級的時候你可以選修四種實作課程，但是到了二年級只能選修兩種，到了三年級就必須專精於其中一項，應該是希望透過嘗試讓學生找到自己有興趣或是有潛能的課程，再加以訓練跟培養。</p>\\n<p>我一年級時嘗試了animation，photography，filmmaking跟illustration，二年級的時候選擇了動畫跟攝影，到三年級就專精於攝影。其中動畫課在一二年級的時候，我們學習到的是stop-motion的製作方式，還有如何製作story board，但是到了三年級會學習到使用After Effect這個軟體來製作動畫。filmmaking其實很有趣，你會跟著一個團隊一起合作，可以嘗試到像採訪陌生人，或是自己尋找演員、找場地拍攝，甚至是製作配樂等經驗。在攝影課我們使用的幾乎都是底片相機，學校都有設備可以出借，但是有規定的出借跟歸還時間。在課堂上你會學到關於攝影的理論跟實際操作器具的體驗，不只是相機的操作，還有燈光的調整，底片的沖洗等等。基本上每學年你都需要製作一個自己的project，主題很自由，老師可能會給你一個大方向，但是你可以自己決定主題去製作。</p>\\n<p>不過，專精於其中一項不代表你只能侷限自己做某一個課程，相反的，學校反而鼓勵學生互相合作、學習，甚至跟不同科系的學生也都能夠互相合作來籌畫活動，推行企劃。但是，這相互間的合作通常是要靠個人去籌劃跟聯繫，學校並不會當中間人或指導人，<strong>主動性的培養</strong>，我覺得這是臺灣跟國外學校的一個最主要的差別吧！老師們，可能會提出你可以籌辦活動的想法，但是做與不做就全權交給你去決定，如果要做了，你有問題都可以主動提出，他們也都會很樂意幫助你，但比較不會主動去督促你的進度。像上必修課也都是一樣的，基本上都是lecture接著上seminar，書籍都是一開學就發放給你，在上課前你必須自己先研讀，到學校講師在講課，講課的內容會以書籍裡的內容為基礎，然後延伸出其他更深更廣的理論，有問題再在seminar上提出討論，老師並不會帶著你唸課文，所以如果你事前預習沒做好，在講師講課的時候可能就已經被搞得一頭霧水了。</p>\\n<p>如果你在大學裡期待的是非常多的實作經驗，還有學校幫你選擇好的實習課程，那金匠大學可能不是屬於你的選擇，但是如果你想把底子學好跟喜歡挑戰非商業性的創作，靠自己去尋找實習機會，金匠大學就會是屬於你的不二選擇喔～</p>\",\n    \"cover\": \"1519267601292148\",\n    \"privacy_status\": \"public\",\n    \"status\": \"publish\",\n    \"recommended\": \"false\",\n    \"postcategory_id\": \"7859246410633216\",\n    \"owner_id\": \"8217810346053632\",\n    \"slug\" : \"test1\",\n    \"excerpt\" : \"test1test1test1test1test1\",\n    \"canonical_url\" : \"http://abc.com/ask-poi\",\n    \"og_title\" : \"標題一\",\n    \"og_description\" : \"test1test1test1test1test1\",\n    \"meta_title\" : \"標題一\",\n    \"meta_description\" : \"test1test1test1test1test1\",\n    \"cover_title\" : \"標題一\",\n    \"cover_alt\" : \"標題一\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.post_id",
            "description": "<p>文章 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"create success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"post_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"title empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"content empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.05",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"cover empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.06",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"postcategory_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.07",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"owner_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.08",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"slug empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.09",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"excerpt empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.10",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"og_title empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.11",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"og_decription empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.12",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"meta_title empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.13",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"meta_description empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.14",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"cover_title empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.15",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"cover_alt empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"privacy_status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"recommended error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"owner error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"postcategory error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 409.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 409,\n    \"comment\": \"slug error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"cover error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"create error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Post.01.Create.php",
    "groupTitle": "Post"
  },
  {
    "type": "POST",
    "url": "/post/cover",
    "title": "10. 上傳封面檔案",
    "version": "0.1.0",
    "description": "<p>・ 上傳封面檔案</p>",
    "name": "PostPostUploadCover",
    "group": "Post",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "owner_id",
            "description": "<p>擁有者</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "public",
              "private"
            ],
            "optional": true,
            "field": "privacy_status",
            "defaultValue": "public",
            "description": "<p>隱私狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "enable",
              "disable",
              "delete"
            ],
            "optional": true,
            "field": "status",
            "defaultValue": "disable",
            "description": "<p>狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "file",
            "optional": false,
            "field": "file",
            "description": "<p>檔案</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"owner_id\" : \"1\",\n    \"privacy_status\" : \"public\",\n    \"status\" : \"disable\",\n    \"file\" : \"\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.filename",
            "description": "<p>檔案名稱</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.cover_links",
            "description": "<p>各尺寸圖檔連結</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"upload cover success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"filename\": \"1517067835270712\",\n        \"cover_links\": {\n            \"c\": \"gallery/1517067835270712_c.jpeg\",\n            \"z\": \"gallery/1517067835270712_z.jpeg\",\n            \"n\": \"gallery/1517067835270712_n.jpeg\",\n            \"q\": \"gallery/1517067835270712_q.jpeg\",\n            \"sq\": \"gallery/1517067835270712_sq.jpeg\",\n            \"o\": \"gallery/1517067835270712_o.jpeg\"\n        }\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"owner_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"file empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"privacy_status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"file error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"owner_id error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"create error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Post.10.UploadCover.php",
    "groupTitle": "Post"
  },
  {
    "type": "PUT",
    "url": "/post/{id}",
    "title": "02. 修改文章",
    "version": "0.1.0",
    "description": "<p>・ 修改文章</p>",
    "name": "PutPostUpdate",
    "group": "Post",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>文章 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "title",
            "description": "<p>標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "content",
            "description": "<p>內容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "cover",
            "description": "<p>封面</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "public",
              "private"
            ],
            "optional": true,
            "field": "privacy_status",
            "description": "<p>隱私狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "draft",
              "publish",
              "delete",
              "disable"
            ],
            "optional": true,
            "field": "status",
            "description": "<p>狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "false",
              "true"
            ],
            "optional": true,
            "field": "recommended",
            "description": "<p>被推薦</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "postcategory_id",
            "description": "<p>文章分類 id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "owner_id",
            "description": "<p>擁有者 id</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"36946497446744064\",\n    \"title\": \"【英國留遊學】倫敦大學金匠學院\",\n    \"content\": \"<p>倫敦大學在台灣應該是個耳熟能詳的英國大學，畢竟民進黨的黨主席蔡英文女士就是畢業於倫敦大學的其中一個學院 &ndash; 倫敦政治經濟學院 （London School of Economics and Political Science），但是除了這個幾乎家喻戶曉的學院之外，其實聯邦制體系的倫敦大學還有其他專攻不同領域的院校，而我今天要介紹的就是專攻藝文與傳媒的金匠學院(Goldsmiths)。</p>\\n<p>如果你喜歡英國當代藝術，或是你有些許研究的話，你應該會有聽過The Young British Artists（簡稱YBAs）這個名稱，他們是一群崛起於80年代的藝術家們，並且在90年代在藝術界造成了一陣轟動，也深深影響了現在的當代藝術。在這群藝術家中有多位都是畢業於金匠學院的藝術科系，例如，在2007年推出全世界最貴的藝術作品 For the love of God的Damien Hirst跟以諷刺手法來表現兩性觀點的Sarah Lucas，他們都是金匠學院的校友。如果你喜歡時尚的話，你一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，你不知道的是，Vivian Westwood也是畢業於金匠學院的設計科系。另外，金匠學院的大眾傳播學系(Media and Communication)也是國際知名的學校，像韓國的導演也都曾來此就讀。可見金匠學院在藝文界真的是有舉足輕重的地位，也培育出了許多赫赫有名的人才。</p>\\n<p>在當年就是衝著這股濃濃藝文味，於是我選擇到了金匠學院，主修大眾傳播媒體，在就讀的期間也感受到了學校的優點與缺點，在這裡就跟可能有計畫來就讀的你分享一下身為校友的個人的經驗吧！</p>\\n<p>首先，有一件事你一定要先知道，在英國，大學的<strong>就讀時間大部分為三年</strong>，也就是讀完三年沒被當的話就能夠順利畢業，另外學校採用的是<strong>三學期制</strong>，跟在台灣熟悉的二學期制不太一樣，雖然這樣假期似乎看起來會比較多，但是其實課業量反而更繁重，因為相同的課程就變成要在較短的時間內學會，而且每一次的假期也就代表著一次的報告dead line，所以，想玩就必須好好做時間規劃，把時間規劃好，才能夠順利繳交報告跟同時享有假期啊！</p>\\n<p>另外，我覺得倫敦大學的學校，相對於倫敦藝術大學，會顯得比較著重於理論，因為我有認識的朋友是就讀於倫敦藝術大學，他們時常都是忙於繳交作品，個人覺得是比較偏向實作方面，但是在金匠學院他們是採取<strong>理論與實作並重</strong>的方式來教課，也就是說作品量的累積跟實作的經驗比起倫敦藝術大學的同學可能會有些落差，但是在理論上知識的基礎可能會更加深厚。會這樣著重理論，是因為他們認為在知識上有著較深厚的基礎就能夠培養出批判性的思考能力(critical thinking)，創造出來的作品會更有內涵並能利用學到的知識來引導觀者，藉以得到更大的共鳴。</p>\\n<p>在大眾傳播的實作課程中，你可以從八種不同的課程來做選擇，分別為：</p>\\n<p>Journalism</p>\\n<p>Photography</p>\\n<p>Filmmaking</p>\\n<p>Illustration</p>\\n<p>Script and Props</p>\\n<p>Animation</p>\\n<p>Radio</p>\\n<p>Interactive Media</p>\\n<p>在一年級的時候你可以選修四種實作課程，但是到了二年級只能選修兩種，到了三年級就必須專精於其中一項，應該是希望透過嘗試讓學生找到自己有興趣或是有潛能的課程，再加以訓練跟培養。</p>\\n<p>我一年級時嘗試了animation，photography，filmmaking跟illustration，二年級的時候選擇了動畫跟攝影，到三年級就專精於攝影。其中動畫課在一二年級的時候，我們學習到的是stop-motion的製作方式，還有如何製作story board，但是到了三年級會學習到使用After Effect這個軟體來製作動畫。filmmaking其實很有趣，你會跟著一個團隊一起合作，可以嘗試到像採訪陌生人，或是自己尋找演員、找場地拍攝，甚至是製作配樂等經驗。在攝影課我們使用的幾乎都是底片相機，學校都有設備可以出借，但是有規定的出借跟歸還時間。在課堂上你會學到關於攝影的理論跟實際操作器具的體驗，不只是相機的操作，還有燈光的調整，底片的沖洗等等。基本上每學年你都需要製作一個自己的project，主題很自由，老師可能會給你一個大方向，但是你可以自己決定主題去製作。</p>\\n<p>不過，專精於其中一項不代表你只能侷限自己做某一個課程，相反的，學校反而鼓勵學生互相合作、學習，甚至跟不同科系的學生也都能夠互相合作來籌畫活動，推行企劃。但是，這相互間的合作通常是要靠個人去籌劃跟聯繫，學校並不會當中間人或指導人，<strong>主動性的培養</strong>，我覺得這是臺灣跟國外學校的一個最主要的差別吧！老師們，可能會提出你可以籌辦活動的想法，但是做與不做就全權交給你去決定，如果要做了，你有問題都可以主動提出，他們也都會很樂意幫助你，但比較不會主動去督促你的進度。像上必修課也都是一樣的，基本上都是lecture接著上seminar，書籍都是一開學就發放給你，在上課前你必須自己先研讀，到學校講師在講課，講課的內容會以書籍裡的內容為基礎，然後延伸出其他更深更廣的理論，有問題再在seminar上提出討論，老師並不會帶著你唸課文，所以如果你事前預習沒做好，在講師講課的時候可能就已經被搞得一頭霧水了。</p>\\n<p>如果你在大學裡期待的是非常多的實作經驗，還有學校幫你選擇好的實習課程，那金匠大學可能不是屬於你的選擇，但是如果你想把底子學好跟喜歡挑戰非商業性的創作，靠自己去尋找實習機會，金匠大學就會是屬於你的不二選擇喔～</p>\",\n    \"cover\": \"1519267601292148\",\n    \"privacy_status\": \"public\",\n    \"status\": \"publish\",\n    \"recommended\": \"false\",\n    \"postcategory_id\": \"7859246410633216\",\n    \"owner_id\": \"8217810346053632\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.post_id",
            "description": "<p>文章 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"update success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"post_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"post error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"cover error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"privacy_status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"recommended error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"owner error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"postcategory error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"updateData empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"update error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Post.02.Update.php",
    "groupTitle": "Post"
  },
  {
    "type": "PUT",
    "url": "/post/{id}/seo",
    "title": "11. 修改文章 SEO",
    "version": "0.1.0",
    "description": "<p>・ 修改文章 SEO</p>",
    "name": "PutPostUpdateSeo",
    "group": "Post",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>文章分類 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "slug",
            "description": "<p>位址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "excerpt",
            "description": "<p>摘要</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "canonical_url",
            "description": "<p>來源位址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "og_title",
            "description": "<p>og 標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "og_description",
            "description": "<p>og 描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "meta_title",
            "description": "<p>meta 標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "meta_description",
            "description": "<p>meta 描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "cover_title",
            "description": "<p>封面標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "cover_alt",
            "description": "<p>封面 alt</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"36946497446744064\",\n    \"slug\" : \"test1\",\n    \"excerpt\" : \"test1test1test1test1test1\",\n    \"canonical_url\" : \"http://abc.com/ask-poi\",\n    \"og_title\" : \"標題一\",\n    \"og_description\" : \"test1test1test1test1test1\",\n    \"meta_title\" : \"標題一\",\n    \"meta_description\" : \"test1test1test1test1test1\",\n    \"cover_title\" : \"標題一\",\n    \"cover_alt\" : \"標題一\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.post_id",
            "description": "<p>文章分類 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"update seo success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"post_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"post error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"post seo error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 409.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 409,\n    \"comment\": \"slug error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"updateData empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"update error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Post.11.UpdateSeo.php",
    "groupTitle": "Post"
  },
  {
    "type": "DELETE",
    "url": "/postcategory/{id}",
    "title": "03. 刪除文章分類",
    "version": "0.1.0",
    "description": "<p>・ 刪除文章分類</p>",
    "name": "DeletePostcategoryDelete",
    "group": "Postcategory",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>文章分類 ID                                        [slug]             位址</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"36946497446744064\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.postcategory_id",
            "description": "<p>分類 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"delete success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"postcategory_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"postcategory error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"delete error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Postcategory.03.Delete.php",
    "groupTitle": "Postcategory"
  },
  {
    "type": "GET",
    "url": "/postcategory/{id}",
    "title": "04. 查詢 單篇文章分類",
    "version": "0.1.0",
    "description": "<p>・ 查詢 單篇文章分類</p>",
    "name": "GetPostcategoryFind",
    "group": "Postcategory",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>文章分類 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.postcategory",
            "description": "<p>文章分類</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"postcategory\": {\n            \"id\": \"7859246544850944\",\n            \"type\": \"global\",\n            \"name\": \"加拿大留學專欄\",\n            \"cover\": \"1519872532384543\",\n            \"status\": \"enable\",\n            \"num_items\": \"0\",\n            \"owner_id\": \"0\",\n            \"creator_id\": \"0\",\n            \"updated_at\": \"2018-03-22 16:29:50\",\n            \"created_at\": \"2018-03-22 16:29:50\",\n            \"slug\": \"study-aboard-canada\",\n            \"excerpt\": \"EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。\",\n            \"og_title\": \"《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！\",\n            \"og_description\": \"EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。\",\n            \"meta_title\": \"《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！\",\n            \"meta_description\": \"study-aboard-canada\",\n            \"cover_title\": \"加拿大留學\",\n            \"cover_alt\": \"加拿大留學\",\n            \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n            \"url_path\": \"/study-aboard-canada-c7859246544850944\"\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Postcategory.04.Find.php",
    "groupTitle": "Postcategory"
  },
  {
    "type": "GET",
    "url": "/postcategory/owner/{owner_id}/{status?}/{order_way?}/{page?}",
    "title": "08. 查詢 文章分類 By OwnerId",
    "version": "0.1.0",
    "description": "<p>・ 查詢 所有文章 By OwnerId</p>",
    "name": "GetPostcategoryFindByOwnerId",
    "group": "Postcategory",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "owner_id",
            "description": "<p>文章分類擁有者</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "all",
              "enable",
              "delete",
              "disable"
            ],
            "optional": true,
            "field": "status",
            "defaultValue": "enable",
            "description": "<p>文章分類狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"owner_id\" : \"0\",\n    \"status\" : \"enable\",\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.postategories",
            "description": "<p>文章分類s</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"postategories\": [\n            {\n                \"id\": \"7859246544850944\",\n                \"type\": \"global\",\n                \"name\": \"加拿大留學專欄\",\n                \"cover\": \"1519872532384543\",\n                \"status\": \"enable\",\n                \"num_items\": \"0\",\n                \"owner_id\": \"0\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-22 16:29:50\",\n                \"created_at\": \"2018-03-22 16:29:50\",\n                \"slug\": \"study-aboard-canada\",\n                \"excerpt\": \"EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。\",\n                \"og_title\": \"《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！\",\n                \"og_description\": \"EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。\",\n                \"meta_title\": \"《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！\",\n                \"meta_description\": \"study-aboard-canada\",\n                \"cover_title\": \"加拿大留學\",\n                \"cover_alt\": \"加拿大留學\",\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"/study-aboard-canada-c7859246544850944\"\n            },\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Postcategory.08.FindByOwnerId.php",
    "groupTitle": "Postcategory"
  },
  {
    "type": "GET",
    "url": "/postcategory/slug/{slug}",
    "title": "06. 查詢 單篇文章分類 By Slug",
    "version": "0.1.0",
    "description": "<p>・ 查詢 單篇文章分類 By Slug</p>",
    "name": "GetPostcategoryFindBySlug",
    "group": "Postcategory",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "slug",
            "description": "<p>文章分類 Slug</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"slug\" : \"study-aboard-canada\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.postcategory",
            "description": "<p>文章分類</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"postcategory\": {\n            \"id\": \"7859246544850944\",\n            \"type\": \"global\",\n            \"name\": \"加拿大留學專欄\",\n            \"cover\": \"1519872532384543\",\n            \"status\": \"enable\",\n            \"num_items\": \"0\",\n            \"owner_id\": \"0\",\n            \"creator_id\": \"0\",\n            \"updated_at\": \"2018-03-22 16:29:50\",\n            \"created_at\": \"2018-03-22 16:29:50\",\n            \"slug\": \"study-aboard-canada\",\n            \"excerpt\": \"EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。\",\n            \"og_title\": \"《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！\",\n            \"og_description\": \"EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。\",\n            \"meta_title\": \"《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！\",\n            \"meta_description\": \"study-aboard-canada\",\n            \"cover_title\": \"加拿大留學\",\n            \"cover_alt\": \"加拿大留學\",\n            \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n            \"url_path\": \"/study-aboard-canada-c7859246544850944\"\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"slug empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Postcategory.06.FindBySlug.php",
    "groupTitle": "Postcategory"
  },
  {
    "type": "GET",
    "url": "/postcategory/type/{type}/{status?}/{order_way?}/{page?}",
    "title": "07. 查詢 文章分類 By Type",
    "version": "0.1.0",
    "description": "<p>・ 查詢 所有文章 By Type</p>",
    "name": "GetPostcategoryFindByType",
    "group": "Postcategory",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "global",
              "user"
            ],
            "optional": false,
            "field": "type",
            "description": "<p>文章分類類型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "all",
              "enable",
              "delete",
              "disable"
            ],
            "optional": true,
            "field": "status",
            "defaultValue": "enable",
            "description": "<p>文章分類狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"type\" : \"global\",\n    \"status\" : \"enable\",\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.postategories",
            "description": "<p>文章分類s</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"postategories\": [\n            {\n                \"id\": \"7859246544850944\",\n                \"type\": \"global\",\n                \"name\": \"加拿大留學專欄\",\n                \"cover\": \"1519872532384543\",\n                \"status\": \"enable\",\n                \"num_items\": \"0\",\n                \"owner_id\": \"0\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-22 16:29:50\",\n                \"created_at\": \"2018-03-22 16:29:50\",\n                \"slug\": \"study-aboard-canada\",\n                \"excerpt\": \"EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。\",\n                \"og_title\": \"《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！\",\n                \"og_description\": \"EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。\",\n                \"meta_title\": \"《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！\",\n                \"meta_description\": \"study-aboard-canada\",\n                \"cover_title\": \"加拿大留學\",\n                \"cover_alt\": \"加拿大留學\",\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"/study-aboard-canada-c7859246544850944\"\n            },\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"type error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Postcategory.07.FindByType.php",
    "groupTitle": "Postcategory"
  },
  {
    "type": "GET",
    "url": "/postcategory/type/{type}/owner/{owner_id}/{status?}/{order_way?}/{page?}",
    "title": "09. 查詢 文章分類 By Type and OwnerId",
    "version": "0.1.0",
    "description": "<p>・ 查詢 所有文章 By Type and OwnerId</p>",
    "name": "GetPostcategoryFindByTypeAndOwnerId",
    "group": "Postcategory",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "global",
              "user"
            ],
            "optional": false,
            "field": "type",
            "description": "<p>文章分類類型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "owner_id",
            "description": "<p>文章分類擁有者</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "all",
              "enable",
              "delete",
              "disable"
            ],
            "optional": true,
            "field": "status",
            "defaultValue": "enable",
            "description": "<p>文章分類狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"type\" : \"global\",\n    \"owner_id\" : \"0\",\n    \"status\" : \"enable\",\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.postategories",
            "description": "<p>文章分類s</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"postategories\": [\n            {\n                \"id\": \"7859246544850944\",\n                \"type\": \"global\",\n                \"name\": \"加拿大留學專欄\",\n                \"cover\": \"1519872532384543\",\n                \"status\": \"enable\",\n                \"num_items\": \"0\",\n                \"owner_id\": \"0\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-22 16:29:50\",\n                \"created_at\": \"2018-03-22 16:29:50\",\n                \"slug\": \"study-aboard-canada\",\n                \"excerpt\": \"EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。\",\n                \"og_title\": \"《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！\",\n                \"og_description\": \"EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。\",\n                \"meta_title\": \"《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！\",\n                \"meta_description\": \"study-aboard-canada\",\n                \"cover_title\": \"加拿大留學\",\n                \"cover_alt\": \"加拿大留學\",\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"/study-aboard-canada-c7859246544850944\"\n            },\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"type error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Postcategory.09.FindByTypeAndOwnerId.php",
    "groupTitle": "Postcategory"
  },
  {
    "type": "GET",
    "url": "/postcategory/list/{status?}/{order_way?}/{page?}",
    "title": "05. 查詢 文章分類",
    "version": "0.1.0",
    "description": "<p>・ 查詢 所有文章</p>",
    "name": "GetPostcategoryFindList",
    "group": "Postcategory",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "all",
              "enable",
              "delete",
              "disable"
            ],
            "optional": true,
            "field": "status",
            "defaultValue": "enable",
            "description": "<p>文章分類狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"status\" : \"enable\",\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.postategories",
            "description": "<p>文章分類s</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"postategories\": [\n            {\n                \"id\": \"7859246544850944\",\n                \"type\": \"global\",\n                \"name\": \"加拿大留學專欄\",\n                \"cover\": \"1519872532384543\",\n                \"status\": \"enable\",\n                \"num_items\": \"0\",\n                \"owner_id\": \"0\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-22 16:29:50\",\n                \"created_at\": \"2018-03-22 16:29:50\",\n                \"slug\": \"study-aboard-canada\",\n                \"excerpt\": \"EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。\",\n                \"og_title\": \"《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！\",\n                \"og_description\": \"EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。\",\n                \"meta_title\": \"《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！\",\n                \"meta_description\": \"study-aboard-canada\",\n                \"cover_title\": \"加拿大留學\",\n                \"cover_alt\": \"加拿大留學\",\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"/study-aboard-canada-c7859246544850944\"\n            },\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Postcategory.05.FindList.php",
    "groupTitle": "Postcategory"
  },
  {
    "type": "POST",
    "url": "/postcategory",
    "title": "01. 新增文章分類",
    "version": "0.1.0",
    "description": "<p>・ 新增文章分類</p>",
    "name": "PostPostcategoryCreate",
    "group": "Postcategory",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "global",
              "user"
            ],
            "optional": true,
            "field": "type",
            "defaultValue": "global",
            "description": "<p>類型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>名稱</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "cover",
            "description": "<p>封面</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "enable",
              "delete",
              "disable"
            ],
            "optional": true,
            "field": "status",
            "defaultValue": "enable",
            "description": "<p>狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "slug",
            "description": "<p>位址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "excerpt",
            "description": "<p>摘要</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "og_title",
            "description": "<p>og 標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "og_description",
            "description": "<p>og 描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "meta_title",
            "description": "<p>meta 標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "meta_description",
            "description": "<p>meta 描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "cover_title",
            "description": "<p>封面標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "cover_alt",
            "description": "<p>封面 alt</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "owner_id",
            "defaultValue": "0",
            "description": "<p>擁有者 id</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"type\" : \"global\",\n    \"name\" : \"標題一\",\n    \"description\" : \"test1test1test1test1test1test1test1\\n\\r test1test1test1test1test1\",\n    \"cover\" : \"7876923123987\",\n    \"status\" : \"enable\",\n    \"slug\" : \"test1\",\n    \"excerpt\" : \"test1test1test1test1test1\",\n    \"og_title\" : \"標題一\",\n    \"og_description\" : \"test1test1test1test1test1\",\n    \"meta_title\" : \"標題一\",\n    \"meta_description\" : \"test1test1test1test1test1\",\n    \"cover_title\" : \"標題一\",\n    \"cover_alt\" : \"標題一\",\n    \"owner_id\" : \"0\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.postcategory_id",
            "description": "<p>文章分類 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"create success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"postcategory_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"name empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"cover empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.05",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"slug empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.06",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"excerpt empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.07",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"og_title empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.08",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"og_decription empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.09",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"meta_title empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.10",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"meta_description empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.11",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"cover_title empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.12",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"cover_alt empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"type error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 409.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 409,\n    \"comment\": \"slug error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"cover error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"dir error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 409.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 409,\n    \"comment\": \"name error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"create error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Postcategory.01.Create.php",
    "groupTitle": "Postcategory"
  },
  {
    "type": "POST",
    "url": "/postcategory/cover",
    "title": "10. 上傳封面檔案",
    "version": "0.1.0",
    "description": "<p>・ 上傳封面檔案</p>",
    "name": "PostPostcategoryUploadCover",
    "group": "Postcategory",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "owner_id",
            "defaultValue": "0",
            "description": "<p>擁有者</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "public",
              "private"
            ],
            "optional": true,
            "field": "privacy_status",
            "defaultValue": "public",
            "description": "<p>隱私狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "enable",
              "disable",
              "delete"
            ],
            "optional": true,
            "field": "status",
            "defaultValue": "disable",
            "description": "<p>狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "file",
            "optional": false,
            "field": "file",
            "description": "<p>檔案</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"owner_id\" : \"0\",\n    \"privacy_status\" : \"public\",\n    \"status\" : \"disable\",\n    \"file\" : \"\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.filename",
            "description": "<p>檔案名稱</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.cover_links",
            "description": "<p>各尺寸圖檔連結</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"upload cover success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"filename\": \"1517067835270712\",\n        \"cover_links\": {\n            \"c\": \"gallery/1517067835270712_c.jpeg\",\n            \"z\": \"gallery/1517067835270712_z.jpeg\",\n            \"n\": \"gallery/1517067835270712_n.jpeg\",\n            \"q\": \"gallery/1517067835270712_q.jpeg\",\n            \"sq\": \"gallery/1517067835270712_sq.jpeg\",\n            \"o\": \"gallery/1517067835270712_o.jpeg\"\n        }\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"file empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"privacy_status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"file error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"create error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Postcategory.10.UploadCover.php",
    "groupTitle": "Postcategory"
  },
  {
    "type": "PUT",
    "url": "/postcategory/{id}",
    "title": "02. 修改文章分類",
    "version": "0.1.0",
    "description": "<p>・ 修改文章分類</p>",
    "name": "PutPostcategoryUpdate",
    "group": "Postcategory",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>文章分類 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "global",
              "user"
            ],
            "optional": true,
            "field": "type",
            "description": "<p>類型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "name",
            "description": "<p>名稱</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "cover",
            "description": "<p>封面</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "enable",
              "delete",
              "disable"
            ],
            "optional": true,
            "field": "status",
            "description": "<p>狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "slug",
            "description": "<p>位址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "excerpt",
            "description": "<p>摘要</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "og_title",
            "description": "<p>og 標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "og_description",
            "description": "<p>og 描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "meta_title",
            "description": "<p>meta 標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "meta_description",
            "description": "<p>meta 描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "cover_title",
            "description": "<p>封面標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "cover_alt",
            "description": "<p>封面 alt</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"36946497446744064\",\n    \"type\" : \"global\",\n    \"name\" : \"標題一\",\n    \"cover\" : \"7876923123987\",\n    \"status\" : \"enable\",\n    \"slug\" : \"test1\",\n    \"excerpt\" : \"test1test1test1test1test1\",\n    \"og_title\" : \"標題一\",\n    \"og_description\" : \"test1test1test1test1test1\",\n    \"meta_title\" : \"標題一\",\n    \"meta_description\" : \"test1test1test1test1test1\",\n    \"cover_title\" : \"標題一\",\n    \"cover_alt\" : \"標題一\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.postcategory_id",
            "description": "<p>文章分類 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"update success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"postcategory_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"postcategory error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"type error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"cover error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 409.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 409,\n    \"comment\": \"slug error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"updateData  / updateSeoData empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"update error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Postcategory.02.Update.php",
    "groupTitle": "Postcategory"
  },
  {
    "type": "PUT",
    "url": "/postcategory/{id}/seo",
    "title": "11. 修改文章分類 SEO",
    "version": "0.1.0",
    "description": "<p>・ 修改文章分類 SEO</p>",
    "name": "PutPostcategoryUpdateSeo",
    "group": "Postcategory",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>文章分類 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "slug",
            "description": "<p>位址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "excerpt",
            "description": "<p>摘要</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "og_title",
            "description": "<p>og 標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "og_description",
            "description": "<p>og 描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "meta_title",
            "description": "<p>meta 標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "meta_description",
            "description": "<p>meta 描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "cover_title",
            "description": "<p>封面標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "cover_alt",
            "description": "<p>封面 alt</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"36946497446744064\",\n    \"slug\" : \"test1\",\n    \"excerpt\" : \"test1test1test1test1test1\",\n    \"og_title\" : \"標題一\",\n    \"og_description\" : \"test1test1test1test1test1\",\n    \"meta_title\" : \"標題一\",\n    \"meta_description\" : \"test1test1test1test1test1\",\n    \"cover_title\" : \"標題一\",\n    \"cover_alt\" : \"標題一\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.postcategory_id",
            "description": "<p>文章分類 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"update seo success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"postcategory_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"postcategory error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"postcategory seo error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 409.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 409,\n    \"comment\": \"slug error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"updateData empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"update error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Postcategory.11.UpdateSeo.php",
    "groupTitle": "Postcategory"
  },
  {
    "type": "POST",
    "url": "/register",
    "title": "01. 註冊",
    "version": "0.1.0",
    "description": "<p>・ 使用者註冊</p>",
    "name": "PostUserRegister",
    "group": "Register",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "account",
            "description": "<p>使用者帳號</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>密碼</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password_repeat",
            "description": "<p>確認密碼</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "first_name",
            "description": "<p>名</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "last_name",
            "description": "<p>姓</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mobile_country_code",
            "description": "<p>手機號碼國碼</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mobile_phone",
            "description": "<p>手機號碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"account\"   : \"service@showhi.co\",\n    \"password\"  : \"12345678\" ,\n    \"password_repeat\" : \"12345678\",\n    \"first_name\" : \"John\",\n    \"last_name\"   : \"Wu\",\n    \"mobile_country_code\" : \"886\",\n    \"mobile_phone\" : \"912345678\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"register success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"token\": \"8f3e973061800dc6ebcb367079b305d8\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"account is empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"password / password_repeat is empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"first_name is empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.05",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"last_name is empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.06",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"mobile_country_code is empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.07",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"mobile_phone is empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 403.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 403,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"account error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"password' length < 8\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"password / password_repeat is NOT equal\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"mobile_phone format error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 409.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 409,\n    \"comment\": \"account error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 409.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 409,\n    \"comment\": \"mobile_phone error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"create user error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Register.01.Register.php",
    "groupTitle": "Register"
  },
  {
    "type": "GET",
    "url": "/role/{id}",
    "title": "04. 查詢 單一角色",
    "version": "0.1.0",
    "description": "<p>・ 查詢 單一角色</p>",
    "name": "GetRoleFind",
    "group": "Role",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>角色 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.role",
            "description": "<p>角色</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"role\": {\n            \"id\": \"5324389482631168\",\n            \"type\": \"general\",\n            \"code\": \"admin\",\n            \"note\": \"網站最高管理員\",\n            \"creator_id\": \"0\",\n            \"updated_at\": \"2018-03-15 16:37:13\",\n            \"created_at\": \"2018-03-15 16:37:13\",\n            \"permissions\": [\n                {\n                    \"id\": \"5324389491019776\",\n                    \"type\": \"admin\",\n                    \"code\": \"role_create\",\n                    \"note\": \"新增角色\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-15 16:37:13\",\n                    \"created_at\": \"2018-03-15 16:37:13\"\n                },\n                {\n                    \"id\": \"5324389507796992\",\n                    \"type\": \"admin\",\n                    \"code\": \"role_update\",\n                    \"note\": \"更新角色\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-15 16:37:13\",\n                    \"created_at\": \"2018-03-15 16:37:13\"\n                },\n                {\n                    \"id\": \"5324389545545728\",\n                    \"type\": \"admin\",\n                    \"code\": \"role_delete\",\n                    \"note\": \"刪除角色\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-15 16:37:13\",\n                    \"created_at\": \"2018-03-15 16:37:13\"\n                },\n                {\n                    \"id\": \"5324389562322944\",\n                    \"type\": \"admin\",\n                    \"code\": \"locales_create\",\n                    \"note\": \"新增語系\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-15 16:37:13\",\n                    \"created_at\": \"2018-03-15 16:37:13\"\n                },\n                {\n                    \"id\": \"5324389570711552\",\n                    \"type\": \"admin\",\n                    \"code\": \"locales_update\",\n                    \"note\": \"更新語系\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-15 16:37:13\",\n                    \"created_at\": \"2018-03-15 16:37:13\"\n                },\n                {\n                    \"id\": \"5324389587488768\",\n                    \"type\": \"admin\",\n                    \"code\": \"locales_delete\",\n                    \"note\": \"刪除語系\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-15 16:37:13\",\n                    \"created_at\": \"2018-03-15 16:37:13\"\n                }\n            ]\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Role.04.Find.php",
    "groupTitle": "Role"
  },
  {
    "type": "GET",
    "url": "/role/code/{code}/{order_way?}/{page?}",
    "title": "07. 查詢 所有角色 By Code",
    "version": "0.1.0",
    "description": "<p>・ 查詢 所有角色 By Code</p>",
    "name": "GetRoleFindByCode",
    "group": "Role",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "code",
            "description": "<p>角色代號</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"code\" : \"admin\",\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.roles",
            "description": "<p>角色s</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"roles\": [\n            {\n                \"id\": \"5324389482631168\",\n                \"type\": \"general\",\n                \"code\": \"admin\",\n                \"note\": \"網站最高管理員\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-15 16:37:13\",\n                \"created_at\": \"2018-03-15 16:37:13\",\n                \"permissions\": [\n                    {\n                        \"id\": \"5324389491019776\",\n                        \"type\": \"admin\",\n                        \"code\": \"role_create\",\n                        \"note\": \"新增角色\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389507796992\",\n                        \"type\": \"admin\",\n                        \"code\": \"role_update\",\n                        \"note\": \"更新角色\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389545545728\",\n                        \"type\": \"admin\",\n                        \"code\": \"role_delete\",\n                        \"note\": \"刪除角色\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389562322944\",\n                        \"type\": \"admin\",\n                        \"code\": \"locales_create\",\n                        \"note\": \"新增語系\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389570711552\",\n                        \"type\": \"admin\",\n                        \"code\": \"locales_update\",\n                        \"note\": \"更新語系\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389587488768\",\n                        \"type\": \"admin\",\n                        \"code\": \"locales_delete\",\n                        \"note\": \"刪除語系\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    }\n                ]\n            },\n            {\n                \"id\": \"5324389914644480\",\n                \"type\": \"company\",\n                \"code\": \"admin\",\n                \"note\": \"公司管理員\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-15 16:37:13\",\n                \"created_at\": \"2018-03-15 16:37:13\",\n                \"permissions\": [\n                    {\n                        \"id\": \"5324389918838784\",\n                        \"type\": \"company\",\n                        \"code\": \"report_show\",\n                        \"note\": \"觀看統計數據\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-22 22:29:58\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389939810304\",\n                        \"type\": \"company\",\n                        \"code\": \"quotation_create\",\n                        \"note\": \"新增學生報名報價單\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389952393216\",\n                        \"type\": \"company\",\n                        \"code\": \"quotation_show\",\n                        \"note\": \"觀看每筆報名的狀態\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-22 22:29:39\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    }\n                ]\n            },\n            {\n                \"id\": \"5324389994336256\",\n                \"type\": \"school\",\n                \"code\": \"admin\",\n                \"note\": \"學校管理員\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-15 16:37:13\",\n                \"created_at\": \"2018-03-15 16:37:13\",\n                \"permissions\": [\n                    {\n                        \"id\": \"5324389998530560\",\n                        \"type\": \"school\",\n                        \"code\": \"report_show\",\n                        \"note\": \"觀看統計數據\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-22 22:29:39\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324390011113472\",\n                        \"type\": \"school\",\n                        \"code\": \"quotation_verify\",\n                        \"note\": \"審核學生報名報價單\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    }\n                ]\n            }\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"code empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Role.07.FindByCode.php",
    "groupTitle": "Role"
  },
  {
    "type": "GET",
    "url": "/role/type/{type}/{order_way?}/{page?}",
    "title": "06. 查詢 所有角色 By Type",
    "version": "0.1.0",
    "description": "<p>・ 查詢 所有角色 By Type</p>",
    "name": "GetRoleFindByType",
    "group": "Role",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "general",
              "school",
              "company"
            ],
            "optional": false,
            "field": "type",
            "description": "<p>角色類型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"type\" : \"general\",\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.roles",
            "description": "<p>角色s</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"roles\": [\n            {\n                \"id\": \"5324389482631168\",\n                \"type\": \"general\",\n                \"code\": \"admin\",\n                \"note\": \"網站最高管理員\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-15 16:37:13\",\n                \"created_at\": \"2018-03-15 16:37:13\",\n                \"permissions\": [\n                    {\n                        \"id\": \"5324389491019776\",\n                        \"type\": \"admin\",\n                        \"code\": \"role_create\",\n                        \"note\": \"新增角色\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389507796992\",\n                        \"type\": \"admin\",\n                        \"code\": \"role_update\",\n                        \"note\": \"更新角色\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389545545728\",\n                        \"type\": \"admin\",\n                        \"code\": \"role_delete\",\n                        \"note\": \"刪除角色\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389562322944\",\n                        \"type\": \"admin\",\n                        \"code\": \"locales_create\",\n                        \"note\": \"新增語系\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389570711552\",\n                        \"type\": \"admin\",\n                        \"code\": \"locales_update\",\n                        \"note\": \"更新語系\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389587488768\",\n                        \"type\": \"admin\",\n                        \"code\": \"locales_delete\",\n                        \"note\": \"刪除語系\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    }\n                ]\n            },\n            {\n                \"id\": \"5324389604265984\",\n                \"type\": \"general\",\n                \"code\": \"manager\",\n                \"note\": \"網站管理員\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-15 16:37:13\",\n                \"created_at\": \"2018-03-15 16:37:13\",\n                \"permissions\": [\n                    {\n                        \"id\": \"5324389625237504\",\n                        \"type\": \"admin\",\n                        \"code\": \"user_create\",\n                        \"note\": \"新增使用者\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389637820416\",\n                        \"type\": \"admin\",\n                        \"code\": \"user_update\",\n                        \"note\": \"更新使用者\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389658791936\",\n                        \"type\": \"admin\",\n                        \"code\": \"user_delete\",\n                        \"note\": \"刪除使用者\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389671374848\",\n                        \"type\": \"admin\",\n                        \"code\": \"postcategory_create\",\n                        \"note\": \"新增文章分類\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-22 22:28:36\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389688152064\",\n                        \"type\": \"admin\",\n                        \"code\": \"postcategory_update\",\n                        \"note\": \"更新文章分類\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-22 22:28:36\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389709123584\",\n                        \"type\": \"admin\",\n                        \"code\": \"postcategory_delete\",\n                        \"note\": \"刪除文章分類\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-22 22:28:36\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    }\n                ]\n            },\n            {\n                \"id\": \"5324389721706496\",\n                \"type\": \"general\",\n                \"code\": \"editor\",\n                \"note\": \"網站編輯\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-15 16:37:13\",\n                \"created_at\": \"2018-03-15 16:37:13\",\n                \"permissions\": [\n                    {\n                        \"id\": \"5324389730095104\",\n                        \"type\": \"admin\",\n                        \"code\": \"blog_update\",\n                        \"note\": \"更新網誌資料\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389742678016\",\n                        \"type\": \"admin\",\n                        \"code\": \"post_create\",\n                        \"note\": \"新增文章\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389759455232\",\n                        \"type\": \"admin\",\n                        \"code\": \"post_update\",\n                        \"note\": \"更新文章\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389772038144\",\n                        \"type\": \"admin\",\n                        \"code\": \"post_delete\",\n                        \"note\": \"刪除文章\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    }\n                ]\n            },\n            {\n                \"id\": \"5324389788815360\",\n                \"type\": \"general\",\n                \"code\": \"author\",\n                \"note\": \"網站作者\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-15 16:37:13\",\n                \"created_at\": \"2018-03-15 16:37:13\",\n                \"permissions\": [\n                    {\n                        \"id\": \"5324389797203968\",\n                        \"type\": \"general\",\n                        \"code\": \"post_create\",\n                        \"note\": \"新增文章\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389813981184\",\n                        \"type\": \"general\",\n                        \"code\": \"post_update\",\n                        \"note\": \"更新文章\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389830758400\",\n                        \"type\": \"general\",\n                        \"code\": \"post_delete\",\n                        \"note\": \"刪除文章\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    }\n                ]\n            },\n            {\n                \"id\": \"5324389847535616\",\n                \"type\": \"general\",\n                \"code\": \"member\",\n                \"note\": \"網站會員\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-15 16:37:13\",\n                \"created_at\": \"2018-03-15 16:37:13\",\n                \"permissions\": [\n                    {\n                        \"id\": \"5324389851729920\",\n                        \"type\": \"general\",\n                        \"code\": \"member_update\",\n                        \"note\": \"更新個人資料\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389868507136\",\n                        \"type\": \"general\",\n                        \"code\": \"file_upload\",\n                        \"note\": \"上傳檔案\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389885284352\",\n                        \"type\": \"general\",\n                        \"code\": \"password_change\",\n                        \"note\": \"變更密碼\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389897867264\",\n                        \"type\": \"general\",\n                        \"code\": \"quotation_fill\",\n                        \"note\": \"填寫報價單\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    }\n                ]\n            }\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"type empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"type error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Role.06.FindByType.php",
    "groupTitle": "Role"
  },
  {
    "type": "GET",
    "url": "/role/type/{type}/code/{code}/{order_way?}/{page?}",
    "title": "08. 查詢 所有角色 By Type And Code",
    "version": "0.1.0",
    "description": "<p>・ 查詢 所有角色 By Type And Code</p>",
    "name": "GetRoleFindByTypeAndCode",
    "group": "Role",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "general",
              "school",
              "company"
            ],
            "optional": false,
            "field": "type",
            "description": "<p>角色類型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "code",
            "description": "<p>角色代號</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"type\" : \"general\",\n    \"code\" : \"admin\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.role",
            "description": "<p>角色</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"role\": {\n            \"id\": \"5324389482631168\",\n            \"type\": \"general\",\n            \"code\": \"admin\",\n            \"note\": \"網站最高管理員\",\n            \"creator_id\": \"0\",\n            \"updated_at\": \"2018-03-15 16:37:13\",\n            \"created_at\": \"2018-03-15 16:37:13\",\n            \"permissions\": [\n                {\n                    \"id\": \"5324389491019776\",\n                    \"type\": \"admin\",\n                    \"code\": \"role_create\",\n                    \"note\": \"新增角色\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-15 16:37:13\",\n                    \"created_at\": \"2018-03-15 16:37:13\"\n                },\n                {\n                    \"id\": \"5324389507796992\",\n                    \"type\": \"admin\",\n                    \"code\": \"role_update\",\n                    \"note\": \"更新角色\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-15 16:37:13\",\n                    \"created_at\": \"2018-03-15 16:37:13\"\n                },\n                {\n                    \"id\": \"5324389545545728\",\n                    \"type\": \"admin\",\n                    \"code\": \"role_delete\",\n                    \"note\": \"刪除角色\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-15 16:37:13\",\n                    \"created_at\": \"2018-03-15 16:37:13\"\n                },\n                {\n                    \"id\": \"5324389562322944\",\n                    \"type\": \"admin\",\n                    \"code\": \"locales_create\",\n                    \"note\": \"新增語系\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-15 16:37:13\",\n                    \"created_at\": \"2018-03-15 16:37:13\"\n                },\n                {\n                    \"id\": \"5324389570711552\",\n                    \"type\": \"admin\",\n                    \"code\": \"locales_update\",\n                    \"note\": \"更新語系\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-15 16:37:13\",\n                    \"created_at\": \"2018-03-15 16:37:13\"\n                },\n                {\n                    \"id\": \"5324389587488768\",\n                    \"type\": \"admin\",\n                    \"code\": \"locales_delete\",\n                    \"note\": \"刪除語系\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-15 16:37:13\",\n                    \"created_at\": \"2018-03-15 16:37:13\"\n                }\n            ]\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"type empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"code empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"type error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Role.08.FindByTypeAndCode.php",
    "groupTitle": "Role"
  },
  {
    "type": "GET",
    "url": "/role/list/{order_way?}/{page?}",
    "title": "05. 查詢 所有角色",
    "version": "0.1.0",
    "description": "<p>・ 查詢 所有角色</p>",
    "name": "GetRoleFindList",
    "group": "Role",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.roles",
            "description": "<p>角色s</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"roles\": [\n            {\n                \"id\": \"5324389482631168\",\n                \"type\": \"general\",\n                \"code\": \"admin\",\n                \"note\": \"網站最高管理員\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-15 16:37:13\",\n                \"created_at\": \"2018-03-15 16:37:13\",\n                \"permissions\": [\n                    {\n                        \"id\": \"5324389491019776\",\n                        \"type\": \"admin\",\n                        \"code\": \"role_create\",\n                        \"note\": \"新增角色\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389507796992\",\n                        \"type\": \"admin\",\n                        \"code\": \"role_update\",\n                        \"note\": \"更新角色\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389545545728\",\n                        \"type\": \"admin\",\n                        \"code\": \"role_delete\",\n                        \"note\": \"刪除角色\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389562322944\",\n                        \"type\": \"admin\",\n                        \"code\": \"locales_create\",\n                        \"note\": \"新增語系\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389570711552\",\n                        \"type\": \"admin\",\n                        \"code\": \"locales_update\",\n                        \"note\": \"更新語系\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389587488768\",\n                        \"type\": \"admin\",\n                        \"code\": \"locales_delete\",\n                        \"note\": \"刪除語系\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    }\n                ]\n            },\n            {\n                \"id\": \"5324389604265984\",\n                \"type\": \"general\",\n                \"code\": \"manager\",\n                \"note\": \"網站管理員\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-15 16:37:13\",\n                \"created_at\": \"2018-03-15 16:37:13\",\n                \"permissions\": [\n                    {\n                        \"id\": \"5324389625237504\",\n                        \"type\": \"admin\",\n                        \"code\": \"user_create\",\n                        \"note\": \"新增使用者\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389637820416\",\n                        \"type\": \"admin\",\n                        \"code\": \"user_update\",\n                        \"note\": \"更新使用者\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389658791936\",\n                        \"type\": \"admin\",\n                        \"code\": \"user_delete\",\n                        \"note\": \"刪除使用者\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389671374848\",\n                        \"type\": \"admin\",\n                        \"code\": \"postcategory_create\",\n                        \"note\": \"新增文章分類\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-22 22:28:36\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389688152064\",\n                        \"type\": \"admin\",\n                        \"code\": \"postcategory_update\",\n                        \"note\": \"更新文章分類\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-22 22:28:36\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389709123584\",\n                        \"type\": \"admin\",\n                        \"code\": \"postcategory_delete\",\n                        \"note\": \"刪除文章分類\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-22 22:28:36\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    }\n                ]\n            },\n            {\n                \"id\": \"5324389721706496\",\n                \"type\": \"general\",\n                \"code\": \"editor\",\n                \"note\": \"網站編輯\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-15 16:37:13\",\n                \"created_at\": \"2018-03-15 16:37:13\",\n                \"permissions\": [\n                    {\n                        \"id\": \"5324389730095104\",\n                        \"type\": \"admin\",\n                        \"code\": \"blog_update\",\n                        \"note\": \"更新網誌資料\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389742678016\",\n                        \"type\": \"admin\",\n                        \"code\": \"post_create\",\n                        \"note\": \"新增文章\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389759455232\",\n                        \"type\": \"admin\",\n                        \"code\": \"post_update\",\n                        \"note\": \"更新文章\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389772038144\",\n                        \"type\": \"admin\",\n                        \"code\": \"post_delete\",\n                        \"note\": \"刪除文章\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    }\n                ]\n            },\n            {\n                \"id\": \"5324389788815360\",\n                \"type\": \"general\",\n                \"code\": \"author\",\n                \"note\": \"網站作者\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-15 16:37:13\",\n                \"created_at\": \"2018-03-15 16:37:13\",\n                \"permissions\": [\n                    {\n                        \"id\": \"5324389797203968\",\n                        \"type\": \"general\",\n                        \"code\": \"post_create\",\n                        \"note\": \"新增文章\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389813981184\",\n                        \"type\": \"general\",\n                        \"code\": \"post_update\",\n                        \"note\": \"更新文章\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389830758400\",\n                        \"type\": \"general\",\n                        \"code\": \"post_delete\",\n                        \"note\": \"刪除文章\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    }\n                ]\n            },\n            {\n                \"id\": \"5324389847535616\",\n                \"type\": \"general\",\n                \"code\": \"member\",\n                \"note\": \"網站會員\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-15 16:37:13\",\n                \"created_at\": \"2018-03-15 16:37:13\",\n                \"permissions\": [\n                    {\n                        \"id\": \"5324389851729920\",\n                        \"type\": \"general\",\n                        \"code\": \"member_update\",\n                        \"note\": \"更新個人資料\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389868507136\",\n                        \"type\": \"general\",\n                        \"code\": \"file_upload\",\n                        \"note\": \"上傳檔案\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389885284352\",\n                        \"type\": \"general\",\n                        \"code\": \"password_change\",\n                        \"note\": \"變更密碼\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389897867264\",\n                        \"type\": \"general\",\n                        \"code\": \"quotation_fill\",\n                        \"note\": \"填寫報價單\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    }\n                ]\n            },\n            {\n                \"id\": \"5324389914644480\",\n                \"type\": \"company\",\n                \"code\": \"admin\",\n                \"note\": \"公司管理員\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-15 16:37:13\",\n                \"created_at\": \"2018-03-15 16:37:13\",\n                \"permissions\": [\n                    {\n                        \"id\": \"5324389918838784\",\n                        \"type\": \"company\",\n                        \"code\": \"report_show\",\n                        \"note\": \"觀看統計數據\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-22 22:29:58\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389939810304\",\n                        \"type\": \"company\",\n                        \"code\": \"quotation_create\",\n                        \"note\": \"新增學生報名報價單\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389952393216\",\n                        \"type\": \"company\",\n                        \"code\": \"quotation_show\",\n                        \"note\": \"觀看每筆報名的狀態\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-22 22:29:39\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    }\n                ]\n            },\n            {\n                \"id\": \"5324389964976128\",\n                \"type\": \"company\",\n                \"code\": \"agent\",\n                \"note\": \"公司仲介\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-15 16:37:13\",\n                \"created_at\": \"2018-03-15 16:37:13\",\n                \"permissions\": [\n                    {\n                        \"id\": \"5324389939810304\",\n                        \"type\": \"company\",\n                        \"code\": \"quotation_create\",\n                        \"note\": \"新增學生報名報價單\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324389952393216\",\n                        \"type\": \"company\",\n                        \"code\": \"quotation_show\",\n                        \"note\": \"觀看每筆報名的狀態\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-22 22:29:39\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    }\n                ]\n            },\n            {\n                \"id\": \"5324389994336256\",\n                \"type\": \"school\",\n                \"code\": \"admin\",\n                \"note\": \"學校管理員\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-15 16:37:13\",\n                \"created_at\": \"2018-03-15 16:37:13\",\n                \"permissions\": [\n                    {\n                        \"id\": \"5324389998530560\",\n                        \"type\": \"school\",\n                        \"code\": \"report_show\",\n                        \"note\": \"觀看統計數據\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-22 22:29:39\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    },\n                    {\n                        \"id\": \"5324390011113472\",\n                        \"type\": \"school\",\n                        \"code\": \"quotation_verify\",\n                        \"note\": \"審核學生報名報價單\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    }\n                ]\n            },\n            {\n                \"id\": \"5324390027890688\",\n                \"type\": \"school\",\n                \"code\": \"contact\",\n                \"note\": \"學校經辦員\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-15 16:37:13\",\n                \"created_at\": \"2018-03-15 16:37:13\",\n                \"permissions\": [\n                    {\n                        \"id\": \"5324390011113472\",\n                        \"type\": \"school\",\n                        \"code\": \"quotation_verify\",\n                        \"note\": \"審核學生報名報價單\",\n                        \"creator_id\": \"0\",\n                        \"updated_at\": \"2018-03-15 16:37:13\",\n                        \"created_at\": \"2018-03-15 16:37:13\"\n                    }\n                ]\n            }\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Role.05.FindList.php",
    "groupTitle": "Role"
  },
  {
    "type": "DELETE",
    "url": "/school/{id}",
    "title": "03. 刪除學校",
    "version": "0.1.0",
    "description": "<p>・ 刪除學校</p>",
    "name": "DeleteSchoolDelete",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>學校 ID                                        [slug]             位址</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"36946497446744064\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_id",
            "description": "<p>學校 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"delete success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"delete error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.03.Delete.php",
    "groupTitle": "School"
  },
  {
    "type": "DELETE",
    "url": "/school/course/{school_course_id}",
    "title": "15. 刪除學校課程",
    "version": "0.1.0",
    "description": "<p>・ 刪除學校課程</p>",
    "name": "DeleteSchoolDeleteCourse",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_course_id",
            "description": "<p>學校課程 ID                                        [slug]             位址</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"school_course_id\" : \"36946497446744064\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_course_id",
            "description": "<p>學校課程 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"delete school course success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_course_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"school_course_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school course error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"delete school course error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.15.DeleteCourse.php",
    "groupTitle": "School"
  },
  {
    "type": "DELETE",
    "url": "/school/dorm/{school_dorm_id}",
    "title": "20. 刪除學校宿舍",
    "version": "0.1.0",
    "description": "<p>・ 刪除學校宿舍</p>",
    "name": "DeleteSchoolDeleteDorm",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_dorm_id",
            "description": "<p>學校宿舍 ID                                        [slug]             位址</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"school_dorm_id\" : \"36946497446744064\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_dorm_id",
            "description": "<p>學校宿舍 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"delete school dorm success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_dorm_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"school_dorm_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school dorm error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"delete school dorm error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.20.DeleteDorm.php",
    "groupTitle": "School"
  },
  {
    "type": "DELETE",
    "url": "/school/{id}/filter",
    "title": "31. 刪除學校 Filter",
    "version": "0.1.0",
    "description": "<p>・ 刪除學校 Filter</p>",
    "name": "DeleteSchoolDeleteFilter",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>學校 ID                                        [slug]             位址</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"36946497446744064\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.filter_school_ids",
            "description": "<p>學校 filter IDs</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"delete success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"filter_school_ids\": [\n            \"1522851523251513\",\n            \"1522851523251513\",\n            \"1522851523251513\"\n        ]\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"filter school error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"delete error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.31.DeleteFilter.php",
    "groupTitle": "School"
  },
  {
    "type": "DELETE",
    "url": "/school/user/{school_user_id}",
    "title": "25. 刪除學校使用者",
    "version": "0.1.0",
    "description": "<p>・ 刪除學校使用者</p>",
    "name": "DeleteSchoolDeleteUser",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_user_id",
            "description": "<p>學校使用者 ID                                        [slug]             位址</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"school_user_id\" : \"36946497446744064\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_user_id",
            "description": "<p>學校使用者 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"delete school user success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_user_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"school_user_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school user error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"delete school user error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.25.DeleteUser.php",
    "groupTitle": "School"
  },
  {
    "type": "GET",
    "url": "/school/{id}",
    "title": "04. 查詢 單篇學校",
    "version": "0.1.0",
    "description": "<p>・ 查詢 單篇學校</p>",
    "name": "GetSchoolFind",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>學校 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school",
            "description": "<p>學校</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school\": {\n            \"id\": \"12658531849342976\",\n            \"name\": \"test777\",\n            \"description\": \"test9999test9999test9999test9999test9999test9999test9999test9999test9999\",\n            \"facility\": \"qwerqwerqwerwqerqwerwqer\",\n            \"cover\": \"1522851523251513\",\n            \"status\": \"enable\",\n            \"creator_id\": \"11042496696160256\",\n            \"updated_at\": \"2018-04-04 22:20:29\",\n            \"created_at\": \"2018-04-04 22:20:29\",\n            \"slug\": \"test777\",\n            \"excerpt\": \"分類三\",\n            \"og_title\": \"分類三\",\n            \"og_description\": \"分類三分類三分類三分類三分類三分類三\",\n            \"meta_title\": \"分類三\",\n            \"meta_description\": \"分類三分類三分類三分類三分類三分類三\",\n            \"cover_title\": \"分類三\",\n            \"cover_alt\": \"分類三\",\n            \"cover_links\": {\n                \"o\": \"gallery/school_12658531849342976/cover/1522851523251513_o.jpeg\"\n            },\n            \"image_links\": [\n                {\n                    \"o\": \"gallery/school_12658531849342976/cover/1522851523251513_o.jpeg\"\n                }\n            ],\n            \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n            \"url_path\": \"/test777-s12658531849342976\"\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.04.Find.php",
    "groupTitle": "School"
  },
  {
    "type": "GET",
    "url": "/school/slug/{slug}",
    "title": "06. 查詢 單篇學校 By Slug",
    "version": "0.1.0",
    "description": "<p>・ 查詢 單篇學校 By Slug</p>",
    "name": "GetSchoolFindBySlug",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "slug",
            "description": "<p>學校 Slug</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"slug\" : \"英國留遊學-倫敦大學金匠學院\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school",
            "description": "<p>學校</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school\": {\n            \"id\": \"12658531849342976\",\n            \"name\": \"test777\",\n            \"description\": \"test9999test9999test9999test9999test9999test9999test9999test9999test9999\",\n            \"facility\": \"qwerqwerqwerwqerqwerwqer\",\n            \"cover\": \"1522851523251513\",\n            \"status\": \"enable\",\n            \"creator_id\": \"11042496696160256\",\n            \"updated_at\": \"2018-04-04 22:20:29\",\n            \"created_at\": \"2018-04-04 22:20:29\",\n            \"slug\": \"test777\",\n            \"excerpt\": \"分類三\",\n            \"og_title\": \"分類三\",\n            \"og_description\": \"分類三分類三分類三分類三分類三分類三\",\n            \"meta_title\": \"分類三\",\n            \"meta_description\": \"分類三分類三分類三分類三分類三分類三\",\n            \"cover_title\": \"分類三\",\n            \"cover_alt\": \"分類三\",\n            \"cover_links\": {\n                \"o\": \"gallery/school_12658531849342976/cover/1522851523251513_o.jpeg\"\n            },\n            \"image_links\": [\n                {\n                    \"o\": \"gallery/school_12658531849342976/cover/1522851523251513_o.jpeg\"\n                }\n            ],\n            \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n            \"url_path\": \"/test777-s12658531849342976\"\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"slug empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.06.FindBySlug.php",
    "groupTitle": "School"
  },
  {
    "type": "GET",
    "url": "/school/course/{school_course_id}",
    "title": "16. 查詢 單篇學校課程",
    "version": "0.1.0",
    "description": "<p>・ 查詢 單篇學校課程</p>",
    "name": "GetSchoolFindCourse",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_course_id",
            "description": "<p>學校課程 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"school_course_id\" : \"12668924801978368\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_course",
            "description": "<p>學校課程</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch school course success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_course\": {\n            \"id\": \"12668924801978368\",\n            \"school_id\": \"12658531849342976\",\n            \"name\": \"course1\",\n            \"description\": \"course1course1course1course1course1course1course1course1course1course1course1\",\n            \"ta\": \"parent-child\",\n            \"updated_at\": \"2018-04-04 23:01:47\",\n            \"created_at\": \"2018-04-04 23:01:47\"\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"school_course_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.16.FindCourse.php",
    "groupTitle": "School"
  },
  {
    "type": "GET",
    "url": "/school/{id}/course/{order_way?}/{page?}",
    "title": "17. 查詢 學校課程 By School Id",
    "version": "0.1.0",
    "description": "<p>・ 查詢 學校課程 By School Id</p>",
    "name": "GetSchoolFindCourseBySchoolId",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>學校 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"12668924801978368\",\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_courses",
            "description": "<p>學校課程s</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch school courses success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_courses\": [\n            {\n                \"id\": \"12668924801978368\",\n                \"school_id\": \"12658531849342976\",\n                \"name\": \"course1\",\n                \"description\": \"course1course1course1course1course1course1course1course1course1course1course1\",\n                \"ta\": \"parent-child\",\n                \"updated_at\": \"2018-04-04 23:01:47\",\n                \"created_at\": \"2018-04-04 23:01:47\"\n            },\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.17.FindCourseBySchoolId.php",
    "groupTitle": "School"
  },
  {
    "type": "GET",
    "url": "/school/dorm/{school_dorm_id}",
    "title": "21. 查詢 單篇學校宿舍",
    "version": "0.1.0",
    "description": "<p>・ 查詢 單篇學校宿舍</p>",
    "name": "GetSchoolFindDorm",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_dorm_id",
            "description": "<p>學校宿舍 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"school_dorm_id\" : \"12668924801978368\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_dorm",
            "description": "<p>學校宿舍</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch school dorm success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_dorm\": {\n            \"id\": \"12679249458761728\",\n            \"school_id\": \"12658531849342976\",\n            \"type\": \"dorm\",\n            \"service\": \"serviceserviceserviceserviceserviceserviceservice\",\n            \"facility\": \"facilityfacilityfacilityfacilityfacilityfacility\",\n            \"updated_at\": \"2018-04-04 23:42:48\",\n            \"created_at\": \"2018-04-04 23:42:48\",\n            \"rooms\": [\n                {\n                    \"id\": \"12679249500704768\",\n                    \"school_id\": \"12658531849342976\",\n                    \"dorm_id\": \"12679249458761728\",\n                    \"type\": \"single\",\n                    \"accessible\": \"0\",\n                    \"smoking\": \"0\",\n                    \"updated_at\": \"2018-04-04 23:42:48\",\n                    \"created_at\": \"2018-04-04 23:42:48\"\n                },\n                {\n                    \"id\": \"12679249513287680\",\n                    \"school_id\": \"12658531849342976\",\n                    \"dorm_id\": \"12679249458761728\",\n                    \"type\": \"twin\",\n                    \"accessible\": \"0\",\n                    \"smoking\": \"0\",\n                    \"updated_at\": \"2018-04-04 23:42:48\",\n                    \"created_at\": \"2018-04-04 23:42:48\"\n                }\n            ]\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"school_dorm_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.21.FindDorm.php",
    "groupTitle": "School"
  },
  {
    "type": "GET",
    "url": "/school/{id}/dorm/{order_way?}/{page?}",
    "title": "22. 查詢 學校宿舍 By School Id",
    "version": "0.1.0",
    "description": "<p>・ 查詢 學校宿舍 By School Id</p>",
    "name": "GetSchoolFindDormBySchoolId",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>學校 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"12668924801978368\",\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_dorms",
            "description": "<p>學校宿舍s</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch school dorms success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_dorms\": [\n            {\n            \"id\": \"12679249458761728\",\n            \"school_id\": \"12658531849342976\",\n            \"type\": \"dorm\",\n            \"service\": \"serviceserviceserviceserviceserviceserviceservice\",\n            \"facility\": \"facilityfacilityfacilityfacilityfacilityfacility\",\n            \"updated_at\": \"2018-04-04 23:42:48\",\n            \"created_at\": \"2018-04-04 23:42:48\",\n            \"rooms\": [\n                {\n                    \"id\": \"12679249500704768\",\n                    \"school_id\": \"12658531849342976\",\n                    \"dorm_id\": \"12679249458761728\",\n                    \"type\": \"single\",\n                    \"accessible\": \"0\",\n                    \"smoking\": \"0\",\n                    \"updated_at\": \"2018-04-04 23:42:48\",\n                    \"created_at\": \"2018-04-04 23:42:48\"\n                },\n                {\n                    \"id\": \"12679249513287680\",\n                    \"school_id\": \"12658531849342976\",\n                    \"dorm_id\": \"12679249458761728\",\n                    \"type\": \"twin\",\n                    \"accessible\": \"0\",\n                    \"smoking\": \"0\",\n                    \"updated_at\": \"2018-04-04 23:42:48\",\n                    \"created_at\": \"2018-04-04 23:42:48\"\n                }\n            ]\n            },\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.22.FindDormBySchoolId.php",
    "groupTitle": "School"
  },
  {
    "type": "GET",
    "url": "/school/fee/{id}",
    "title": "12. 查詢 單篇學校費用",
    "version": "0.1.0",
    "description": "<p>・ 查詢 單篇學校費用</p>",
    "name": "GetSchoolFindFee",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>學校 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_fee",
            "description": "<p>學校費用</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_fee\": {\n            \"id\": \"12661029259579392\",\n            \"school_id\": \"12658531849342976\",\n            \"tuition\": \"999\",\n            \"dorm\": \"10\",\n            \"unit\": \"week\",\n            \"currency\": \"twd\",\n            \"updated_at\": \"2018-04-04 22:37:20\",\n            \"created_at\": \"2018-04-04 22:30:24\"\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.12.FindFee.php",
    "groupTitle": "School"
  },
  {
    "type": "GET",
    "url": "/school/{id}/filter",
    "title": "32. 查詢 學校 Filter",
    "version": "0.1.0",
    "description": "<p>・ 查詢 學校 Filter</p>",
    "name": "GetSchoolFindFilter",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>學校 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.filder_ids",
            "description": "<p>學校 Filter IDs</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"3033560758758fb91623b61b0813475f\",\n        \"filder_ids\": [\n            \"13292119993225216\",\n            \"13292120089694208\",\n            \"13292120169385984\",\n            \"13292120207134720\",\n            \"13292120253272064\",\n            \"13292120349741056\"\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school filter error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.32.FindFilter.php",
    "groupTitle": "School"
  },
  {
    "type": "GET",
    "url": "/school/list/{status?}/{order_way?}/{page?}",
    "title": "05. 查詢 所有學校",
    "version": "0.1.0",
    "description": "<p>・ 查詢 所有學校</p>",
    "name": "GetSchoolFindList",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "all",
              "enable",
              "delete",
              "disable"
            ],
            "optional": true,
            "field": "status",
            "defaultValue": "enable",
            "description": "<p>學校狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"status\" : \"enable\",\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.schools",
            "description": "<p>學校</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"schools\": [\n            {\n                \"id\": \"12658531849342976\",\n                \"name\": \"test777\",\n                \"description\": \"test9999test9999test9999test9999test9999test9999test9999test9999test9999\",\n                \"facility\": \"qwerqwerqwerwqerqwerwqer\",\n                \"cover\": \"1522851523251513\",\n                \"status\": \"enable\",\n                \"creator_id\": \"11042496696160256\",\n                \"updated_at\": \"2018-04-04 22:20:29\",\n                \"created_at\": \"2018-04-04 22:20:29\",\n                \"slug\": \"test777\",\n                \"excerpt\": \"分類三\",\n                \"og_title\": \"分類三\",\n                \"og_description\": \"分類三分類三分類三分類三分類三分類三\",\n                \"meta_title\": \"分類三\",\n                \"meta_description\": \"分類三分類三分類三分類三分類三分類三\",\n                \"cover_title\": \"分類三\",\n                \"cover_alt\": \"分類三\",\n                \"cover_links\": {\n                    \"o\": \"gallery/school_12658531849342976/cover/1522851523251513_o.jpeg\"\n                },\n                \"image_links\": [\n                    {\n                        \"o\": \"gallery/school_12658531849342976/cover/1522851523251513_o.jpeg\"\n                    }\n                ],\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"/test777-s12658531849342976\"\n            },\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.05.FindList.php",
    "groupTitle": "School"
  },
  {
    "type": "GET",
    "url": "/school/user/{school_user_id}",
    "title": "26. 查詢 單篇學校使用者",
    "version": "0.1.0",
    "description": "<p>・ 查詢 單篇學校使用者</p>",
    "name": "GetSchoolFindUser",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_user_id",
            "description": "<p>學校使用者 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"school_user_id\" : \"12668924801978368\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_user",
            "description": "<p>學校使用者</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch school user success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_user\": {\n            \"id\": \"12691228839776256\",\n            \"school_id\": \"12658531849342976\",\n            \"user_id\": \"11042496696160256\",\n            \"role_id\": \"5324389994336256\",\n            \"type\": \"school\",\n            \"creator_id\": \"11042496696160256\",\n            \"updated_at\": \"2018-04-05 00:30:24\",\n            \"created_at\": \"2018-04-05 00:30:24\"\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"school_user_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.26.FindUser.php",
    "groupTitle": "School"
  },
  {
    "type": "GET",
    "url": "/school/{id}/user/{order_way?}/{page?}",
    "title": "27. 查詢 學校使用者 By School Id",
    "version": "0.1.0",
    "description": "<p>・ 查詢 學校使用者 By School Id</p>",
    "name": "GetSchoolFindUserBySchoolId",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>學校 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"12668924801978368\",\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_users",
            "description": "<p>學校使用者s</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch school users success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_users\": [\n            {\n                \"id\": \"12691228839776256\",\n                \"school_id\": \"12658531849342976\",\n                \"user_id\": \"11042496696160256\",\n                \"role_id\": \"5324389994336256\",\n                \"type\": \"school\",\n                \"creator_id\": \"11042496696160256\",\n                \"updated_at\": \"2018-04-05 00:30:24\",\n                \"created_at\": \"2018-04-05 00:30:24\"\n            },\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.27.FindUserBySchoolId.php",
    "groupTitle": "School"
  },
  {
    "type": "GET",
    "url": "/school/{id}/user/type/{type}/{order_way?}/{page?}",
    "title": "28. 查詢 學校使用者 By School Id And Type",
    "version": "0.1.0",
    "description": "<p>・ 查詢 學校使用者 By School Id And Type</p>",
    "name": "GetSchoolFindUserBySchoolIdAndType",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>學校 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "school",
              "company",
              "student"
            ],
            "optional": false,
            "field": "type",
            "description": "<p>使用者 type</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"12668924801978368\",\n    \"type\" : \"school\",\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_users",
            "description": "<p>學校使用者s</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch school users success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_users\": [\n            {\n                \"id\": \"12691228839776256\",\n                \"school_id\": \"12658531849342976\",\n                \"user_id\": \"11042496696160256\",\n                \"role_id\": \"5324389994336256\",\n                \"type\": \"school\",\n                \"creator_id\": \"11042496696160256\",\n                \"updated_at\": \"2018-04-05 00:30:24\",\n                \"created_at\": \"2018-04-05 00:30:24\"\n            },\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"type empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"type error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.28.FindUserBySchoolIdAndType.php",
    "groupTitle": "School"
  },
  {
    "type": "POST",
    "url": "/school",
    "title": "01. 新增學校",
    "version": "0.1.0",
    "description": "<p>・ 新增學校</p>",
    "name": "PostSchoolCreate",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>名稱</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "description",
            "description": "<p>描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "facility",
            "description": "<p>設備</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "cover",
            "description": "<p>封面</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "enable",
              "delete",
              "disable"
            ],
            "optional": true,
            "field": "status",
            "defaultValue": "enable",
            "description": "<p>狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "slug",
            "description": "<p>位址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "excerpt",
            "description": "<p>摘要</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "og_title",
            "description": "<p>og 標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "og_description",
            "description": "<p>og 描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "meta_title",
            "description": "<p>meta 標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "meta_description",
            "description": "<p>meta 描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "cover_title",
            "description": "<p>封面標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "cover_alt",
            "description": "<p>封面 alt</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"name\" : \"test777\",\n    \"description\" : \"test9999test9999test9999test9999test9999test9999test9999test9999test9999\",\n    \"cover\" : \"1522851523251513\",\n    \"facility\" : \"qwerqwerqwerwqerqwerwqer\",\n    \"status\" : \"enable\",\n    \"slug\" : \"test1\",\n    \"excerpt\" : \"test1test1test1test1test1\",\n    \"og_title\" : \"標題一\",\n    \"og_description\" : \"test1test1test1test1test1\",\n    \"meta_title\" : \"標題一\",\n    \"meta_description\" : \"test1test1test1test1test1\",\n    \"cover_title\" : \"標題一\",\n    \"cover_alt\" : \"標題一\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_id",
            "description": "<p>學校 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"create success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"name empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"cover empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.05",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"slug empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.06",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"excerpt empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.07",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"og_title empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.08",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"og_decription empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.09",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"meta_title empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.10",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"meta_description empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.11",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"cover_title empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.12",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"cover_alt empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 409.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 409,\n    \"comment\": \"slug error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"cover error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"dir error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"create error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.01.Create.php",
    "groupTitle": "School"
  },
  {
    "type": "POST",
    "url": "/school/course",
    "title": "13. 新增學校課程",
    "version": "0.1.0",
    "description": "<p>・ 新增學校課程</p>",
    "name": "PostSchoolCreateCourse",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_id",
            "description": "<p>學校 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>名稱</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "description",
            "description": "<p>描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "parent-child",
              "middle",
              "senior",
              "bachelor",
              "master",
              "doctoral",
              "adult"
            ],
            "optional": false,
            "field": "ta",
            "description": "<p>對象</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"school_id\" : \"12658531849342976\",\n    \"name\" : \"course 1\",\n    \"description\" : \"course 1course 1course 1course 1course 1course 1course 1course 1course 1course 1course 1\",\n    \"ta\" : \"senior\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_course_id",
            "description": "<p>學校課程 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"create school course success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_course_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"school_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"name empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.05",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"ta empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"ta error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"create school course error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.13.CreateCourse.php",
    "groupTitle": "School"
  },
  {
    "type": "POST",
    "url": "/school/course",
    "title": "18. 新增學校宿舍",
    "version": "0.1.0",
    "description": "<p>・ 新增學校宿舍</p>",
    "name": "PostSchoolCreateDorm",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_id",
            "description": "<p>學校 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "dorm",
              "hotel",
              "homestay"
            ],
            "optional": false,
            "field": "type",
            "description": "<p>宿舍類型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "service",
            "description": "<p>服務</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "facility",
            "description": "<p>設施</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "[]"
            ],
            "optional": false,
            "field": "rooms",
            "description": "<p>房型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "'single'",
              "'double'",
              "'triple'",
              "'quad'",
              "'twin'",
              "'double-double'",
              "'studio'",
              "'suit'",
              "'apartment'"
            ],
            "optional": false,
            "field": "rooms.N.type",
            "description": "<p>房型類型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "true",
              "false"
            ],
            "optional": true,
            "field": "rooms.N.accessible",
            "defaultValue": "true",
            "description": "<p>無障礙設施</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "true",
              "false"
            ],
            "optional": true,
            "field": "rooms.N.smoking",
            "defaultValue": "true",
            "description": "<p>吸菸</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"school_id\" : \"12658531849342976\",\n    \"type\" : \"hotel\",\n    \"service\" : \"course 1course 1course 1course 1course 1course 1course 1course 1course 1course 1course 1\",\n    \"facility\" : \"senior\",\n    \"rooms\" : [\n        {\n            \"type\" : \"single\"\n            \"accessible\" : \"false\"\n            \"smoking\" : \"false\"\n        },\n        {\n            \"type\" : \"twin\"\n            \"accessible\" : \"true\"\n            \"smoking\" : \"false\"\n        },\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_dorm_id",
            "description": "<p>學校宿舍 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"create school dorm success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_dorm_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"school_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"service empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.05",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"facility empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.06",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"rooms empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"type error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"all room type error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school dorm error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"create school dorm error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.18.CreateDorm.php",
    "groupTitle": "School"
  },
  {
    "type": "POST",
    "url": "/school/fee",
    "title": "10. 新增學校費用",
    "version": "0.1.0",
    "description": "<p>・ 新增學校費用</p>",
    "name": "PostSchoolCreateFee",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_id",
            "description": "<p>學校 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "tuition",
            "defaultValue": "0",
            "description": "<p>學費</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "dorm",
            "defaultValue": "0",
            "description": "<p>住宿</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "week",
              "month",
              "year",
              "acad-term"
            ],
            "optional": true,
            "field": "unit",
            "defaultValue": "week",
            "description": "<p>期間</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "usd",
              "gbp",
              "eur",
              "aud",
              "twd",
              "cny"
            ],
            "optional": true,
            "field": "currency",
            "defaultValue": "usd",
            "description": "<p>幣別</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"school_id\" : \"12658531849342976\",\n    \"tuition\" : \"9\",\n    \"dorm\" : \"10\",\n    \"unit\" : \"week\",\n    \"currency\" : \"twd\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_id",
            "description": "<p>學校 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"create success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"school_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"tuition empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.05",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"dorm empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"unit error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"currency error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 409.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 409,\n    \"comment\": \"school fee error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"create fee error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.10.CreateFee.php",
    "groupTitle": "School"
  },
  {
    "type": "POST",
    "url": "/school/filter",
    "title": "29. 新增學校 Filter",
    "version": "0.1.0",
    "description": "<p>・ 新增學校 Filter</p>",
    "name": "PostSchoolCreateFilter",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_id",
            "description": "<p>學校 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string[]",
            "optional": false,
            "field": "filter_ids",
            "description": "<p>filter Ids</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"school_id\" : \"1522851523251513\",\n    \"filter_ids\" : [\n        \"1522851523251513\",\n        \"1522851523251513\",\n        \"1522851523251513\"\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.filter_school_ids",
            "description": "<p>學校 filter IDs</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"create success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"filter_school_ids\": [\n            \"1522851523251513\",\n            \"1522851523251513\",\n            \"1522851523251513\"\n        ]\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"school_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"filter_ids empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"filter error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"create error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.29.CreateFilter.php",
    "groupTitle": "School"
  },
  {
    "type": "POST",
    "url": "/school/user",
    "title": "23. 新增學校使用者",
    "version": "0.1.0",
    "description": "<p>・ 新增學校使用者</p>",
    "name": "PostSchoolCreateUser",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_id",
            "description": "<p>學校 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_id",
            "description": "<p>使用者 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "role_id",
            "description": "<p>角色 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"school_id\" : \"12658531849342976\",\n    \"user_id\" : \"11042496696160256\",\n    \"role_id\" : \"5324389994336256\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_user_id",
            "description": "<p>學校使用者 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"create school user success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_user_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"school_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"user_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.05",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"role_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"user error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"role error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 409.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 409,\n    \"comment\": \"school user error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"create school user error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.23.CreateUser.php",
    "groupTitle": "School"
  },
  {
    "type": "POST",
    "url": "/school/filter/search",
    "title": "33. 搜尋學校 Filter",
    "version": "0.1.0",
    "description": "<p>・ 搜尋學校 Filter</p>",
    "name": "PostSchoolSearchFilter",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string[]",
            "optional": false,
            "field": "filter_ids",
            "description": "<p>filter Ids</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"filter_ids\" : [\n        \"1522851523251513\",\n        \"1522851523251513\",\n        \"1522851523251513\"\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.schools",
            "description": "<p>學校s</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"search success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"schools\": [\n{\n                \"id\": \"12658531849342976\",\n                \"name\": \"test777\",\n                \"description\": \"test9999test9999test9999test9999test9999test9999test9999test9999test9999\",\n                \"facility\": \"qwerqwerqwerwqerqwerwqer\",\n                \"cover\": \"1522851523251513\",\n                \"status\": \"enable\",\n                \"creator_id\": \"11042496696160256\",\n                \"updated_at\": \"2018-04-04 22:20:29\",\n                \"created_at\": \"2018-04-04 22:20:29\",\n                \"slug\": \"test777\",\n                \"excerpt\": \"分類三\",\n                \"og_title\": \"分類三\",\n                \"og_description\": \"分類三分類三分類三分類三分類三分類三\",\n                \"meta_title\": \"分類三\",\n                \"meta_description\": \"分類三分類三分類三分類三分類三分類三\",\n                \"cover_title\": \"分類三\",\n                \"cover_alt\": \"分類三\"\n            },\n        ]\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"filter_ids empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"filter error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.33.SearchFilter.php",
    "groupTitle": "School"
  },
  {
    "type": "POST",
    "url": "/school/cover",
    "title": "07. 上傳封面檔案",
    "version": "0.1.0",
    "description": "<p>・ 上傳封面檔案</p>",
    "name": "PostSchoolUploadCover",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "owner_id",
            "defaultValue": "0",
            "description": "<p>擁有者 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "public",
              "private"
            ],
            "optional": true,
            "field": "privacy_status",
            "defaultValue": "public",
            "description": "<p>隱私狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "enable",
              "disable",
              "delete"
            ],
            "optional": true,
            "field": "status",
            "defaultValue": "disable",
            "description": "<p>狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "file",
            "optional": false,
            "field": "file",
            "description": "<p>檔案</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"owner_id\" : \"1\",\n    \"privacy_status\" : \"public\",\n    \"status\" : \"disable\",\n    \"file\" : \"\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.filename",
            "description": "<p>檔案名稱</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.cover_links",
            "description": "<p>各尺寸圖檔連結</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"upload cover success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"filename\": \"1517067835270712\",\n        \"cover_links\": {\n            \"c\": \"gallery/1517067835270712_c.jpeg\",\n            \"z\": \"gallery/1517067835270712_z.jpeg\",\n            \"n\": \"gallery/1517067835270712_n.jpeg\",\n            \"q\": \"gallery/1517067835270712_q.jpeg\",\n            \"sq\": \"gallery/1517067835270712_sq.jpeg\",\n            \"o\": \"gallery/1517067835270712_o.jpeg\"\n        }\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"file empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"privacy_status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"file error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"create error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.07.UploadCover.php",
    "groupTitle": "School"
  },
  {
    "type": "POST",
    "url": "/school/{id}/image",
    "title": "09. 上傳圖片檔案",
    "version": "0.1.0",
    "description": "<p>・ 上傳圖片檔案</p>",
    "name": "PostSchoolUploadImage",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "public",
              "private"
            ],
            "optional": true,
            "field": "privacy_status",
            "defaultValue": "public",
            "description": "<p>隱私狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "enable",
              "disable",
              "delete"
            ],
            "optional": true,
            "field": "status",
            "defaultValue": "disable",
            "description": "<p>狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "file",
            "optional": false,
            "field": "file",
            "description": "<p>檔案</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "PUT"
            ],
            "optional": false,
            "field": "_method",
            "description": "<p>傳輸方法</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"privacy_status\" : \"public\",\n    \"status\" : \"disable\",\n    \"_method\" : \"PUT\",\n    \"file\" : \"\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.filename",
            "description": "<p>檔案名稱</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.image_links",
            "description": "<p>各尺寸圖檔連結</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"upload cover success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"filename\": \"1517067835270712\",\n        \"image_links\": {\n            \"c\": \"gallery/1517067835270712_c.jpeg\",\n            \"z\": \"gallery/1517067835270712_z.jpeg\",\n            \"n\": \"gallery/1517067835270712_n.jpeg\",\n            \"q\": \"gallery/1517067835270712_q.jpeg\",\n            \"sq\": \"gallery/1517067835270712_sq.jpeg\",\n            \"o\": \"gallery/1517067835270712_o.jpeg\"\n        }\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"file empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"privacy_status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"file error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"owner error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"owner status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"create error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.09.UploadImage.php",
    "groupTitle": "School"
  },
  {
    "type": "PUT",
    "url": "/school/{id}",
    "title": "02. 修改學校",
    "version": "0.1.0",
    "description": "<p>・ 修改學校</p>",
    "name": "PutSchoolUpdate",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>學校 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "name",
            "description": "<p>名稱</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "description",
            "description": "<p>描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "facility",
            "description": "<p>設備</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "cover",
            "description": "<p>封面</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "enable",
              "delete",
              "disable"
            ],
            "optional": true,
            "field": "status",
            "description": "<p>狀態</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"36946497446744064\",\n    \"name\" : \"test777\",\n    \"description\" : \"test9999test9999test9999test9999test9999test9999test9999test9999test9999\",\n    \"cover\" : \"1522851523251513\",\n    \"facility\" : \"qwerqwerqwerwqerqwerwqer\",\n    \"status\" : \"enable\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_id",
            "description": "<p>學校 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"update success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"cover error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"updateData empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"update error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.02.Update.php",
    "groupTitle": "School"
  },
  {
    "type": "PUT",
    "url": "/school/course/{school_course_id}",
    "title": "14. 修改學校課程",
    "version": "0.1.0",
    "description": "<p>・ 修改學校課程</p>",
    "name": "PutSchoolUpdateCourse",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_course_id",
            "description": "<p>學校課程 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "name",
            "description": "<p>名稱</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "description",
            "description": "<p>描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "parent-child",
              "middle",
              "senior",
              "bachelor",
              "master",
              "doctoral",
              "adult"
            ],
            "optional": true,
            "field": "ta",
            "description": "<p>對象</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"school_course_id\" : \"12658531849342976\",\n    \"name\" : \"course 1\",\n    \"description\" : \"course 1course 1course 1course 1course 1course 1course 1course 1course 1course 1course 1\",\n    \"ta\" : \"senior\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_course_id",
            "description": "<p>學校課程 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"update school course success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_course_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"school_course_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school course error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"ta error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"updateData empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"update school course error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.14.UpdateCourse.php",
    "groupTitle": "School"
  },
  {
    "type": "PUT",
    "url": "/school/dorm/{school_dorm_id}",
    "title": "19. 修改學校宿舍",
    "version": "0.1.0",
    "description": "<p>・ 修改學校宿舍</p>",
    "name": "PutSchoolUpdateDorm",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_dorm_id",
            "description": "<p>學校宿舍 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "dorm",
              "hotel",
              "homestay"
            ],
            "optional": true,
            "field": "type",
            "description": "<p>宿舍類型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "service",
            "description": "<p>服務</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "facility",
            "description": "<p>設施</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "[]"
            ],
            "optional": true,
            "field": "rooms",
            "description": "<p>房型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "'single'",
              "'double'",
              "'triple'",
              "'quad'",
              "'twin'",
              "'double-double'",
              "'studio'",
              "'suit'",
              "'apartment'"
            ],
            "optional": false,
            "field": "rooms.N.type",
            "description": "<p>房型類型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "true",
              "false"
            ],
            "optional": false,
            "field": "rooms.N.accessible",
            "description": "<p>無障礙設施</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "true",
              "false"
            ],
            "optional": false,
            "field": "rooms.N.smoking",
            "description": "<p>吸菸</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"school_dorm_id\" : \"12658531849342976\",\n    \"type\" : \"hotel\",\n    \"service\" : \"course 1course 1course 1course 1course 1course 1course 1course 1course 1course 1course 1\",\n    \"facility\" : \"senior\",\n    \"rooms\" : [\n        {\n            \"type\" : \"single\"\n            \"accessible\" : \"false\"\n            \"smoking\" : \"false\"\n        },\n        {\n            \"type\" : \"twin\"\n            \"accessible\" : \"false\"\n            \"smoking\" : \"false\"\n        },\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_dorm_id",
            "description": "<p>學校宿舍 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"update school dorm success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_dorm_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"school_dorm_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school dorm error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"type error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"updateData / updateRoomData empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"update school dorm error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.19.UpdateDorm.php",
    "groupTitle": "School"
  },
  {
    "type": "PUT",
    "url": "/school/{id}/fee",
    "title": "11. 修改學校費用",
    "version": "0.1.0",
    "description": "<p>・ 修改學校費用</p>",
    "name": "PutSchoolUpdateFee",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>學校 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "tuition",
            "description": "<p>學費</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "dorm",
            "description": "<p>住宿</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "week",
              "month",
              "year",
              "acad-term"
            ],
            "optional": true,
            "field": "unit",
            "description": "<p>期間</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "usd",
              "gbp",
              "eur",
              "aud",
              "twd",
              "cny"
            ],
            "optional": true,
            "field": "currency",
            "description": "<p>幣別</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"12658531849342976\",\n    \"tuition\" : \"9\",\n    \"dorm\" : \"10\",\n    \"unit\" : \"week\",\n    \"currency\" : \"twd\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_id",
            "description": "<p>學校 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"update fee success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school fee error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"unit error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"currency error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"updateData empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"update fee error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.11.UpdateFee.php",
    "groupTitle": "School"
  },
  {
    "type": "PUT",
    "url": "/school/{id}/filter",
    "title": "30. 修改學校 Filter",
    "version": "0.1.0",
    "description": "<p>・ 修改學校 Filter</p>",
    "name": "PutSchoolUpdateFilter",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>學校 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string[]",
            "optional": false,
            "field": "filter_ids",
            "description": "<p>filter ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"12658531849342976\",\n    \"filter_ids\" : [\n        \"1522851523251513\",\n        \"1522851523251513\",\n        \"1522851523251513\"\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.filter_school_ids",
            "description": "<p>學校 filter IDs</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"update success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"filter_school_ids\": [\n            \"1522851523251513\",\n            \"1522851523251513\",\n            \"1522851523251513\"\n        ]\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"filter_ids empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"filter error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"update error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.30.UpdateFilter.php",
    "groupTitle": "School"
  },
  {
    "type": "PUT",
    "url": "/school/{id}/seo",
    "title": "08. 修改學校 SEO",
    "version": "0.1.0",
    "description": "<p>・ 修改學校 SEO</p>",
    "name": "PutSchoolUpdateSeo",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>學校 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "slug",
            "description": "<p>位址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "excerpt",
            "description": "<p>摘要</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "canonical_url",
            "description": "<p>來源位址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "og_title",
            "description": "<p>og 標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "og_description",
            "description": "<p>og 描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "meta_title",
            "description": "<p>meta 標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "meta_description",
            "description": "<p>meta 描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "cover_title",
            "description": "<p>封面標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "cover_alt",
            "description": "<p>封面 alt</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"36946497446744064\",\n    \"slug\" : \"test1\",\n    \"excerpt\" : \"test1test1test1test1test1\",\n    \"canonical_url\" : \"http://abc.com/ask-poi\",\n    \"og_title\" : \"標題一\",\n    \"og_description\" : \"test1test1test1test1test1\",\n    \"meta_title\" : \"標題一\",\n    \"meta_description\" : \"test1test1test1test1test1\",\n    \"cover_title\" : \"標題一\",\n    \"cover_alt\" : \"標題一\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_id",
            "description": "<p>學校 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"update seo success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school seo error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 409.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 409,\n    \"comment\": \"slug error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"updateData empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"update error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.08.UpdateSeo.php",
    "groupTitle": "School"
  },
  {
    "type": "PUT",
    "url": "/school/user/{school_user_id}",
    "title": "24. 修改學校使用者",
    "version": "0.1.0",
    "description": "<p>・ 修改學校使用者</p>",
    "name": "PutSchoolUpdateUser",
    "group": "School",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_user_id",
            "description": "<p>學校使用者 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "user_id",
            "description": "<p>使用者 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "role_id",
            "description": "<p>角色 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"school_user_id\" : \"12658531849342976\",\n    \"user_id\" : \"11042496696160256\",\n    \"role_id\" : \"5324389994336256\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.school_user_id",
            "description": "<p>學校使用者  ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"update school user success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"school_user_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"school_user_id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"school user error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"user error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"role error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"updateData empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"update school user error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/School.24.UpdateUser.php",
    "groupTitle": "School"
  },
  {
    "type": "GET",
    "url": "/search/{term}",
    "title": "01. 搜尋",
    "version": "0.1.0",
    "description": "<p>・ 搜尋 文章 title/slug 及 作者 nickname/slug</p>",
    "name": "GetSearchFindByTerm",
    "group": "Search",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "term",
            "description": "<p>搜尋字串 (請先用 urlencode)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"term\"   : \"出國\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        },
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"posts\": [],\n        \"users\": [\n            {\n                \"id\": \"8220321387778048\",\n                \"title\": \"【英國留遊學】倫敦大學金匠學院\",\n                \"content\": \"<p>倫敦大學在台灣應該是個耳熟能詳的英國大學，畢竟民進黨的黨主席蔡英文女士就是畢業於倫敦大學的其中一個學院 &ndash; 倫敦政治經濟學院 （London School of Economics and Political Science），但是除了這個幾乎家喻戶曉的學院之外，其實聯邦制體系的倫敦大學還有其他專攻不同領域的院校，而我今天要介紹的就是專攻藝文與傳媒的金匠學院(Goldsmiths)。</p>\\n<p>如果你喜歡英國當代藝術，或是你有些許研究的話，你應該會有聽過The Young British Artists（簡稱YBAs）這個名稱，他們是一群崛起於80年代的藝術家們，並且在90年代在藝術界造成了一陣轟動，也深深影響了現在的當代藝術。在這群藝術家中有多位都是畢業於金匠學院的藝術科系，例如，在2007年推出全世界最貴的藝術作品 For the love of God的Damien Hirst跟以諷刺手法來表現兩性觀點的Sarah Lucas，他們都是金匠學院的校友。如果你喜歡時尚的話，你一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，你不知道的是，Vivian Westwood也是畢業於金匠學院的設計科系。另外，金匠學院的大眾傳播學系(Media and Communication)也是國際知名的學校，像韓國的導演也都曾來此就讀。可見金匠學院在藝文界真的是有舉足輕重的地位，也培育出了許多赫赫有名的人才。</p>\\n<p>在當年就是衝著這股濃濃藝文味，於是我選擇到了金匠學院，主修大眾傳播媒體，在就讀的期間也感受到了學校的優點與缺點，在這裡就跟可能有計畫來就讀的你分享一下身為校友的個人的經驗吧！</p>\\n<p>首先，有一件事你一定要先知道，在英國，大學的<strong>就讀時間大部分為三年</strong>，也就是讀完三年沒被當的話就能夠順利畢業，另外學校採用的是<strong>三學期制</strong>，跟在台灣熟悉的二學期制不太一樣，雖然這樣假期似乎看起來會比較多，但是其實課業量反而更繁重，因為相同的課程就變成要在較短的時間內學會，而且每一次的假期也就代表著一次的報告dead line，所以，想玩就必須好好做時間規劃，把時間規劃好，才能夠順利繳交報告跟同時享有假期啊！</p>\\n<p>另外，我覺得倫敦大學的學校，相對於倫敦藝術大學，會顯得比較著重於理論，因為我有認識的朋友是就讀於倫敦藝術大學，他們時常都是忙於繳交作品，個人覺得是比較偏向實作方面，但是在金匠學院他們是採取<strong>理論與實作並重</strong>的方式來教課，也就是說作品量的累積跟實作的經驗比起倫敦藝術大學的同學可能會有些落差，但是在理論上知識的基礎可能會更加深厚。會這樣著重理論，是因為他們認為在知識上有著較深厚的基礎就能夠培養出批判性的思考能力(critical thinking)，創造出來的作品會更有內涵並能利用學到的知識來引導觀者，藉以得到更大的共鳴。</p>\\n<p>在大眾傳播的實作課程中，你可以從八種不同的課程來做選擇，分別為：</p>\\n<p>Journalism</p>\\n<p>Photography</p>\\n<p>Filmmaking</p>\\n<p>Illustration</p>\\n<p>Script and Props</p>\\n<p>Animation</p>\\n<p>Radio</p>\\n<p>Interactive Media</p>\\n<p>在一年級的時候你可以選修四種實作課程，但是到了二年級只能選修兩種，到了三年級就必須專精於其中一項，應該是希望透過嘗試讓學生找到自己有興趣或是有潛能的課程，再加以訓練跟培養。</p>\\n<p>我一年級時嘗試了animation，photography，filmmaking跟illustration，二年級的時候選擇了動畫跟攝影，到三年級就專精於攝影。其中動畫課在一二年級的時候，我們學習到的是stop-motion的製作方式，還有如何製作story board，但是到了三年級會學習到使用After Effect這個軟體來製作動畫。filmmaking其實很有趣，你會跟著一個團隊一起合作，可以嘗試到像採訪陌生人，或是自己尋找演員、找場地拍攝，甚至是製作配樂等經驗。在攝影課我們使用的幾乎都是底片相機，學校都有設備可以出借，但是有規定的出借跟歸還時間。在課堂上你會學到關於攝影的理論跟實際操作器具的體驗，不只是相機的操作，還有燈光的調整，底片的沖洗等等。基本上每學年你都需要製作一個自己的project，主題很自由，老師可能會給你一個大方向，但是你可以自己決定主題去製作。</p>\\n<p>不過，專精於其中一項不代表你只能侷限自己做某一個課程，相反的，學校反而鼓勵學生互相合作、學習，甚至跟不同科系的學生也都能夠互相合作來籌畫活動，推行企劃。但是，這相互間的合作通常是要靠個人去籌劃跟聯繫，學校並不會當中間人或指導人，<strong>主動性的培養</strong>，我覺得這是臺灣跟國外學校的一個最主要的差別吧！老師們，可能會提出你可以籌辦活動的想法，但是做與不做就全權交給你去決定，如果要做了，你有問題都可以主動提出，他們也都會很樂意幫助你，但比較不會主動去督促你的進度。像上必修課也都是一樣的，基本上都是lecture接著上seminar，書籍都是一開學就發放給你，在上課前你必須自己先研讀，到學校講師在講課，講課的內容會以書籍裡的內容為基礎，然後延伸出其他更深更廣的理論，有問題再在seminar上提出討論，老師並不會帶著你唸課文，所以如果你事前預習沒做好，在講師講課的時候可能就已經被搞得一頭霧水了。</p>\\n<p>如果你在大學裡期待的是非常多的實作經驗，還有學校幫你選擇好的實習課程，那金匠大學可能不是屬於你的選擇，但是如果你想把底子學好跟喜歡挑戰非商業性的創作，靠自己去尋找實習機會，金匠大學就會是屬於你的不二選擇喔～</p>\",\n                \"cover\": \"1519267601292148\",\n                \"privacy_status\": \"public\",\n                \"status\": \"publish\",\n                \"recommended\": \"0\",\n                \"blog_id\": \"8217810404773888\",\n                \"postcategory_id\": \"7859246410633216\",\n                \"owner_id\": \"8217810346053632\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:24:37\",\n                \"created_at\": \"2018-03-23 16:24:37\",\n                \"slug\": \"英國留遊學-倫敦大學金匠學院\",\n                \"excerpt\": \"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。\",\n                \"og_title\": \"【英國留遊學】倫敦大學金匠學院\",\n                \"og_description\": \"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。\",\n                \"meta_title\": \"【英國留遊學】倫敦大學金匠學院\",\n                \"meta_description\": \"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。\",\n                \"cover_title\": \"倫敦大學金匠學院\",\n                \"cover_alt\": \"倫敦大學金匠學院\",\n                \"postcategory\": {\n                    \"id\": \"7859246410633216\",\n                    \"type\": \"global\",\n                    \"name\": \"英國留學專欄\",\n                    \"cover\": \"1519872393361401\",\n                    \"status\": \"enable\",\n                    \"num_items\": \"0\",\n                    \"owner_id\": \"0\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-22 16:29:50\",\n                    \"created_at\": \"2018-03-22 16:29:50\",\n                    \"slug\": \"study-aboard-UK\",\n                    \"excerpt\": \"出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。\",\n                    \"og_title\": \"《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！\",\n                    \"og_description\": \"出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。\",\n                    \"meta_title\": \"《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！\",\n                    \"meta_description\": \"出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。\",\n                    \"cover_title\": \"出國留學\",\n                    \"cover_alt\": \"出國留學\"\n                },\n                \"owner\": {\n                    \"id\": \"8217810346053632\",\n                    \"account\": \"david@english.agency\",\n                    \"password\": \"$2y$10$5zcXhEAiHzYwfy5Vs7m5tObjcu3cgbvciFA5Myz4cmWgpP.ISSPWW\",\n                    \"status\": \"enable\",\n                    \"token\": \"070c01d7-a9c5-5614-b9ac-8f14b53ee5a7\",\n                    \"creator_id\": \"0\",\n                    \"updated_at\": \"2018-03-23 16:14:38\",\n                    \"created_at\": \"2018-03-23 16:14:38\",\n                    \"slug\": \"david_b\",\n                    \"excerpt\": \"曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。\",\n                    \"og_title\": \"\\\"David B\\\"\",\n                    \"og_description\": \"曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。\",\n                    \"meta_title\": \"\\\"David B\\\"\",\n                    \"meta_description\": \"曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。\",\n                    \"avatar_title\": \"\\\"David B\\\"\",\n                    \"avatar_alt\": \"\\\"David B\\\"\"\n                },\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"/英國留遊學-倫敦大學金匠學院-p8220321387778048\"\n            }\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ],
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.posts",
            "description": "<p>文章</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.users",
            "description": "<p>作者有關文章</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"term empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Search.01.findByTerm.php",
    "groupTitle": "Search"
  },
  {
    "type": "GET",
    "url": "/session/{code}",
    "title": "02. 用 code 查詢 session",
    "version": "0.1.0",
    "description": "<p>・ 用 code 查詢 session。</p>",
    "name": "GetSessionFindByCode",
    "group": "Session",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "code",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"code\" : \"8f3e973061800dc6ebcb367079b305d8\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.status",
            "description": "<p>Session 狀態</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"status\": \"login\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"code empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"session error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Session.02.FindByCode.php",
    "groupTitle": "Session"
  },
  {
    "type": "POST",
    "url": "/session/init",
    "title": "01. 初始化",
    "version": "0.1.0",
    "description": "<p>・ 用 device_code, device_os, device_type, device_lang, device_token, device_channel 初始化 session<br /> ・ 若不傳 device_code 或為空值，則自行產生<br /> ・ 若不傳 device_channel 或為空值，則依 device_os 產生相對應的值</p>",
    "name": "PostSessionInit",
    "group": "Session",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "device_code",
            "description": "<p>裝置代碼</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ios",
              "andriod",
              "macos",
              "windows",
              "others"
            ],
            "optional": true,
            "field": "device_os",
            "defaultValue": "others",
            "description": "<p>裝置作業系統</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "iphone",
              "ipad",
              "mobile",
              "pc",
              "others"
            ],
            "optional": true,
            "field": "device_type",
            "defaultValue": "others",
            "description": "<p>裝置型態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "zh-tw",
              "zh-cn",
              "jp",
              "en"
            ],
            "optional": true,
            "field": "device_lang",
            "defaultValue": "en",
            "description": "<p>裝置語言</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "device_token",
            "description": "<p>裝置 token</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "apns",
              "gcm",
              "baidu",
              "others"
            ],
            "optional": true,
            "field": "device_channel",
            "description": "<p>裝置 訊息頻道</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"device_code\" : \"b4233639-0a61-4b0f-9ab3-682066ded25a\",\n    \"device_os\"   : \"ios\",\n    \"device_type\" : \"iphone\" ,\n    \"device_lang\" : \"en\",\n    \"device_token\" : \"b4233639-0a61-4b0f-9ab3-682066ded25a\",\n    \"device_channel\" : \"apns\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.device_code",
            "description": "<p>裝置代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"init success\",\n    \"data\": {\n        \"device_code\": \"aeb0c92d-c9fa-47ea-814e-8e009c1d3056\",\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"device os error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"device type error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"device lang error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"device channel error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Session.01.Init.php",
    "groupTitle": "Session"
  },
  {
    "type": "GET",
    "url": "/subscribe/subscribers",
    "title": "02. 查詢 所有訂閱者",
    "version": "0.1.0",
    "description": "<p>・ 查詢 所有訂閱者。</p>",
    "name": "GetSubscribeFindByStatusSubscribe",
    "group": "Subscribe",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.subscribers",
            "description": "<p>訂閱者</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"subscribers\": [\n            {\n                \"id\": \"1\",\n                \"name\": \"John\",\n                \"email\": \"jasechen@gmail.com\",\n                \"status\": \"subscribe\",\n                \"updated_at\": \"2018-01-22 11:28:55\",\n                \"created_at\": \"2018-01-22 11:28:55\"\n            },\n            {\n                \"id\": \"2\",\n                \"name\": \"John\",\n                \"email\": \"jase.chen@gmail.com\",\n                \"status\": \"subscribe\",\n                \"updated_at\": \"2018-01-22 11:28:55\",\n                \"created_at\": \"2018-01-22 11:28:55\"\n            },\n            ...\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Subscribe.02.FindByStatus.php",
    "groupTitle": "Subscribe"
  },
  {
    "type": "POST",
    "url": "/subscribe",
    "title": "01. 訂閱 EDM",
    "version": "0.1.0",
    "description": "<p>・ 用 email 訂閱 EDM</p>",
    "name": "PostSubscribeCreate",
    "group": "Subscribe",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": true,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>姓名</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": "<p>E-Mail</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"name\" : \"John\",\n    \"email\" : \"account@abc.com\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.subscriber_id",
            "description": "<p>訂閱 EDM ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"create success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"subscriber_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"name empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"email empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"email format error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 409.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 409,\n    \"comment\": \"email already exist\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"create error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/Subscribe.01.Create.php",
    "groupTitle": "Subscribe"
  },
  {
    "type": "DELETE",
    "url": "/user/{id}",
    "title": "03. 刪除使用者",
    "version": "0.1.0",
    "description": "<p>・ 刪除使用者</p>",
    "name": "DeleteUserDelete",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>使用者 ID                                        [slug]             位址</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"36946497446744064\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.user_id",
            "description": "<p>使用者 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"delete success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"user_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"user error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"delete error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/User.03.Delete.php",
    "groupTitle": "User"
  },
  {
    "type": "GET",
    "url": "/user/{id}",
    "title": "04. 查詢 單一使用者",
    "version": "0.1.0",
    "description": "<p>・ 查詢 單一使用者</p>",
    "name": "GetUserFind",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>使用者 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.user",
            "description": "<p>使用者</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"user\": {\n            \"id\": \"8217809167454208\",\n            \"account\": \"admin@english.agency\",\n            \"password\": \"$2y$10$yEX7E/.ueop/dbbgBP9qy.4vdobMHcR6diBG.pQKrOBiT61.cADaG\",\n            \"status\": \"enable\",\n            \"token\": \"cd315b42-4108-52f2-92ce-86a76007dfe7\",\n            \"creator_id\": \"0\",\n            \"updated_at\": \"2018-03-23 16:14:38\",\n            \"created_at\": \"2018-03-23 16:14:38\",\n            \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n            \"url_path\": \"\"\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/User.04.Find.php",
    "groupTitle": "User"
  },
  {
    "type": "GET",
    "url": "/user/slug/{slug}",
    "title": "06. 查詢 單一使用者 By Slug",
    "version": "0.1.0",
    "description": "<p>・ 查詢 單一使用者 By Slug</p>",
    "name": "GetUserFindBySlug",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "slug",
            "description": "<p>使用者 Slug</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"slug\" : \"user-admin\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.user",
            "description": "<p>使用者</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"user\": {\n            \"id\": \"8217809167454208\",\n            \"account\": \"admin@english.agency\",\n            \"password\": \"$2y$10$yEX7E/.ueop/dbbgBP9qy.4vdobMHcR6diBG.pQKrOBiT61.cADaG\",\n            \"status\": \"enable\",\n            \"token\": \"cd315b42-4108-52f2-92ce-86a76007dfe7\",\n            \"creator_id\": \"0\",\n            \"updated_at\": \"2018-03-23 16:14:38\",\n            \"created_at\": \"2018-03-23 16:14:38\",\n            \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n            \"url_path\": \"\"\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"slug empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/User.06.FindBySlug.php",
    "groupTitle": "User"
  },
  {
    "type": "GET",
    "url": "/user/token/{token}",
    "title": "07. 查詢 單一使用者 By Token",
    "version": "0.1.0",
    "description": "<p>・ 查詢 單一使用者 By Token</p>",
    "name": "GetUserFindByToken",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>使用者 Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"token\" : \"cd315b42-4108-52f2-92ce-86a76007dfe7\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.user",
            "description": "<p>使用者</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"user\": {\n            \"id\": \"8217809167454208\",\n            \"account\": \"admin@english.agency\",\n            \"password\": \"$2y$10$yEX7E/.ueop/dbbgBP9qy.4vdobMHcR6diBG.pQKrOBiT61.cADaG\",\n            \"status\": \"enable\",\n            \"token\": \"cd315b42-4108-52f2-92ce-86a76007dfe7\",\n            \"creator_id\": \"0\",\n            \"updated_at\": \"2018-03-23 16:14:38\",\n            \"created_at\": \"2018-03-23 16:14:38\",\n            \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n            \"url_path\": \"\"\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/User.07.FindByToken.php",
    "groupTitle": "User"
  },
  {
    "type": "GET",
    "url": "/user/list/{status?}/{order_way?}/{page?}",
    "title": "05. 查詢 所有使用者",
    "version": "0.1.0",
    "description": "<p>・ 查詢 所有使用者</p>",
    "name": "GetUserFindList",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "all",
              "enable",
              "delete",
              "disable"
            ],
            "optional": true,
            "field": "status",
            "defaultValue": "enable",
            "description": "<p>使用者狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "ASC",
              "DESC"
            ],
            "optional": true,
            "field": "order_way",
            "defaultValue": "ASC",
            "description": "<p>排列升降冪</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "page",
            "defaultValue": "-1",
            "description": "<p>頁數</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"status\" : \"enable\",\n    \"order_way\" : \"ASC\",\n    \"page\" : \"-1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.users",
            "description": "<p>使用者s</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 200",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 200,\n    \"comment\": \"fetch success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"users\": [\n            {\n                \"id\": \"8217809167454208\",\n                \"account\": \"admin@english.agency\",\n                \"password\": \"$2y$10$yEX7E/.ueop/dbbgBP9qy.4vdobMHcR6diBG.pQKrOBiT61.cADaG\",\n                \"status\": \"enable\",\n                \"token\": \"cd315b42-4108-52f2-92ce-86a76007dfe7\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:14:38\",\n                \"created_at\": \"2018-03-23 16:14:38\",\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"\"\n            },\n            {\n                \"id\": \"8217809599467520\",\n                \"account\": \"alexs@english.agency\",\n                \"password\": \"$2y$10$dTEwxbx0Q5pvkJhjyYndCehQRHk0epqGD/yWMUCcDjmm7HODAJD/q\",\n                \"status\": \"enable\",\n                \"token\": \"fcf7727b-1ea5-5549-a8cf-40c6a852c30d\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:14:38\",\n                \"created_at\": \"2018-03-23 16:14:38\",\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"\"\n            },\n            {\n                \"id\": \"8217809985343488\",\n                \"account\": \"magic@english.agency\",\n                \"password\": \"$2y$10$MRLFkU3Px7ckRBTxwj2DM.xcL70c.PwP2lqZGeYDKFAxL49is7Lzi\",\n                \"status\": \"enable\",\n                \"token\": \"5b01417c-522b-5fa4-bf80-1e86c2201832\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:14:38\",\n                \"created_at\": \"2018-03-23 16:14:38\",\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"\"\n            },\n            {\n                \"id\": \"8217810346053632\",\n                \"account\": \"david@english.agency\",\n                \"password\": \"$2y$10$5zcXhEAiHzYwfy5Vs7m5tObjcu3cgbvciFA5Myz4cmWgpP.ISSPWW\",\n                \"status\": \"enable\",\n                \"token\": \"070c01d7-a9c5-5614-b9ac-8f14b53ee5a7\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:14:38\",\n                \"created_at\": \"2018-03-23 16:14:38\",\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"\"\n            },\n            {\n                \"id\": \"8217810706763776\",\n                \"account\": \"vacca@english.agency\",\n                \"password\": \"$2y$10$GH30Glfuxz0lrQiSemmKjuXHL/ay7TbO/o84bvbwPcdVv7JGdfVH.\",\n                \"status\": \"enable\",\n                \"token\": \"22874266-9ad8-5bc3-ae3d-6b20996ac576\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:14:38\",\n                \"created_at\": \"2018-03-23 16:14:38\",\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"\"\n            },\n            {\n                \"id\": \"8217811075862528\",\n                \"account\": \"bacio@english.agency\",\n                \"password\": \"$2y$10$3oV.jiNtXHnqgds39kaxOeiTNARl0A0Z2mfeEzQeP51krjZBRLTjC\",\n                \"status\": \"enable\",\n                \"token\": \"fdfe7acc-8097-519f-a8fd-60c49b5bc189\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:14:38\",\n                \"created_at\": \"2018-03-23 16:14:38\",\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"\"\n            },\n            {\n                \"id\": \"8217811432378368\",\n                \"account\": \"gemora@english.agency\",\n                \"password\": \"$2y$10$JJrQtFPbZAh.VssdUkchQeaRgWMA62HsTdzg.7eU2VQEPDiUM0yjC\",\n                \"status\": \"enable\",\n                \"token\": \"68308d20-f309-5d9f-baa3-528389747854\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:14:38\",\n                \"created_at\": \"2018-03-23 16:14:38\",\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"\"\n            },\n            {\n                \"id\": \"8217811793088512\",\n                \"account\": \"rabo@english.agency\",\n                \"password\": \"$2y$10$MLqwbWJYndct44a2Ti6Q4eRuwY97B84MG2qtXoDHzwAYvh9t9lG.O\",\n                \"status\": \"enable\",\n                \"token\": \"295a5e7a-0e14-5ebd-b8f3-7e84300848ce\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:14:39\",\n                \"created_at\": \"2018-03-23 16:14:39\",\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"\"\n            },\n            {\n                \"id\": \"8217812157992960\",\n                \"account\": \"globaljobs@english.agency\",\n                \"password\": \"$2y$10$KFhXj9W1nIJWOnkgadlhg.wtUxma0yutBlqFo0RPEtTu7cuAX590q\",\n                \"status\": \"enable\",\n                \"token\": \"e5405c9b-8a0e-5332-94de-0208cd92a3e7\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:14:39\",\n                \"created_at\": \"2018-03-23 16:14:39\",\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"\"\n            },\n            {\n                \"id\": \"8217812506120192\",\n                \"account\": \"toeic@english.agency\",\n                \"password\": \"$2y$10$QuLXGH33wnx6S6s7PzSnw.buhFbDDhAbGq3pZ6K6M0.qD2osH52GS\",\n                \"status\": \"enable\",\n                \"token\": \"1a661b2d-db14-5540-ae65-bff88e20e654\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:14:39\",\n                \"created_at\": \"2018-03-23 16:14:39\",\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"\"\n            },\n            {\n                \"id\": \"8217812858441728\",\n                \"account\": \"autumn@english.agency\",\n                \"password\": \"$2y$10$XIw3Hm9wA.QAUWTpxH1GlOSEUrZcbvRcQ8OObU03YOEGlUn572sQ2\",\n                \"status\": \"enable\",\n                \"token\": \"05a12268-f836-502a-901b-fc5f06c2e566\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:14:39\",\n                \"created_at\": \"2018-03-23 16:14:39\",\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"\"\n            },\n            {\n                \"id\": \"8217813202374656\",\n                \"account\": \"alya@english.agency\",\n                \"password\": \"$2y$10$Fg84g9DsonIJCXpdLHdkC.qddD2jEj/UUzOgLe5kK3Nskx86/BFfC\",\n                \"status\": \"enable\",\n                \"token\": \"e2c63cf7-6ddb-5e84-bb32-31a31278cf4b\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:14:39\",\n                \"created_at\": \"2018-03-23 16:14:39\",\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"\"\n            },\n            {\n                \"id\": \"8217813571473408\",\n                \"account\": \"toefl@english.agency\",\n                \"password\": \"$2y$10$IEB8MrgmPaScblLyxzbLgeCnNOaEMKUBolM32hskDRgJLH91US73a\",\n                \"status\": \"enable\",\n                \"token\": \"1212d5ff-993e-5754-a609-0c020f98359c\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:14:39\",\n                \"created_at\": \"2018-03-23 16:14:39\",\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"\"\n            },\n            {\n                \"id\": \"8217813932183552\",\n                \"account\": \"luna@english.agency\",\n                \"password\": \"$2y$10$S8VRZ0odLFSyQYBKbRjwLO8LTJgEbXMk1LsyBNdNsuODoMX4dBLU6\",\n                \"status\": \"enable\",\n                \"token\": \"14bcc8af-eb01-5e81-b0e1-3d3bab4cea2f\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:14:39\",\n                \"created_at\": \"2018-03-23 16:14:39\",\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"\"\n            },\n            {\n                \"id\": \"8217814297088000\",\n                \"account\": \"jasmine@english.agency\",\n                \"password\": \"$2y$10$4XQn4RQ2N51qzPJ5V.ml1Om9TocKVx9/K/CYyRc4KY8y36xzgjNzO\",\n                \"status\": \"enable\",\n                \"token\": \"da6c3c3c-d8ae-56de-9e9f-d0feeb42571a\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:14:39\",\n                \"created_at\": \"2018-03-23 16:14:39\",\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"\"\n            },\n            {\n                \"id\": \"8217814678769664\",\n                \"account\": \"claire@english.agency\",\n                \"password\": \"$2y$10$b4RqTLd.CA4/0r1LPt5vZOWJ.j61tnh2v5aCi9ZOZNp5K60J2dK2W\",\n                \"status\": \"enable\",\n                \"token\": \"45713cc3-a826-5b4b-99f7-042f17c7e817\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:14:39\",\n                \"created_at\": \"2018-03-23 16:14:39\",\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"\"\n            },\n            {\n                \"id\": \"8217815043674112\",\n                \"account\": \"winnie@english.agency\",\n                \"password\": \"$2y$10$u4AjL6tK4dcwmRjtQHtNCO0XTxESMQXss2UiFuusyVCbwe12NTAna\",\n                \"status\": \"enable\",\n                \"token\": \"4d5bff58-b363-5010-88f1-9303e4a6fa40\",\n                \"creator_id\": \"0\",\n                \"updated_at\": \"2018-03-23 16:14:39\",\n                \"created_at\": \"2018-03-23 16:14:39\",\n                \"s3_url\": \"https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery\",\n                \"url_path\": \"\"\n            }\n        ]\n    }\n}",
          "type": "json"
        },
        {
          "title": "Response: 204",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"order_way error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/User.05.FindList.php",
    "groupTitle": "User"
  },
  {
    "type": "GET",
    "url": "/user/{id}/email/notify",
    "title": "13. 重寄 E-Mail 驗證信",
    "version": "0.1.0",
    "description": "<p>・ 重寄 E-Mail 驗證信</p>",
    "name": "GetUserNotifyEmail",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>使用者 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 202",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 202,\n    \"comment\": \"notify success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"user error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"user status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"user email check error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"user check status error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/User.13.NotifyEmail.php",
    "groupTitle": "User"
  },
  {
    "type": "GET",
    "url": "/user/{id}/mobile/notify",
    "title": "15. 重寄 Mobile 驗證簡訊",
    "version": "0.1.0",
    "description": "<p>・ 重寄 Mobile 驗證簡訊</p>",
    "name": "GetUserNotifyMobile",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>使用者 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 202",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 202,\n    \"comment\": \"notify success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"user error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"user status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"user mobile check error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"user check status error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/User.15.NotifyMobile.php",
    "groupTitle": "User"
  },
  {
    "type": "POST",
    "url": "/user",
    "title": "01. 新增使用者",
    "version": "0.1.0",
    "description": "<p>・ 新增使用者</p>",
    "name": "PostUserCreate",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "account",
            "description": "<p>使用者帳號</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>密碼</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password_repeat",
            "description": "<p>確認密碼</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "init",
              "enable",
              "disable",
              "delete"
            ],
            "optional": true,
            "field": "status",
            "defaultValue": "init",
            "description": "<p>狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "first_name",
            "description": "<p>名</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "last_name",
            "description": "<p>姓</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mobile_country_code",
            "description": "<p>手機號碼國碼</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mobile_phone",
            "description": "<p>手機號碼</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "slug",
            "description": "<p>位址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "excerpt",
            "description": "<p>摘要</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "og_title",
            "description": "<p>og 標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "og_description",
            "description": "<p>og 描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "meta_title",
            "description": "<p>meta 標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "meta_description",
            "description": "<p>meta 描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "avatar_title",
            "description": "<p>頭像標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "avatar_alt",
            "description": "<p>頭像 alt</p>"
          },
          {
            "group": "Parameter",
            "type": "string[]",
            "optional": true,
            "field": "role_ids",
            "description": "<p>角色 IDs (default:type=general,code=mamber)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"account\"   : \"service@showhi.co\",\n    \"password\"  : \"12345678\" ,\n    \"password_repeat\" : \"12345678\",\n    \"first_name\" : \"John\",\n    \"last_name\"   : \"Wu\",\n    \"mobile_country_code\" : \"886\",\n    \"mobile_phone\" : \"912345678\",\n    \"status\" : \"init\",\n    \"slug\" : \"test1\",\n    \"excerpt\" : \"test1test1test1test1test1\",\n    \"og_title\" : \"標題一\",\n    \"og_description\" : \"test1test1test1test1test1\",\n    \"meta_title\" : \"標題一\",\n    \"meta_description\" : \"test1test1test1test1test1\",\n    \"avatar_title\" : \"標題一\",\n    \"avatar_alt\" : \"標題一\",\n     \"role_ids\" : [\n        \"5324389482631168\",\n        \"5324389604265984\",\n        \"5324389721706496\",\n        \"5324389788815360\",\n        \"5324389847535616\"\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.user_id",
            "description": "<p>使用者 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"create success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"user_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"account empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"password / password_repeat empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.05",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"first_name empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.06",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"last_name empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.07",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"mobile_country_code empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.08",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"mobile_phone empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.09",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"slug empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.10",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"excerpt empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.11",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"og_title empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.12",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"og_decription empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.013",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"meta_title empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.14",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"meta_description empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.15",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"avatar_title empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.16",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"avatar_alt empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"account error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"password' length < 8\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"password / password_repeat is NOT equal\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.05",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"mobile_phone format error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 409.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 409,\n    \"comment\": \"account error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 409.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 409,\n    \"comment\": \"mobile_phone error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.06",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 409.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 409,\n    \"comment\": \"slug error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"create user error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/User.01.Create.php",
    "groupTitle": "User"
  },
  {
    "type": "POST",
    "url": "/user/password/notify",
    "title": "17. 寄 忘記密碼通知信",
    "version": "0.1.0",
    "description": "<p>・ 寄 忘記密碼通知信</p>",
    "name": "PostUserNotifyPassword",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "account",
            "description": "<p>使用者帳號</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"account\" : \"account@abc.com\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 202",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 202,\n    \"comment\": \"notify success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"account empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"account error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"user error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"user status error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/User.17.NotifyPassword.php",
    "groupTitle": "User"
  },
  {
    "type": "POST",
    "url": "/user/{id}/avatar",
    "title": "10. 上傳使用者頭像",
    "version": "0.1.0",
    "description": "<p>・ 上傳使用者頭像</p>",
    "name": "PostUserUpdateAvatar",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>使用者 id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "public",
              "private"
            ],
            "optional": true,
            "field": "privacy_status",
            "defaultValue": "public",
            "description": "<p>隱私狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "enable",
              "disable",
              "delete"
            ],
            "optional": true,
            "field": "status",
            "defaultValue": "disable",
            "description": "<p>狀態</p>"
          },
          {
            "group": "Parameter",
            "type": "file",
            "optional": false,
            "field": "file",
            "description": "<p>檔案</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "PUT"
            ],
            "optional": false,
            "field": "_method",
            "description": "<p>傳輸方法</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"privacy_status\" : \"public\",\n    \"status\" : \"disable\",\n    \"_method\" : \"PUT\",\n    \"file\" : \"\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.filename",
            "description": "<p>檔案名稱</p>"
          },
          {
            "group": "Success",
            "type": "string[]",
            "optional": false,
            "field": "data.avatar_links",
            "description": "<p>各尺寸圖檔連結</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"upload avatar success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"filename\": \"1517067835270712\",\n        \"avatar_links\": {\n            \"c\": \"gallery/user_10335147731849216/avatar/1517067835270712_c.jpeg\",\n            \"z\": \"gallery/user_10335147731849216/avatar/1517067835270712_z.jpeg\",\n            \"n\": \"gallery/user_10335147731849216/avatar/1517067835270712_n.jpeg\",\n            \"q\": \"gallery/user_10335147731849216/avatar/1517067835270712_q.jpeg\",\n            \"sq\": \"gallery/user_10335147731849216/avatar/1517067835270712_sq.jpeg\",\n            \"o\": \"gallery/user_10335147731849216/avatar/1517067835270712_o.jpeg\"\n        }\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"file empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"privacy_status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"file error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"owner error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.05",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"owner status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"create error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/User.10.UpdateAvatar.php",
    "groupTitle": "User"
  },
  {
    "type": "PUT",
    "url": "/user/{id}/password/reset",
    "title": "18. 重設密碼",
    "version": "0.1.0",
    "description": "<p>・ 重設密碼</p>",
    "name": "PutUserResetPassword",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>使用者 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "account",
            "description": "<p>使用者帳號</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>密碼</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password_repeat",
            "description": "<p>重設密碼</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "check_code",
            "description": "<p>驗證碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"36946497446744064\",\n    \"account\" : \"account@abc.com\",\n    \"password\" : \"987654321\",\n    \"password_repeat\" : \"987654321\",\n    \"check_code\" : \"123456\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"reset success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"account empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"password / password_repeat empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.05",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"check_code empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"account error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"password\\' length < 8\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"password / password_repeat is NOT equal\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"user error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"user status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.05",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"user account error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"user password check error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.06",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"user check status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.07",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"user check code error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"update check status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"update password error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/User.18.ResetPassword.php",
    "groupTitle": "User"
  },
  {
    "type": "PUT",
    "url": "/user/{id}",
    "title": "02. 修改使用者",
    "version": "0.1.0",
    "description": "<p>・ 修改使用者</p>",
    "name": "PutUserUpdate",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>使用者 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "init",
              "enable",
              "delete",
              "disable"
            ],
            "optional": false,
            "field": "status",
            "description": "<p>狀態</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"36946497446744064\",\n    \"status\" : \"enable\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.user_id",
            "description": "<p>使用者 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"update success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"user_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"user error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"updateData empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"update error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/User.02.Update.php",
    "groupTitle": "User"
  },
  {
    "type": "PUT",
    "url": "/user/{id}/password",
    "title": "08. 修改使用者密碼",
    "version": "0.1.0",
    "description": "<p>・ 修改使用者密碼</p>",
    "name": "PutUserUpdatePassword",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>使用者 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>密碼</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password_repeat",
            "description": "<p>確認密碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"36946497446744064\",\n    \"password\" : \"12345678\",\n    \"password_repeat\" : \"12345678\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.user_id",
            "description": "<p>使用者 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"update success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"user_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"user error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"password' length < 8\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"password / password_repeat is NOT equal\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"updateData empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"update error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/User.08.UpdatePassword.php",
    "groupTitle": "User"
  },
  {
    "type": "PUT",
    "url": "/user/{id}/profile",
    "title": "09. 修改使用者資料",
    "version": "0.1.0",
    "description": "<p>・ 修改使用者資料</p>",
    "name": "PutUserUpdateProfile",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>使用者 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "first_name",
            "description": "<p>名</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "last_name",
            "description": "<p>姓</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "nickname",
            "description": "<p>暱稱</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "email",
            "description": "<p>E-Mail</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "mobile_country_code",
            "description": "<p>手機號碼國碼</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "mobile_phone",
            "description": "<p>手機號碼</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "birth",
            "description": "<p>生日</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "bio",
            "description": "<p>自我介紹</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"36946497446744064\",\n    \"first_name\" : \"John\",\n    \"last_name\"   : \"Wu\",\n    \"email\"   : \"account@abc.com\",\n    \"nickname\"   : \"Big John\",\n    \"mobile_country_code\" : \"886\",\n    \"mobile_phone\" : \"912345678\",\n    \"birth\"   : \"1976-11-24\",\n    \"bio\"   : \"...\",\n\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.user_id",
            "description": "<p>使用者 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"update profile success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"user_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"user error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"user profile error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"email error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 409.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 409,\n    \"comment\": \"mobile_phone error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 409.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"mobile_phone format error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 409.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"birth error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"updateData empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"update error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/User.09.UpdateProfile.php",
    "groupTitle": "User"
  },
  {
    "type": "PUT",
    "url": "/user/{id}/role",
    "title": "12. 修改使用者角色",
    "version": "0.1.0",
    "description": "<p>・ 修改使用者角色</p>",
    "name": "PutUserUpdateRole",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>使用者 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "role_ids",
            "description": "<p>角色 IDs</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"36946497446744064\",\n    \"role_ids\" : [\n        \"5324389482631168\",\n        \"5324389604265984\",\n        \"5324389721706496\",\n        \"5324389788815360\",\n        \"5324389847535616\"\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.user_id",
            "description": "<p>使用者 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"update user role success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"user_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"role_ids empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"user error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.05",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"updateData empty\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/User.12.UpdateRole.php",
    "groupTitle": "User"
  },
  {
    "type": "PUT",
    "url": "/user/{id}/seo",
    "title": "11. 修改使用者 SEO",
    "version": "0.1.0",
    "description": "<p>・ 修改使用者 SEO</p>",
    "name": "PutUserUpdateSeo",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n    \"token\": \"\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>使用者 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "slug",
            "description": "<p>位址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "excerpt",
            "description": "<p>摘要</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "og_title",
            "description": "<p>og 標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "og_description",
            "description": "<p>og 描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "meta_title",
            "description": "<p>meta 標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "meta_description",
            "description": "<p>meta 描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "avatar_title",
            "description": "<p>頭像標題</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "avatar_alt",
            "description": "<p>頭像 alt</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"36946497446744064\",\n    \"slug\" : \"test1\",\n    \"excerpt\" : \"test1test1test1test1test1\",\n    \"og_title\" : \"標題一\",\n    \"og_description\" : \"test1test1test1test1test1\",\n    \"meta_title\" : \"標題一\",\n    \"meta_description\" : \"test1test1test1test1test1\",\n    \"avatar_title\" : \"標題一\",\n    \"avatar_alt\" : \"標題一\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.user_id",
            "description": "<p>使用者 ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"update profile success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\",\n        \"user_id\": \"36946497446744064\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"token empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"session token error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"user error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"user seo error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 409.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 409,\n    \"comment\": \"slug error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.04",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"updateData empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 500.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 500,\n    \"comment\": \"update error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/User.11.UpdateSeo.php",
    "groupTitle": "User"
  },
  {
    "type": "PUT",
    "url": "/user/{id}/email/verify",
    "title": "14. 驗證 E-Mail",
    "version": "0.1.0",
    "description": "<p>・ 驗證 E-Mail</p>",
    "name": "PutUserVerifyEmail",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>使用者 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "check_code",
            "description": "<p>驗證碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"36946497446744064\",\n    \"check_code\" : \"123456\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"verify success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"check_code empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"user error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"user status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"user email check error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"user check status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"user check code error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/User.14.VerifyEmail.php",
    "groupTitle": "User"
  },
  {
    "type": "PUT",
    "url": "/user/{id}/mobile/verify",
    "title": "16. 驗證 Mobile",
    "version": "0.1.0",
    "description": "<p>・ 驗證 Mobile</p>",
    "name": "PutUserVerifyMobile",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header",
          "content": "{\n    \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>使用者 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "check_code",
            "description": "<p>驗證碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request",
          "content": "{\n    \"id\" : \"36946497446744064\",\n    \"check_code\" : \"123456\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Success",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          },
          {
            "group": "Success",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>回傳資訊</p>"
          },
          {
            "group": "Success",
            "type": "string",
            "optional": false,
            "field": "data.session",
            "description": "<p>Session 代碼</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 201",
          "content": "{\n    \"status\": \"success\",\n    \"code\": 201,\n    \"comment\": \"verify success\",\n    \"data\": {\n        \"session\": \"8f3e973061800dc6ebcb367079b305d8\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>回傳狀態</p>"
          },
          {
            "group": "Error",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>回傳代碼</p>"
          },
          {
            "group": "Error",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>回傳訊息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response: 400.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"session empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"id empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 400.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 400,\n    \"comment\": \"check_code empty\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 410.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 410,\n    \"comment\": \"session is NOT alive\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"user error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.01",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"user status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 404.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 404,\n    \"comment\": \"user mobile check error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.02",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"user check status error\"\n}",
          "type": "json"
        },
        {
          "title": "Response: 422.03",
          "content": "{\n    \"status\": \"fail\",\n    \"code\": 422,\n    \"comment\": \"user check code error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "ea-stage1-api/app/Docs/User.16.VerifyMobile.php",
    "groupTitle": "User"
  }
] });
