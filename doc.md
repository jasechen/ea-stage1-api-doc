<a name="top"></a>
#  v0.0.0



- [File](#file)
	- [03. 刪除檔案 by filename](#03.-刪除檔案-by-filename)
	
- [Filter](#filter)
	- [04. 查詢 單篇過濾條件](#04.-查詢-單篇過濾條件)
	- [06. 查詢 單篇過濾條件 By Code](#06.-查詢-單篇過濾條件-by-code)
	- [07. 查詢 所有過濾條件 By parent_type](#07.-查詢-所有過濾條件-by-parent_type)
	- [09. 查詢 所有過濾條件 By parent_type and code](#09.-查詢-所有過濾條件-by-parent_type-and-code)
	- [08. 查詢 所有過濾條件 By parent_type and parent_id](#08.-查詢-所有過濾條件-by-parent_type-and-parent_id)
	- [05. 查詢 所有過濾條件](#05.-查詢-所有過濾條件)
	
- [Login](#login)
	- [01. 登入](#01.-登入)
	
- [Logout](#logout)
	- [01. 登出](#01.-登出)
	
- [Post](#post)
	- [03. 刪除文章](#03.-刪除文章)
	- [04. 查詢 單篇文章](#04.-查詢-單篇文章)
	- [08. 查詢 文章 By PostcategoryId](#08.-查詢-文章-by-postcategoryid)
	- [09. 查詢 文章 By PostcategorySlug](#09.-查詢-文章-by-postcategoryslug)
	- [06. 查詢 單篇文章 By Slug](#06.-查詢-單篇文章-by-slug)
	- [05. 查詢 所有文章](#05.-查詢-所有文章)
	- [07. 查詢 被推薦文章 By PostcategoryId](#07.-查詢-被推薦文章-by-postcategoryid)
	- [01. 新增文章](#01.-新增文章)
	- [10. 上傳封面檔案](#10.-上傳封面檔案)
	- [02. 修改文章](#02.-修改文章)
	- [11. 修改文章 SEO](#11.-修改文章-seo)
	
- [Postcategory](#postcategory)
	- [03. 刪除文章分類](#03.-刪除文章分類)
	- [04. 查詢 單篇文章分類](#04.-查詢-單篇文章分類)
	- [08. 查詢 文章分類 By OwnerId](#08.-查詢-文章分類-by-ownerid)
	- [06. 查詢 單篇文章分類 By Slug](#06.-查詢-單篇文章分類-by-slug)
	- [07. 查詢 文章分類 By Type](#07.-查詢-文章分類-by-type)
	- [09. 查詢 文章分類 By Type and OwnerId](#09.-查詢-文章分類-by-type-and-ownerid)
	- [05. 查詢 文章分類](#05.-查詢-文章分類)
	- [01. 新增文章分類](#01.-新增文章分類)
	- [10. 上傳封面檔案](#10.-上傳封面檔案)
	- [02. 修改文章分類](#02.-修改文章分類)
	- [11. 修改文章分類 SEO](#11.-修改文章分類-seo)
	
- [Register](#register)
	- [01. 註冊](#01.-註冊)
	
- [Role](#role)
	- [04. 查詢 單一角色](#04.-查詢-單一角色)
	- [07. 查詢 所有角色 By Code](#07.-查詢-所有角色-by-code)
	- [06. 查詢 所有角色 By Type](#06.-查詢-所有角色-by-type)
	- [08. 查詢 所有角色 By Type And Code](#08.-查詢-所有角色-by-type-and-code)
	- [05. 查詢 所有角色](#05.-查詢-所有角色)
	
- [School](#school)
	- [03. 刪除學校](#03.-刪除學校)
	- [15. 刪除學校課程](#15.-刪除學校課程)
	- [20. 刪除學校宿舍](#20.-刪除學校宿舍)
	- [31. 刪除學校 Filter](#31.-刪除學校-filter)
	- [25. 刪除學校使用者](#25.-刪除學校使用者)
	- [04. 查詢 單篇學校](#04.-查詢-單篇學校)
	- [06. 查詢 單篇學校 By Slug](#06.-查詢-單篇學校-by-slug)
	- [16. 查詢 單篇學校課程](#16.-查詢-單篇學校課程)
	- [17. 查詢 學校課程 By School Id](#17.-查詢-學校課程-by-school-id)
	- [21. 查詢 單篇學校宿舍](#21.-查詢-單篇學校宿舍)
	- [22. 查詢 學校宿舍 By School Id](#22.-查詢-學校宿舍-by-school-id)
	- [12. 查詢 單篇學校費用](#12.-查詢-單篇學校費用)
	- [32. 查詢 學校 Filter](#32.-查詢-學校-filter)
	- [05. 查詢 所有學校](#05.-查詢-所有學校)
	- [26. 查詢 單篇學校使用者](#26.-查詢-單篇學校使用者)
	- [27. 查詢 學校使用者 By School Id](#27.-查詢-學校使用者-by-school-id)
	- [28. 查詢 學校使用者 By School Id And Type](#28.-查詢-學校使用者-by-school-id-and-type)
	- [01. 新增學校](#01.-新增學校)
	- [13. 新增學校課程](#13.-新增學校課程)
	- [18. 新增學校宿舍](#18.-新增學校宿舍)
	- [10. 新增學校費用](#10.-新增學校費用)
	- [29. 新增學校 Filter](#29.-新增學校-filter)
	- [23. 新增學校使用者](#23.-新增學校使用者)
	- [33. 搜尋學校 Filter](#33.-搜尋學校-filter)
	- [07. 上傳封面檔案](#07.-上傳封面檔案)
	- [09. 上傳圖片檔案](#09.-上傳圖片檔案)
	- [02. 修改學校](#02.-修改學校)
	- [14. 修改學校課程](#14.-修改學校課程)
	- [19. 修改學校宿舍](#19.-修改學校宿舍)
	- [11. 修改學校費用](#11.-修改學校費用)
	- [30. 修改學校 Filter](#30.-修改學校-filter)
	- [08. 修改學校 SEO](#08.-修改學校-seo)
	- [24. 修改學校使用者](#24.-修改學校使用者)
	
- [Search](#search)
	- [01. 搜尋](#01.-搜尋)
	
- [Session](#session)
	- [02. 用 code 查詢 session](#02.-用-code-查詢-session)
	- [01. 初始化](#01.-初始化)
	
- [Subscribe](#subscribe)
	- [02. 查詢 所有訂閱者](#02.-查詢-所有訂閱者)
	- [01. 訂閱 EDM](#01.-訂閱-edm)
	
- [User](#user)
	- [03. 刪除使用者](#03.-刪除使用者)
	- [04. 查詢 單一使用者](#04.-查詢-單一使用者)
	- [06. 查詢 單一使用者 By Slug](#06.-查詢-單一使用者-by-slug)
	- [07. 查詢 單一使用者 By Token](#07.-查詢-單一使用者-by-token)
	- [05. 查詢 所有使用者](#05.-查詢-所有使用者)
	- [13. 重寄 E-Mail 驗證信](#13.-重寄-e-mail-驗證信)
	- [15. 重寄 Mobile 驗證簡訊](#15.-重寄-mobile-驗證簡訊)
	- [01. 新增使用者](#01.-新增使用者)
	- [17. 寄 忘記密碼通知信](#17.-寄-忘記密碼通知信)
	- [10. 上傳使用者頭像](#10.-上傳使用者頭像)
	- [18. 重設密碼](#18.-重設密碼)
	- [02. 修改使用者](#02.-修改使用者)
	- [08. 修改使用者密碼](#08.-修改使用者密碼)
	- [09. 修改使用者資料](#09.-修改使用者資料)
	- [12. 修改使用者角色](#12.-修改使用者角色)
	- [11. 修改使用者 SEO](#11.-修改使用者-seo)
	- [14. 驗證 E-Mail](#14.-驗證-e-mail)
	- [16. 驗證 Mobile](#16.-驗證-mobile)
	


# <a name='file'></a> File

## <a name='03.-刪除檔案-by-filename'></a> 03. 刪除檔案 by filename
[Back to top](#top)

<p>・ 刪除檔案 by filename</p>

	DELETE /file/{filename}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  filename | string | <p>檔名                                        [slug]             位址</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "delete success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "filename": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.filename | string | <p>檔名</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "filename empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "file error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "delete error"
}
```
# <a name='filter'></a> Filter

## <a name='04.-查詢-單篇過濾條件'></a> 04. 查詢 單篇過濾條件
[Back to top](#top)

<p>・ 查詢 單篇過濾條件</p>

	GET /filter/{id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>過濾條件 ID</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "filter": {
            "id": "13292119993225216",
            "type": "school",
            "parent_type": "country",
            "parent_id": "0",
            "code": "ph",
            "creator_id": "0",
            "updated_at": "2018-04-06 16:18:08",
            "created_at": "2018-04-06 16:18:08",
            "child": [
                {
                    "id": "13292120068722688",
                    "type": "school",
                    "parent_type": "location",
                    "parent_id": "13292119993225216",
                    "code": "ceb",
                    "creator_id": "0",
                    "updated_at": "2018-04-06 16:18:08",
                    "created_at": "2018-04-06 16:18:08"
                },
                {
                    "id": "13292120081305600",
                    "type": "school",
                    "parent_type": "location",
                    "parent_id": "13292119993225216",
                    "code": "bag",
                    "creator_id": "0",
                    "updated_at": "2018-04-06 16:18:08",
                    "created_at": "2018-04-06 16:18:08"
                },
                {
                    "id": "13292120089694208",
                    "type": "school",
                    "parent_type": "location",
                    "parent_id": "13292119993225216",
                    "code": "crk",
                    "creator_id": "0",
                    "updated_at": "2018-04-06 16:18:08",
                    "created_at": "2018-04-06 16:18:08"
                }
            ]
        }
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.filter | string | <p>過濾條件</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
## <a name='06.-查詢-單篇過濾條件-by-code'></a> 06. 查詢 單篇過濾條件 By Code
[Back to top](#top)

<p>・ 查詢 單篇過濾條件 By Code</p>

	GET /filter/code/{code}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  code | string | <p>過濾條件 code</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "filter": {
            "id": "13292119993225216",
            "type": "school",
            "parent_type": "country",
            "parent_id": "0",
            "code": "ph",
            "creator_id": "0",
            "updated_at": "2018-04-06 16:18:08",
            "created_at": "2018-04-06 16:18:08",
            "child": [
                {
                    "id": "13292120068722688",
                    "type": "school",
                    "parent_type": "location",
                    "parent_id": "13292119993225216",
                    "code": "ceb",
                    "creator_id": "0",
                    "updated_at": "2018-04-06 16:18:08",
                    "created_at": "2018-04-06 16:18:08"
                },
                {
                    "id": "13292120081305600",
                    "type": "school",
                    "parent_type": "location",
                    "parent_id": "13292119993225216",
                    "code": "bag",
                    "creator_id": "0",
                    "updated_at": "2018-04-06 16:18:08",
                    "created_at": "2018-04-06 16:18:08"
                },
                {
                    "id": "13292120089694208",
                    "type": "school",
                    "parent_type": "location",
                    "parent_id": "13292119993225216",
                    "code": "crk",
                    "creator_id": "0",
                    "updated_at": "2018-04-06 16:18:08",
                    "created_at": "2018-04-06 16:18:08"
                }
            ]
        }
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.filter | string | <p>過濾條件</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "code empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
## <a name='07.-查詢-所有過濾條件-by-parent_type'></a> 07. 查詢 所有過濾條件 By parent_type
[Back to top](#top)

<p>・ 查詢 所有過濾條件 By parent_type</p>

	GET /filter/school/type/{parent_type}/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  parent_type | string | <p>parent type</p>_Allowed values: country,location,school-type,program-type,program,program-ta_|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "filters": [
            {
                "id": "13292120144220160",
                "type": "school",
                "parent_type": "school-type",
                "parent_id": "0",
                "code": "middle",
                "creator_id": "0",
                "updated_at": "2018-04-06 16:18:08",
                "created_at": "2018-04-06 16:18:08"
            },
            {
                "id": "13292120152608768",
                "type": "school",
                "parent_type": "school-type",
                "parent_id": "0",
                "code": "senior",
                "creator_id": "0",
                "updated_at": "2018-04-06 16:18:08",
                "created_at": "2018-04-06 16:18:08"
            },
            {
                "id": "13292120160997376",
                "type": "school",
                "parent_type": "school-type",
                "parent_id": "0",
                "code": "collage",
                "creator_id": "0",
                "updated_at": "2018-04-06 16:18:08",
                "created_at": "2018-04-06 16:18:08"
            },
            {
                "id": "13292120169385984",
                "type": "school",
                "parent_type": "school-type",
                "parent_id": "0",
                "code": "community",
                "creator_id": "0",
                "updated_at": "2018-04-06 16:18:08",
                "created_at": "2018-04-06 16:18:08"
            },
            {
                "id": "13292120177774592",
                "type": "school",
                "parent_type": "school-type",
                "parent_id": "0",
                "code": "graduate",
                "creator_id": "0",
                "updated_at": "2018-04-06 16:18:08",
                "created_at": "2018-04-06 16:18:08"
            },
            {
                "id": "13292120181968896",
                "type": "school",
                "parent_type": "school-type",
                "parent_id": "0",
                "code": "language",
                "creator_id": "0",
                "updated_at": "2018-04-06 16:18:08",
                "created_at": "2018-04-06 16:18:08"
            }
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.filters | string[] | <p>過濾條件s</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "parent_type empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "parent_type error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
## <a name='09.-查詢-所有過濾條件-by-parent_type-and-code'></a> 09. 查詢 所有過濾條件 By parent_type and code
[Back to top](#top)

<p>・ 查詢 所有過濾條件 By parent_type and code</p>

	GET /filter/school/type/{parent_type}/code/{code}/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  parent_type | string | <p>parent type</p>_Allowed values: country,location,school-type,program-type,program,program-ta_|
|  code | string | <p>code</p>|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "filters": [
            {
                "id": "13292120068722688",
                "type": "school",
                "parent_type": "location",
                "parent_id": "13292119993225216",
                "code": "ceb",
                "creator_id": "0",
                "updated_at": "2018-04-06 16:18:08",
                "created_at": "2018-04-06 16:18:08"
            },
            {
                "id": "13292120081305600",
                "type": "school",
                "parent_type": "location",
                "parent_id": "13292119993225216",
                "code": "bag",
                "creator_id": "0",
                "updated_at": "2018-04-06 16:18:08",
                "created_at": "2018-04-06 16:18:08"
            },
            {
                "id": "13292120089694208",
                "type": "school",
                "parent_type": "location",
                "parent_id": "13292119993225216",
                "code": "crk",
                "creator_id": "0",
                "updated_at": "2018-04-06 16:18:08",
                "created_at": "2018-04-06 16:18:08"
            }
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.filters | string[] | <p>過濾條件s</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "parent_type empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "code empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "parent_type error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "code error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
## <a name='08.-查詢-所有過濾條件-by-parent_type-and-parent_id'></a> 08. 查詢 所有過濾條件 By parent_type and parent_id
[Back to top](#top)

<p>・ 查詢 所有過濾條件 By parent_type and parent_id</p>

	GET /filter/school/type/{parent_type}/id/{parent_id}/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  parent_type | string | <p>parent type</p>_Allowed values: country,location,school-type,program-type,program,program-ta_|
|  parent_id | string | <p>parent id</p>|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "filters": [
            {
                "id": "13292120068722688",
                "type": "school",
                "parent_type": "location",
                "parent_id": "13292119993225216",
                "code": "ceb",
                "creator_id": "0",
                "updated_at": "2018-04-06 16:18:08",
                "created_at": "2018-04-06 16:18:08"
            },
            {
                "id": "13292120081305600",
                "type": "school",
                "parent_type": "location",
                "parent_id": "13292119993225216",
                "code": "bag",
                "creator_id": "0",
                "updated_at": "2018-04-06 16:18:08",
                "created_at": "2018-04-06 16:18:08"
            },
            {
                "id": "13292120089694208",
                "type": "school",
                "parent_type": "location",
                "parent_id": "13292119993225216",
                "code": "crk",
                "creator_id": "0",
                "updated_at": "2018-04-06 16:18:08",
                "created_at": "2018-04-06 16:18:08"
            }
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.filters | string[] | <p>過濾條件s</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "parent_type empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "parent_id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "parent_type error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
## <a name='05.-查詢-所有過濾條件'></a> 05. 查詢 所有過濾條件
[Back to top](#top)

<p>・ 查詢 所有過濾條件</p>

	GET /filter/list/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "filters": [
            {
                "id": "13292119993225216",
                "type": "school",
                "parent_type": "country",
                "parent_id": "0",
                "code": "ph",
                "creator_id": "0",
                "updated_at": "2018-04-06 16:18:08",
                "created_at": "2018-04-06 16:18:08",
                "child": [
                    {
                        "id": "13292120068722688",
                        "type": "school",
                        "parent_type": "location",
                        "parent_id": "13292119993225216",
                        "code": "ceb",
                        "creator_id": "0",
                        "updated_at": "2018-04-06 16:18:08",
                        "created_at": "2018-04-06 16:18:08"
                    },
                    {
                        "id": "13292120081305600",
                        "type": "school",
                        "parent_type": "location",
                        "parent_id": "13292119993225216",
                        "code": "bag",
                        "creator_id": "0",
                        "updated_at": "2018-04-06 16:18:08",
                        "created_at": "2018-04-06 16:18:08"
                    },
                    {
                        "id": "13292120089694208",
                        "type": "school",
                        "parent_type": "location",
                        "parent_id": "13292119993225216",
                        "code": "crk",
                        "creator_id": "0",
                        "updated_at": "2018-04-06 16:18:08",
                        "created_at": "2018-04-06 16:18:08"
                    }
                ]
            },
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.filters | string[] | <p>過濾條件s</p>|

### Error Response

Response: 400

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
# <a name='login'></a> Login

## <a name='01.-登入'></a> 01. 登入
[Back to top](#top)

<p>・ 使用者登入</p>

	POST /login

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  account | string | <p>使用者帳號</p>|
|  password | string | <p>密碼</p>|

### Success Response

Response

```
{
    "status": "success",
    "code": 200,
    "comment": "login success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "token": "33a909a7-22d2-5c0e-90c6-833a5e5b30b0",
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.token | string | <p>使用者 token</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "account empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "password empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "account error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "password' length < 8"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "account error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "password error"
}
```
Response: 422.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "user status error"
}
```
# <a name='logout'></a> Logout

## <a name='01.-登出'></a> 01. 登出
[Back to top](#top)

<p>・ 使用者登出</p>

	POST /logout

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | **optional**<p>使用者 token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
```


### Success Response

Response

```
{
    "status": "success",
    "code": 200,
    "comment": "logout success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
# <a name='post'></a> Post

## <a name='03.-刪除文章'></a> 03. 刪除文章
[Back to top](#top)

<p>・ 刪除文章</p>

	DELETE /post/{id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>文章 ID                                        [slug]             位址</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "delete success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "post_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.post_id | string | <p>文章 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "post error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "delete error"
}
```
## <a name='04.-查詢-單篇文章'></a> 04. 查詢 單篇文章
[Back to top](#top)

<p>・ 查詢 單篇文章</p>

	GET /post/{id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>文章 ID</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "post": {
            "id": "8220321387778048",
            "title": "【英國留遊學】倫敦大學金匠學院",
            "content": "<p>倫敦大學在台灣應該是個耳熟能詳的英國大學，畢竟民進黨的黨主席蔡英文女士就是畢業於倫敦大學的其中一個學院 &ndash; 倫敦政治經濟學院 （London School of Economics and Political Science），但是除了這個幾乎家喻戶曉的學院之外，其實聯邦制體系的倫敦大學還有其他專攻不同領域的院校，而我今天要介紹的就是專攻藝文與傳媒的金匠學院(Goldsmiths)。</p>\n<p>如果你喜歡英國當代藝術，或是你有些許研究的話，你應該會有聽過The Young British Artists（簡稱YBAs）這個名稱，他們是一群崛起於80年代的藝術家們，並且在90年代在藝術界造成了一陣轟動，也深深影響了現在的當代藝術。在這群藝術家中有多位都是畢業於金匠學院的藝術科系，例如，在2007年推出全世界最貴的藝術作品 For the love of God的Damien Hirst跟以諷刺手法來表現兩性觀點的Sarah Lucas，他們都是金匠學院的校友。如果你喜歡時尚的話，你一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，你不知道的是，Vivian Westwood也是畢業於金匠學院的設計科系。另外，金匠學院的大眾傳播學系(Media and Communication)也是國際知名的學校，像韓國的導演也都曾來此就讀。可見金匠學院在藝文界真的是有舉足輕重的地位，也培育出了許多赫赫有名的人才。</p>\n<p>在當年就是衝著這股濃濃藝文味，於是我選擇到了金匠學院，主修大眾傳播媒體，在就讀的期間也感受到了學校的優點與缺點，在這裡就跟可能有計畫來就讀的你分享一下身為校友的個人的經驗吧！</p>\n<p>首先，有一件事你一定要先知道，在英國，大學的<strong>就讀時間大部分為三年</strong>，也就是讀完三年沒被當的話就能夠順利畢業，另外學校採用的是<strong>三學期制</strong>，跟在台灣熟悉的二學期制不太一樣，雖然這樣假期似乎看起來會比較多，但是其實課業量反而更繁重，因為相同的課程就變成要在較短的時間內學會，而且每一次的假期也就代表著一次的報告dead line，所以，想玩就必須好好做時間規劃，把時間規劃好，才能夠順利繳交報告跟同時享有假期啊！</p>\n<p>另外，我覺得倫敦大學的學校，相對於倫敦藝術大學，會顯得比較著重於理論，因為我有認識的朋友是就讀於倫敦藝術大學，他們時常都是忙於繳交作品，個人覺得是比較偏向實作方面，但是在金匠學院他們是採取<strong>理論與實作並重</strong>的方式來教課，也就是說作品量的累積跟實作的經驗比起倫敦藝術大學的同學可能會有些落差，但是在理論上知識的基礎可能會更加深厚。會這樣著重理論，是因為他們認為在知識上有著較深厚的基礎就能夠培養出批判性的思考能力(critical thinking)，創造出來的作品會更有內涵並能利用學到的知識來引導觀者，藉以得到更大的共鳴。</p>\n<p>在大眾傳播的實作課程中，你可以從八種不同的課程來做選擇，分別為：</p>\n<p>Journalism</p>\n<p>Photography</p>\n<p>Filmmaking</p>\n<p>Illustration</p>\n<p>Script and Props</p>\n<p>Animation</p>\n<p>Radio</p>\n<p>Interactive Media</p>\n<p>在一年級的時候你可以選修四種實作課程，但是到了二年級只能選修兩種，到了三年級就必須專精於其中一項，應該是希望透過嘗試讓學生找到自己有興趣或是有潛能的課程，再加以訓練跟培養。</p>\n<p>我一年級時嘗試了animation，photography，filmmaking跟illustration，二年級的時候選擇了動畫跟攝影，到三年級就專精於攝影。其中動畫課在一二年級的時候，我們學習到的是stop-motion的製作方式，還有如何製作story board，但是到了三年級會學習到使用After Effect這個軟體來製作動畫。filmmaking其實很有趣，你會跟著一個團隊一起合作，可以嘗試到像採訪陌生人，或是自己尋找演員、找場地拍攝，甚至是製作配樂等經驗。在攝影課我們使用的幾乎都是底片相機，學校都有設備可以出借，但是有規定的出借跟歸還時間。在課堂上你會學到關於攝影的理論跟實際操作器具的體驗，不只是相機的操作，還有燈光的調整，底片的沖洗等等。基本上每學年你都需要製作一個自己的project，主題很自由，老師可能會給你一個大方向，但是你可以自己決定主題去製作。</p>\n<p>不過，專精於其中一項不代表你只能侷限自己做某一個課程，相反的，學校反而鼓勵學生互相合作、學習，甚至跟不同科系的學生也都能夠互相合作來籌畫活動，推行企劃。但是，這相互間的合作通常是要靠個人去籌劃跟聯繫，學校並不會當中間人或指導人，<strong>主動性的培養</strong>，我覺得這是臺灣跟國外學校的一個最主要的差別吧！老師們，可能會提出你可以籌辦活動的想法，但是做與不做就全權交給你去決定，如果要做了，你有問題都可以主動提出，他們也都會很樂意幫助你，但比較不會主動去督促你的進度。像上必修課也都是一樣的，基本上都是lecture接著上seminar，書籍都是一開學就發放給你，在上課前你必須自己先研讀，到學校講師在講課，講課的內容會以書籍裡的內容為基礎，然後延伸出其他更深更廣的理論，有問題再在seminar上提出討論，老師並不會帶著你唸課文，所以如果你事前預習沒做好，在講師講課的時候可能就已經被搞得一頭霧水了。</p>\n<p>如果你在大學裡期待的是非常多的實作經驗，還有學校幫你選擇好的實習課程，那金匠大學可能不是屬於你的選擇，但是如果你想把底子學好跟喜歡挑戰非商業性的創作，靠自己去尋找實習機會，金匠大學就會是屬於你的不二選擇喔～</p>",
            "cover": "1519267601292148",
            "privacy_status": "public",
            "status": "publish",
            "recommended": "0",
            "blog_id": "8217810404773888",
            "postcategory_id": "7859246410633216",
            "owner_id": "8217810346053632",
            "creator_id": "0",
            "updated_at": "2018-03-23 16:24:37",
            "created_at": "2018-03-23 16:24:37",
            "slug": "英國留遊學-倫敦大學金匠學院",
            "excerpt": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
            "og_title": "【英國留遊學】倫敦大學金匠學院",
            "og_description": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
            "meta_title": "【英國留遊學】倫敦大學金匠學院",
            "meta_description": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
            "cover_title": "倫敦大學金匠學院",
            "cover_alt": "倫敦大學金匠學院",
            "postcategory": {
                "id": "7859246410633216",
                "type": "global",
                "name": "英國留學專欄",
                "cover": "1519872393361401",
                "status": "enable",
                "num_items": "0",
                "owner_id": "0",
                "creator_id": "0",
                "updated_at": "2018-03-22 16:29:50",
                "created_at": "2018-03-22 16:29:50",
                "slug": "study-aboard-UK",
                "excerpt": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                "og_title": "《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！",
                "og_description": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                "meta_title": "《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！",
                "meta_description": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                "cover_title": "出國留學",
                "cover_alt": "出國留學"
            },
            "owner": {
                "id": "8217810346053632",
                "account": "david@english.agency",
                "password": "$2y$10$5zcXhEAiHzYwfy5Vs7m5tObjcu3cgbvciFA5Myz4cmWgpP.ISSPWW",
                "status": "enable",
                "token": "070c01d7-a9c5-5614-b9ac-8f14b53ee5a7",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:38",
                "created_at": "2018-03-23 16:14:38",
                "slug": "david_b",
                "excerpt": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                "og_title": "\"David B\"",
                "og_description": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                "meta_title": "\"David B\"",
                "meta_description": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                "avatar_title": "\"David B\"",
                "avatar_alt": "\"David B\""
            },
            "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
            "url_path": "/英國留遊學-倫敦大學金匠學院-p8220321387778048"
        }
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.post | string | <p>文章</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
## <a name='08.-查詢-文章-by-postcategoryid'></a> 08. 查詢 文章 By PostcategoryId
[Back to top](#top)

<p>・ 查詢 文章 By PostcategoryId</p>

	GET /post/postcategory/{postcategory_id}/{status?}/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  postcategory_id | string | <p>文章分類 ID</p>|
|  status | string | **optional**<p>文章狀態</p>_Default value: publish_<br>_Allowed values: all,draft,publish,delete,disable_|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "posts": [
            {
                "id": "8220321387778048",
                "title": "【英國留遊學】倫敦大學金匠學院",
                "content": "<p>倫敦大學在台灣應該是個耳熟能詳的英國大學，畢竟民進黨的黨主席蔡英文女士就是畢業於倫敦大學的其中一個學院 &ndash; 倫敦政治經濟學院 （London School of Economics and Political Science），但是除了這個幾乎家喻戶曉的學院之外，其實聯邦制體系的倫敦大學還有其他專攻不同領域的院校，而我今天要介紹的就是專攻藝文與傳媒的金匠學院(Goldsmiths)。</p>\n<p>如果你喜歡英國當代藝術，或是你有些許研究的話，你應該會有聽過The Young British Artists（簡稱YBAs）這個名稱，他們是一群崛起於80年代的藝術家們，並且在90年代在藝術界造成了一陣轟動，也深深影響了現在的當代藝術。在這群藝術家中有多位都是畢業於金匠學院的藝術科系，例如，在2007年推出全世界最貴的藝術作品 For the love of God的Damien Hirst跟以諷刺手法來表現兩性觀點的Sarah Lucas，他們都是金匠學院的校友。如果你喜歡時尚的話，你一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，你不知道的是，Vivian Westwood也是畢業於金匠學院的設計科系。另外，金匠學院的大眾傳播學系(Media and Communication)也是國際知名的學校，像韓國的導演也都曾來此就讀。可見金匠學院在藝文界真的是有舉足輕重的地位，也培育出了許多赫赫有名的人才。</p>\n<p>在當年就是衝著這股濃濃藝文味，於是我選擇到了金匠學院，主修大眾傳播媒體，在就讀的期間也感受到了學校的優點與缺點，在這裡就跟可能有計畫來就讀的你分享一下身為校友的個人的經驗吧！</p>\n<p>首先，有一件事你一定要先知道，在英國，大學的<strong>就讀時間大部分為三年</strong>，也就是讀完三年沒被當的話就能夠順利畢業，另外學校採用的是<strong>三學期制</strong>，跟在台灣熟悉的二學期制不太一樣，雖然這樣假期似乎看起來會比較多，但是其實課業量反而更繁重，因為相同的課程就變成要在較短的時間內學會，而且每一次的假期也就代表著一次的報告dead line，所以，想玩就必須好好做時間規劃，把時間規劃好，才能夠順利繳交報告跟同時享有假期啊！</p>\n<p>另外，我覺得倫敦大學的學校，相對於倫敦藝術大學，會顯得比較著重於理論，因為我有認識的朋友是就讀於倫敦藝術大學，他們時常都是忙於繳交作品，個人覺得是比較偏向實作方面，但是在金匠學院他們是採取<strong>理論與實作並重</strong>的方式來教課，也就是說作品量的累積跟實作的經驗比起倫敦藝術大學的同學可能會有些落差，但是在理論上知識的基礎可能會更加深厚。會這樣著重理論，是因為他們認為在知識上有著較深厚的基礎就能夠培養出批判性的思考能力(critical thinking)，創造出來的作品會更有內涵並能利用學到的知識來引導觀者，藉以得到更大的共鳴。</p>\n<p>在大眾傳播的實作課程中，你可以從八種不同的課程來做選擇，分別為：</p>\n<p>Journalism</p>\n<p>Photography</p>\n<p>Filmmaking</p>\n<p>Illustration</p>\n<p>Script and Props</p>\n<p>Animation</p>\n<p>Radio</p>\n<p>Interactive Media</p>\n<p>在一年級的時候你可以選修四種實作課程，但是到了二年級只能選修兩種，到了三年級就必須專精於其中一項，應該是希望透過嘗試讓學生找到自己有興趣或是有潛能的課程，再加以訓練跟培養。</p>\n<p>我一年級時嘗試了animation，photography，filmmaking跟illustration，二年級的時候選擇了動畫跟攝影，到三年級就專精於攝影。其中動畫課在一二年級的時候，我們學習到的是stop-motion的製作方式，還有如何製作story board，但是到了三年級會學習到使用After Effect這個軟體來製作動畫。filmmaking其實很有趣，你會跟著一個團隊一起合作，可以嘗試到像採訪陌生人，或是自己尋找演員、找場地拍攝，甚至是製作配樂等經驗。在攝影課我們使用的幾乎都是底片相機，學校都有設備可以出借，但是有規定的出借跟歸還時間。在課堂上你會學到關於攝影的理論跟實際操作器具的體驗，不只是相機的操作，還有燈光的調整，底片的沖洗等等。基本上每學年你都需要製作一個自己的project，主題很自由，老師可能會給你一個大方向，但是你可以自己決定主題去製作。</p>\n<p>不過，專精於其中一項不代表你只能侷限自己做某一個課程，相反的，學校反而鼓勵學生互相合作、學習，甚至跟不同科系的學生也都能夠互相合作來籌畫活動，推行企劃。但是，這相互間的合作通常是要靠個人去籌劃跟聯繫，學校並不會當中間人或指導人，<strong>主動性的培養</strong>，我覺得這是臺灣跟國外學校的一個最主要的差別吧！老師們，可能會提出你可以籌辦活動的想法，但是做與不做就全權交給你去決定，如果要做了，你有問題都可以主動提出，他們也都會很樂意幫助你，但比較不會主動去督促你的進度。像上必修課也都是一樣的，基本上都是lecture接著上seminar，書籍都是一開學就發放給你，在上課前你必須自己先研讀，到學校講師在講課，講課的內容會以書籍裡的內容為基礎，然後延伸出其他更深更廣的理論，有問題再在seminar上提出討論，老師並不會帶著你唸課文，所以如果你事前預習沒做好，在講師講課的時候可能就已經被搞得一頭霧水了。</p>\n<p>如果你在大學裡期待的是非常多的實作經驗，還有學校幫你選擇好的實習課程，那金匠大學可能不是屬於你的選擇，但是如果你想把底子學好跟喜歡挑戰非商業性的創作，靠自己去尋找實習機會，金匠大學就會是屬於你的不二選擇喔～</p>",
                "cover": "1519267601292148",
                "privacy_status": "public",
                "status": "publish",
                "recommended": "0",
                "blog_id": "8217810404773888",
                "postcategory_id": "7859246410633216",
                "owner_id": "8217810346053632",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:24:37",
                "created_at": "2018-03-23 16:24:37",
                "slug": "英國留遊學-倫敦大學金匠學院",
                "excerpt": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
                "og_title": "【英國留遊學】倫敦大學金匠學院",
                "og_description": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
                "meta_title": "【英國留遊學】倫敦大學金匠學院",
                "meta_description": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
                "cover_title": "倫敦大學金匠學院",
                "cover_alt": "倫敦大學金匠學院",
                "postcategory": {
                    "id": "7859246410633216",
                    "type": "global",
                    "name": "英國留學專欄",
                    "cover": "1519872393361401",
                    "status": "enable",
                    "num_items": "0",
                    "owner_id": "0",
                    "creator_id": "0",
                    "updated_at": "2018-03-22 16:29:50",
                    "created_at": "2018-03-22 16:29:50",
                    "slug": "study-aboard-UK",
                    "excerpt": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                    "og_title": "《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！",
                    "og_description": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                    "meta_title": "《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！",
                    "meta_description": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                    "cover_title": "出國留學",
                    "cover_alt": "出國留學"
                },
                "owner": {
                    "id": "8217810346053632",
                    "account": "david@english.agency",
                    "password": "$2y$10$5zcXhEAiHzYwfy5Vs7m5tObjcu3cgbvciFA5Myz4cmWgpP.ISSPWW",
                    "status": "enable",
                    "token": "070c01d7-a9c5-5614-b9ac-8f14b53ee5a7",
                    "creator_id": "0",
                    "updated_at": "2018-03-23 16:14:38",
                    "created_at": "2018-03-23 16:14:38",
                    "slug": "david_b",
                    "excerpt": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                    "og_title": "\"David B\"",
                    "og_description": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                    "meta_title": "\"David B\"",
                    "meta_description": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                    "avatar_title": "\"David B\"",
                    "avatar_alt": "\"David B\""
                },
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": "/英國留遊學-倫敦大學金匠學院-p8220321387778048"
            },
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.posts | string[] | <p>文章</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "postcategory_id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
## <a name='09.-查詢-文章-by-postcategoryslug'></a> 09. 查詢 文章 By PostcategorySlug
[Back to top](#top)

<p>・ 查詢 文章 By PostcategorySlug</p>

	GET /post/postcategory/slug/{postcategory_slug}/{status?}/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  postcategory_slug | string | <p>文章分類 Slug</p>|
|  status | string | **optional**<p>文章狀態</p>_Default value: publish_<br>_Allowed values: all,draft,publish,delete,disable_|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "posts": [
            {
                "id": "8220321387778048",
                "title": "【英國留遊學】倫敦大學金匠學院",
                "content": "<p>倫敦大學在台灣應該是個耳熟能詳的英國大學，畢竟民進黨的黨主席蔡英文女士就是畢業於倫敦大學的其中一個學院 &ndash; 倫敦政治經濟學院 （London School of Economics and Political Science），但是除了這個幾乎家喻戶曉的學院之外，其實聯邦制體系的倫敦大學還有其他專攻不同領域的院校，而我今天要介紹的就是專攻藝文與傳媒的金匠學院(Goldsmiths)。</p>\n<p>如果你喜歡英國當代藝術，或是你有些許研究的話，你應該會有聽過The Young British Artists（簡稱YBAs）這個名稱，他們是一群崛起於80年代的藝術家們，並且在90年代在藝術界造成了一陣轟動，也深深影響了現在的當代藝術。在這群藝術家中有多位都是畢業於金匠學院的藝術科系，例如，在2007年推出全世界最貴的藝術作品 For the love of God的Damien Hirst跟以諷刺手法來表現兩性觀點的Sarah Lucas，他們都是金匠學院的校友。如果你喜歡時尚的話，你一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，你不知道的是，Vivian Westwood也是畢業於金匠學院的設計科系。另外，金匠學院的大眾傳播學系(Media and Communication)也是國際知名的學校，像韓國的導演也都曾來此就讀。可見金匠學院在藝文界真的是有舉足輕重的地位，也培育出了許多赫赫有名的人才。</p>\n<p>在當年就是衝著這股濃濃藝文味，於是我選擇到了金匠學院，主修大眾傳播媒體，在就讀的期間也感受到了學校的優點與缺點，在這裡就跟可能有計畫來就讀的你分享一下身為校友的個人的經驗吧！</p>\n<p>首先，有一件事你一定要先知道，在英國，大學的<strong>就讀時間大部分為三年</strong>，也就是讀完三年沒被當的話就能夠順利畢業，另外學校採用的是<strong>三學期制</strong>，跟在台灣熟悉的二學期制不太一樣，雖然這樣假期似乎看起來會比較多，但是其實課業量反而更繁重，因為相同的課程就變成要在較短的時間內學會，而且每一次的假期也就代表著一次的報告dead line，所以，想玩就必須好好做時間規劃，把時間規劃好，才能夠順利繳交報告跟同時享有假期啊！</p>\n<p>另外，我覺得倫敦大學的學校，相對於倫敦藝術大學，會顯得比較著重於理論，因為我有認識的朋友是就讀於倫敦藝術大學，他們時常都是忙於繳交作品，個人覺得是比較偏向實作方面，但是在金匠學院他們是採取<strong>理論與實作並重</strong>的方式來教課，也就是說作品量的累積跟實作的經驗比起倫敦藝術大學的同學可能會有些落差，但是在理論上知識的基礎可能會更加深厚。會這樣著重理論，是因為他們認為在知識上有著較深厚的基礎就能夠培養出批判性的思考能力(critical thinking)，創造出來的作品會更有內涵並能利用學到的知識來引導觀者，藉以得到更大的共鳴。</p>\n<p>在大眾傳播的實作課程中，你可以從八種不同的課程來做選擇，分別為：</p>\n<p>Journalism</p>\n<p>Photography</p>\n<p>Filmmaking</p>\n<p>Illustration</p>\n<p>Script and Props</p>\n<p>Animation</p>\n<p>Radio</p>\n<p>Interactive Media</p>\n<p>在一年級的時候你可以選修四種實作課程，但是到了二年級只能選修兩種，到了三年級就必須專精於其中一項，應該是希望透過嘗試讓學生找到自己有興趣或是有潛能的課程，再加以訓練跟培養。</p>\n<p>我一年級時嘗試了animation，photography，filmmaking跟illustration，二年級的時候選擇了動畫跟攝影，到三年級就專精於攝影。其中動畫課在一二年級的時候，我們學習到的是stop-motion的製作方式，還有如何製作story board，但是到了三年級會學習到使用After Effect這個軟體來製作動畫。filmmaking其實很有趣，你會跟著一個團隊一起合作，可以嘗試到像採訪陌生人，或是自己尋找演員、找場地拍攝，甚至是製作配樂等經驗。在攝影課我們使用的幾乎都是底片相機，學校都有設備可以出借，但是有規定的出借跟歸還時間。在課堂上你會學到關於攝影的理論跟實際操作器具的體驗，不只是相機的操作，還有燈光的調整，底片的沖洗等等。基本上每學年你都需要製作一個自己的project，主題很自由，老師可能會給你一個大方向，但是你可以自己決定主題去製作。</p>\n<p>不過，專精於其中一項不代表你只能侷限自己做某一個課程，相反的，學校反而鼓勵學生互相合作、學習，甚至跟不同科系的學生也都能夠互相合作來籌畫活動，推行企劃。但是，這相互間的合作通常是要靠個人去籌劃跟聯繫，學校並不會當中間人或指導人，<strong>主動性的培養</strong>，我覺得這是臺灣跟國外學校的一個最主要的差別吧！老師們，可能會提出你可以籌辦活動的想法，但是做與不做就全權交給你去決定，如果要做了，你有問題都可以主動提出，他們也都會很樂意幫助你，但比較不會主動去督促你的進度。像上必修課也都是一樣的，基本上都是lecture接著上seminar，書籍都是一開學就發放給你，在上課前你必須自己先研讀，到學校講師在講課，講課的內容會以書籍裡的內容為基礎，然後延伸出其他更深更廣的理論，有問題再在seminar上提出討論，老師並不會帶著你唸課文，所以如果你事前預習沒做好，在講師講課的時候可能就已經被搞得一頭霧水了。</p>\n<p>如果你在大學裡期待的是非常多的實作經驗，還有學校幫你選擇好的實習課程，那金匠大學可能不是屬於你的選擇，但是如果你想把底子學好跟喜歡挑戰非商業性的創作，靠自己去尋找實習機會，金匠大學就會是屬於你的不二選擇喔～</p>",
                "cover": "1519267601292148",
                "privacy_status": "public",
                "status": "publish",
                "recommended": "0",
                "blog_id": "8217810404773888",
                "postcategory_id": "7859246410633216",
                "owner_id": "8217810346053632",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:24:37",
                "created_at": "2018-03-23 16:24:37",
                "slug": "英國留遊學-倫敦大學金匠學院",
                "excerpt": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
                "og_title": "【英國留遊學】倫敦大學金匠學院",
                "og_description": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
                "meta_title": "【英國留遊學】倫敦大學金匠學院",
                "meta_description": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
                "cover_title": "倫敦大學金匠學院",
                "cover_alt": "倫敦大學金匠學院",
                "postcategory": {
                    "id": "7859246410633216",
                    "type": "global",
                    "name": "英國留學專欄",
                    "cover": "1519872393361401",
                    "status": "enable",
                    "num_items": "0",
                    "owner_id": "0",
                    "creator_id": "0",
                    "updated_at": "2018-03-22 16:29:50",
                    "created_at": "2018-03-22 16:29:50",
                    "slug": "study-aboard-UK",
                    "excerpt": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                    "og_title": "《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！",
                    "og_description": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                    "meta_title": "《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！",
                    "meta_description": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                    "cover_title": "出國留學",
                    "cover_alt": "出國留學"
                },
                "owner": {
                    "id": "8217810346053632",
                    "account": "david@english.agency",
                    "password": "$2y$10$5zcXhEAiHzYwfy5Vs7m5tObjcu3cgbvciFA5Myz4cmWgpP.ISSPWW",
                    "status": "enable",
                    "token": "070c01d7-a9c5-5614-b9ac-8f14b53ee5a7",
                    "creator_id": "0",
                    "updated_at": "2018-03-23 16:14:38",
                    "created_at": "2018-03-23 16:14:38",
                    "slug": "david_b",
                    "excerpt": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                    "og_title": "\"David B\"",
                    "og_description": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                    "meta_title": "\"David B\"",
                    "meta_description": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                    "avatar_title": "\"David B\"",
                    "avatar_alt": "\"David B\""
                },
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": "/英國留遊學-倫敦大學金匠學院-p8220321387778048"
            },
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.posts | string[] | <p>文章</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "postcategory_slug empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "postcategory_slug error"
}
```
## <a name='06.-查詢-單篇文章-by-slug'></a> 06. 查詢 單篇文章 By Slug
[Back to top](#top)

<p>・ 查詢 單篇文章 By Slug</p>

	GET /post/slug/{slug}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  slug | string | <p>文章 Slug</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "post": {
            "id": "8220321387778048",
            "title": "【英國留遊學】倫敦大學金匠學院",
            "content": "<p>倫敦大學在台灣應該是個耳熟能詳的英國大學，畢竟民進黨的黨主席蔡英文女士就是畢業於倫敦大學的其中一個學院 &ndash; 倫敦政治經濟學院 （London School of Economics and Political Science），但是除了這個幾乎家喻戶曉的學院之外，其實聯邦制體系的倫敦大學還有其他專攻不同領域的院校，而我今天要介紹的就是專攻藝文與傳媒的金匠學院(Goldsmiths)。</p>\n<p>如果你喜歡英國當代藝術，或是你有些許研究的話，你應該會有聽過The Young British Artists（簡稱YBAs）這個名稱，他們是一群崛起於80年代的藝術家們，並且在90年代在藝術界造成了一陣轟動，也深深影響了現在的當代藝術。在這群藝術家中有多位都是畢業於金匠學院的藝術科系，例如，在2007年推出全世界最貴的藝術作品 For the love of God的Damien Hirst跟以諷刺手法來表現兩性觀點的Sarah Lucas，他們都是金匠學院的校友。如果你喜歡時尚的話，你一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，你不知道的是，Vivian Westwood也是畢業於金匠學院的設計科系。另外，金匠學院的大眾傳播學系(Media and Communication)也是國際知名的學校，像韓國的導演也都曾來此就讀。可見金匠學院在藝文界真的是有舉足輕重的地位，也培育出了許多赫赫有名的人才。</p>\n<p>在當年就是衝著這股濃濃藝文味，於是我選擇到了金匠學院，主修大眾傳播媒體，在就讀的期間也感受到了學校的優點與缺點，在這裡就跟可能有計畫來就讀的你分享一下身為校友的個人的經驗吧！</p>\n<p>首先，有一件事你一定要先知道，在英國，大學的<strong>就讀時間大部分為三年</strong>，也就是讀完三年沒被當的話就能夠順利畢業，另外學校採用的是<strong>三學期制</strong>，跟在台灣熟悉的二學期制不太一樣，雖然這樣假期似乎看起來會比較多，但是其實課業量反而更繁重，因為相同的課程就變成要在較短的時間內學會，而且每一次的假期也就代表著一次的報告dead line，所以，想玩就必須好好做時間規劃，把時間規劃好，才能夠順利繳交報告跟同時享有假期啊！</p>\n<p>另外，我覺得倫敦大學的學校，相對於倫敦藝術大學，會顯得比較著重於理論，因為我有認識的朋友是就讀於倫敦藝術大學，他們時常都是忙於繳交作品，個人覺得是比較偏向實作方面，但是在金匠學院他們是採取<strong>理論與實作並重</strong>的方式來教課，也就是說作品量的累積跟實作的經驗比起倫敦藝術大學的同學可能會有些落差，但是在理論上知識的基礎可能會更加深厚。會這樣著重理論，是因為他們認為在知識上有著較深厚的基礎就能夠培養出批判性的思考能力(critical thinking)，創造出來的作品會更有內涵並能利用學到的知識來引導觀者，藉以得到更大的共鳴。</p>\n<p>在大眾傳播的實作課程中，你可以從八種不同的課程來做選擇，分別為：</p>\n<p>Journalism</p>\n<p>Photography</p>\n<p>Filmmaking</p>\n<p>Illustration</p>\n<p>Script and Props</p>\n<p>Animation</p>\n<p>Radio</p>\n<p>Interactive Media</p>\n<p>在一年級的時候你可以選修四種實作課程，但是到了二年級只能選修兩種，到了三年級就必須專精於其中一項，應該是希望透過嘗試讓學生找到自己有興趣或是有潛能的課程，再加以訓練跟培養。</p>\n<p>我一年級時嘗試了animation，photography，filmmaking跟illustration，二年級的時候選擇了動畫跟攝影，到三年級就專精於攝影。其中動畫課在一二年級的時候，我們學習到的是stop-motion的製作方式，還有如何製作story board，但是到了三年級會學習到使用After Effect這個軟體來製作動畫。filmmaking其實很有趣，你會跟著一個團隊一起合作，可以嘗試到像採訪陌生人，或是自己尋找演員、找場地拍攝，甚至是製作配樂等經驗。在攝影課我們使用的幾乎都是底片相機，學校都有設備可以出借，但是有規定的出借跟歸還時間。在課堂上你會學到關於攝影的理論跟實際操作器具的體驗，不只是相機的操作，還有燈光的調整，底片的沖洗等等。基本上每學年你都需要製作一個自己的project，主題很自由，老師可能會給你一個大方向，但是你可以自己決定主題去製作。</p>\n<p>不過，專精於其中一項不代表你只能侷限自己做某一個課程，相反的，學校反而鼓勵學生互相合作、學習，甚至跟不同科系的學生也都能夠互相合作來籌畫活動，推行企劃。但是，這相互間的合作通常是要靠個人去籌劃跟聯繫，學校並不會當中間人或指導人，<strong>主動性的培養</strong>，我覺得這是臺灣跟國外學校的一個最主要的差別吧！老師們，可能會提出你可以籌辦活動的想法，但是做與不做就全權交給你去決定，如果要做了，你有問題都可以主動提出，他們也都會很樂意幫助你，但比較不會主動去督促你的進度。像上必修課也都是一樣的，基本上都是lecture接著上seminar，書籍都是一開學就發放給你，在上課前你必須自己先研讀，到學校講師在講課，講課的內容會以書籍裡的內容為基礎，然後延伸出其他更深更廣的理論，有問題再在seminar上提出討論，老師並不會帶著你唸課文，所以如果你事前預習沒做好，在講師講課的時候可能就已經被搞得一頭霧水了。</p>\n<p>如果你在大學裡期待的是非常多的實作經驗，還有學校幫你選擇好的實習課程，那金匠大學可能不是屬於你的選擇，但是如果你想把底子學好跟喜歡挑戰非商業性的創作，靠自己去尋找實習機會，金匠大學就會是屬於你的不二選擇喔～</p>",
            "cover": "1519267601292148",
            "privacy_status": "public",
            "status": "publish",
            "recommended": "0",
            "blog_id": "8217810404773888",
            "postcategory_id": "7859246410633216",
            "owner_id": "8217810346053632",
            "creator_id": "0",
            "updated_at": "2018-03-23 16:24:37",
            "created_at": "2018-03-23 16:24:37",
            "slug": "英國留遊學-倫敦大學金匠學院",
            "excerpt": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
            "og_title": "【英國留遊學】倫敦大學金匠學院",
            "og_description": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
            "meta_title": "【英國留遊學】倫敦大學金匠學院",
            "meta_description": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
            "cover_title": "倫敦大學金匠學院",
            "cover_alt": "倫敦大學金匠學院",
            "postcategory": {
                "id": "7859246410633216",
                "type": "global",
                "name": "英國留學專欄",
                "cover": "1519872393361401",
                "status": "enable",
                "num_items": "0",
                "owner_id": "0",
                "creator_id": "0",
                "updated_at": "2018-03-22 16:29:50",
                "created_at": "2018-03-22 16:29:50",
                "slug": "study-aboard-UK",
                "excerpt": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                "og_title": "《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！",
                "og_description": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                "meta_title": "《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！",
                "meta_description": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                "cover_title": "出國留學",
                "cover_alt": "出國留學"
            },
            "owner": {
                "id": "8217810346053632",
                "account": "david@english.agency",
                "password": "$2y$10$5zcXhEAiHzYwfy5Vs7m5tObjcu3cgbvciFA5Myz4cmWgpP.ISSPWW",
                "status": "enable",
                "token": "070c01d7-a9c5-5614-b9ac-8f14b53ee5a7",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:38",
                "created_at": "2018-03-23 16:14:38",
                "slug": "david_b",
                "excerpt": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                "og_title": "\"David B\"",
                "og_description": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                "meta_title": "\"David B\"",
                "meta_description": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                "avatar_title": "\"David B\"",
                "avatar_alt": "\"David B\""
            },
            "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
            "url_path": "/英國留遊學-倫敦大學金匠學院-p8220321387778048"
        }
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.post | string | <p>文章</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "slug empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
## <a name='05.-查詢-所有文章'></a> 05. 查詢 所有文章
[Back to top](#top)

<p>・ 查詢 所有文章</p>

	GET /post/list/{status?}/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | **optional**<p>文章狀態</p>_Default value: publish_<br>_Allowed values: all,draft,publish,delete,disable_|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "posts": [
            {
                "id": "8220321387778048",
                "title": "【英國留遊學】倫敦大學金匠學院",
                "content": "<p>倫敦大學在台灣應該是個耳熟能詳的英國大學，畢竟民進黨的黨主席蔡英文女士就是畢業於倫敦大學的其中一個學院 &ndash; 倫敦政治經濟學院 （London School of Economics and Political Science），但是除了這個幾乎家喻戶曉的學院之外，其實聯邦制體系的倫敦大學還有其他專攻不同領域的院校，而我今天要介紹的就是專攻藝文與傳媒的金匠學院(Goldsmiths)。</p>\n<p>如果你喜歡英國當代藝術，或是你有些許研究的話，你應該會有聽過The Young British Artists（簡稱YBAs）這個名稱，他們是一群崛起於80年代的藝術家們，並且在90年代在藝術界造成了一陣轟動，也深深影響了現在的當代藝術。在這群藝術家中有多位都是畢業於金匠學院的藝術科系，例如，在2007年推出全世界最貴的藝術作品 For the love of God的Damien Hirst跟以諷刺手法來表現兩性觀點的Sarah Lucas，他們都是金匠學院的校友。如果你喜歡時尚的話，你一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，你不知道的是，Vivian Westwood也是畢業於金匠學院的設計科系。另外，金匠學院的大眾傳播學系(Media and Communication)也是國際知名的學校，像韓國的導演也都曾來此就讀。可見金匠學院在藝文界真的是有舉足輕重的地位，也培育出了許多赫赫有名的人才。</p>\n<p>在當年就是衝著這股濃濃藝文味，於是我選擇到了金匠學院，主修大眾傳播媒體，在就讀的期間也感受到了學校的優點與缺點，在這裡就跟可能有計畫來就讀的你分享一下身為校友的個人的經驗吧！</p>\n<p>首先，有一件事你一定要先知道，在英國，大學的<strong>就讀時間大部分為三年</strong>，也就是讀完三年沒被當的話就能夠順利畢業，另外學校採用的是<strong>三學期制</strong>，跟在台灣熟悉的二學期制不太一樣，雖然這樣假期似乎看起來會比較多，但是其實課業量反而更繁重，因為相同的課程就變成要在較短的時間內學會，而且每一次的假期也就代表著一次的報告dead line，所以，想玩就必須好好做時間規劃，把時間規劃好，才能夠順利繳交報告跟同時享有假期啊！</p>\n<p>另外，我覺得倫敦大學的學校，相對於倫敦藝術大學，會顯得比較著重於理論，因為我有認識的朋友是就讀於倫敦藝術大學，他們時常都是忙於繳交作品，個人覺得是比較偏向實作方面，但是在金匠學院他們是採取<strong>理論與實作並重</strong>的方式來教課，也就是說作品量的累積跟實作的經驗比起倫敦藝術大學的同學可能會有些落差，但是在理論上知識的基礎可能會更加深厚。會這樣著重理論，是因為他們認為在知識上有著較深厚的基礎就能夠培養出批判性的思考能力(critical thinking)，創造出來的作品會更有內涵並能利用學到的知識來引導觀者，藉以得到更大的共鳴。</p>\n<p>在大眾傳播的實作課程中，你可以從八種不同的課程來做選擇，分別為：</p>\n<p>Journalism</p>\n<p>Photography</p>\n<p>Filmmaking</p>\n<p>Illustration</p>\n<p>Script and Props</p>\n<p>Animation</p>\n<p>Radio</p>\n<p>Interactive Media</p>\n<p>在一年級的時候你可以選修四種實作課程，但是到了二年級只能選修兩種，到了三年級就必須專精於其中一項，應該是希望透過嘗試讓學生找到自己有興趣或是有潛能的課程，再加以訓練跟培養。</p>\n<p>我一年級時嘗試了animation，photography，filmmaking跟illustration，二年級的時候選擇了動畫跟攝影，到三年級就專精於攝影。其中動畫課在一二年級的時候，我們學習到的是stop-motion的製作方式，還有如何製作story board，但是到了三年級會學習到使用After Effect這個軟體來製作動畫。filmmaking其實很有趣，你會跟著一個團隊一起合作，可以嘗試到像採訪陌生人，或是自己尋找演員、找場地拍攝，甚至是製作配樂等經驗。在攝影課我們使用的幾乎都是底片相機，學校都有設備可以出借，但是有規定的出借跟歸還時間。在課堂上你會學到關於攝影的理論跟實際操作器具的體驗，不只是相機的操作，還有燈光的調整，底片的沖洗等等。基本上每學年你都需要製作一個自己的project，主題很自由，老師可能會給你一個大方向，但是你可以自己決定主題去製作。</p>\n<p>不過，專精於其中一項不代表你只能侷限自己做某一個課程，相反的，學校反而鼓勵學生互相合作、學習，甚至跟不同科系的學生也都能夠互相合作來籌畫活動，推行企劃。但是，這相互間的合作通常是要靠個人去籌劃跟聯繫，學校並不會當中間人或指導人，<strong>主動性的培養</strong>，我覺得這是臺灣跟國外學校的一個最主要的差別吧！老師們，可能會提出你可以籌辦活動的想法，但是做與不做就全權交給你去決定，如果要做了，你有問題都可以主動提出，他們也都會很樂意幫助你，但比較不會主動去督促你的進度。像上必修課也都是一樣的，基本上都是lecture接著上seminar，書籍都是一開學就發放給你，在上課前你必須自己先研讀，到學校講師在講課，講課的內容會以書籍裡的內容為基礎，然後延伸出其他更深更廣的理論，有問題再在seminar上提出討論，老師並不會帶著你唸課文，所以如果你事前預習沒做好，在講師講課的時候可能就已經被搞得一頭霧水了。</p>\n<p>如果你在大學裡期待的是非常多的實作經驗，還有學校幫你選擇好的實習課程，那金匠大學可能不是屬於你的選擇，但是如果你想把底子學好跟喜歡挑戰非商業性的創作，靠自己去尋找實習機會，金匠大學就會是屬於你的不二選擇喔～</p>",
                "cover": "1519267601292148",
                "privacy_status": "public",
                "status": "publish",
                "recommended": "0",
                "blog_id": "8217810404773888",
                "postcategory_id": "7859246410633216",
                "owner_id": "8217810346053632",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:24:37",
                "created_at": "2018-03-23 16:24:37",
                "slug": "英國留遊學-倫敦大學金匠學院",
                "excerpt": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
                "og_title": "【英國留遊學】倫敦大學金匠學院",
                "og_description": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
                "meta_title": "【英國留遊學】倫敦大學金匠學院",
                "meta_description": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
                "cover_title": "倫敦大學金匠學院",
                "cover_alt": "倫敦大學金匠學院",
                "postcategory": {
                    "id": "7859246410633216",
                    "type": "global",
                    "name": "英國留學專欄",
                    "cover": "1519872393361401",
                    "status": "enable",
                    "num_items": "0",
                    "owner_id": "0",
                    "creator_id": "0",
                    "updated_at": "2018-03-22 16:29:50",
                    "created_at": "2018-03-22 16:29:50",
                    "slug": "study-aboard-UK",
                    "excerpt": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                    "og_title": "《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！",
                    "og_description": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                    "meta_title": "《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！",
                    "meta_description": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                    "cover_title": "出國留學",
                    "cover_alt": "出國留學"
                },
                "owner": {
                    "id": "8217810346053632",
                    "account": "david@english.agency",
                    "password": "$2y$10$5zcXhEAiHzYwfy5Vs7m5tObjcu3cgbvciFA5Myz4cmWgpP.ISSPWW",
                    "status": "enable",
                    "token": "070c01d7-a9c5-5614-b9ac-8f14b53ee5a7",
                    "creator_id": "0",
                    "updated_at": "2018-03-23 16:14:38",
                    "created_at": "2018-03-23 16:14:38",
                    "slug": "david_b",
                    "excerpt": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                    "og_title": "\"David B\"",
                    "og_description": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                    "meta_title": "\"David B\"",
                    "meta_description": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                    "avatar_title": "\"David B\"",
                    "avatar_alt": "\"David B\""
                },
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": "/英國留遊學-倫敦大學金匠學院-p8220321387778048"
            },
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.posts | string[] | <p>文章</p>|

### Error Response

Response: 400

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
## <a name='07.-查詢-被推薦文章-by-postcategoryid'></a> 07. 查詢 被推薦文章 By PostcategoryId
[Back to top](#top)

<p>・ 查詢 被推薦文章 By PostcategoryId</p>

	GET /post/recommended/postcategory/{postcategory_id}/{status?}/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  postcategory_id | string | <p>文章分類 ID</p>|
|  status | string | **optional**<p>文章狀態</p>_Default value: publish_<br>_Allowed values: all,draft,publish,delete,disable_|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "posts": [
            {
                "id": "8220321387778048",
                "title": "【英國留遊學】倫敦大學金匠學院",
                "content": "<p>倫敦大學在台灣應該是個耳熟能詳的英國大學，畢竟民進黨的黨主席蔡英文女士就是畢業於倫敦大學的其中一個學院 &ndash; 倫敦政治經濟學院 （London School of Economics and Political Science），但是除了這個幾乎家喻戶曉的學院之外，其實聯邦制體系的倫敦大學還有其他專攻不同領域的院校，而我今天要介紹的就是專攻藝文與傳媒的金匠學院(Goldsmiths)。</p>\n<p>如果你喜歡英國當代藝術，或是你有些許研究的話，你應該會有聽過The Young British Artists（簡稱YBAs）這個名稱，他們是一群崛起於80年代的藝術家們，並且在90年代在藝術界造成了一陣轟動，也深深影響了現在的當代藝術。在這群藝術家中有多位都是畢業於金匠學院的藝術科系，例如，在2007年推出全世界最貴的藝術作品 For the love of God的Damien Hirst跟以諷刺手法來表現兩性觀點的Sarah Lucas，他們都是金匠學院的校友。如果你喜歡時尚的話，你一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，你不知道的是，Vivian Westwood也是畢業於金匠學院的設計科系。另外，金匠學院的大眾傳播學系(Media and Communication)也是國際知名的學校，像韓國的導演也都曾來此就讀。可見金匠學院在藝文界真的是有舉足輕重的地位，也培育出了許多赫赫有名的人才。</p>\n<p>在當年就是衝著這股濃濃藝文味，於是我選擇到了金匠學院，主修大眾傳播媒體，在就讀的期間也感受到了學校的優點與缺點，在這裡就跟可能有計畫來就讀的你分享一下身為校友的個人的經驗吧！</p>\n<p>首先，有一件事你一定要先知道，在英國，大學的<strong>就讀時間大部分為三年</strong>，也就是讀完三年沒被當的話就能夠順利畢業，另外學校採用的是<strong>三學期制</strong>，跟在台灣熟悉的二學期制不太一樣，雖然這樣假期似乎看起來會比較多，但是其實課業量反而更繁重，因為相同的課程就變成要在較短的時間內學會，而且每一次的假期也就代表著一次的報告dead line，所以，想玩就必須好好做時間規劃，把時間規劃好，才能夠順利繳交報告跟同時享有假期啊！</p>\n<p>另外，我覺得倫敦大學的學校，相對於倫敦藝術大學，會顯得比較著重於理論，因為我有認識的朋友是就讀於倫敦藝術大學，他們時常都是忙於繳交作品，個人覺得是比較偏向實作方面，但是在金匠學院他們是採取<strong>理論與實作並重</strong>的方式來教課，也就是說作品量的累積跟實作的經驗比起倫敦藝術大學的同學可能會有些落差，但是在理論上知識的基礎可能會更加深厚。會這樣著重理論，是因為他們認為在知識上有著較深厚的基礎就能夠培養出批判性的思考能力(critical thinking)，創造出來的作品會更有內涵並能利用學到的知識來引導觀者，藉以得到更大的共鳴。</p>\n<p>在大眾傳播的實作課程中，你可以從八種不同的課程來做選擇，分別為：</p>\n<p>Journalism</p>\n<p>Photography</p>\n<p>Filmmaking</p>\n<p>Illustration</p>\n<p>Script and Props</p>\n<p>Animation</p>\n<p>Radio</p>\n<p>Interactive Media</p>\n<p>在一年級的時候你可以選修四種實作課程，但是到了二年級只能選修兩種，到了三年級就必須專精於其中一項，應該是希望透過嘗試讓學生找到自己有興趣或是有潛能的課程，再加以訓練跟培養。</p>\n<p>我一年級時嘗試了animation，photography，filmmaking跟illustration，二年級的時候選擇了動畫跟攝影，到三年級就專精於攝影。其中動畫課在一二年級的時候，我們學習到的是stop-motion的製作方式，還有如何製作story board，但是到了三年級會學習到使用After Effect這個軟體來製作動畫。filmmaking其實很有趣，你會跟著一個團隊一起合作，可以嘗試到像採訪陌生人，或是自己尋找演員、找場地拍攝，甚至是製作配樂等經驗。在攝影課我們使用的幾乎都是底片相機，學校都有設備可以出借，但是有規定的出借跟歸還時間。在課堂上你會學到關於攝影的理論跟實際操作器具的體驗，不只是相機的操作，還有燈光的調整，底片的沖洗等等。基本上每學年你都需要製作一個自己的project，主題很自由，老師可能會給你一個大方向，但是你可以自己決定主題去製作。</p>\n<p>不過，專精於其中一項不代表你只能侷限自己做某一個課程，相反的，學校反而鼓勵學生互相合作、學習，甚至跟不同科系的學生也都能夠互相合作來籌畫活動，推行企劃。但是，這相互間的合作通常是要靠個人去籌劃跟聯繫，學校並不會當中間人或指導人，<strong>主動性的培養</strong>，我覺得這是臺灣跟國外學校的一個最主要的差別吧！老師們，可能會提出你可以籌辦活動的想法，但是做與不做就全權交給你去決定，如果要做了，你有問題都可以主動提出，他們也都會很樂意幫助你，但比較不會主動去督促你的進度。像上必修課也都是一樣的，基本上都是lecture接著上seminar，書籍都是一開學就發放給你，在上課前你必須自己先研讀，到學校講師在講課，講課的內容會以書籍裡的內容為基礎，然後延伸出其他更深更廣的理論，有問題再在seminar上提出討論，老師並不會帶著你唸課文，所以如果你事前預習沒做好，在講師講課的時候可能就已經被搞得一頭霧水了。</p>\n<p>如果你在大學裡期待的是非常多的實作經驗，還有學校幫你選擇好的實習課程，那金匠大學可能不是屬於你的選擇，但是如果你想把底子學好跟喜歡挑戰非商業性的創作，靠自己去尋找實習機會，金匠大學就會是屬於你的不二選擇喔～</p>",
                "cover": "1519267601292148",
                "privacy_status": "public",
                "status": "publish",
                "recommended": "0",
                "blog_id": "8217810404773888",
                "postcategory_id": "7859246410633216",
                "owner_id": "8217810346053632",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:24:37",
                "created_at": "2018-03-23 16:24:37",
                "slug": "英國留遊學-倫敦大學金匠學院",
                "excerpt": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
                "og_title": "【英國留遊學】倫敦大學金匠學院",
                "og_description": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
                "meta_title": "【英國留遊學】倫敦大學金匠學院",
                "meta_description": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
                "cover_title": "倫敦大學金匠學院",
                "cover_alt": "倫敦大學金匠學院",
                "postcategory": {
                    "id": "7859246410633216",
                    "type": "global",
                    "name": "英國留學專欄",
                    "cover": "1519872393361401",
                    "status": "enable",
                    "num_items": "0",
                    "owner_id": "0",
                    "creator_id": "0",
                    "updated_at": "2018-03-22 16:29:50",
                    "created_at": "2018-03-22 16:29:50",
                    "slug": "study-aboard-UK",
                    "excerpt": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                    "og_title": "《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！",
                    "og_description": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                    "meta_title": "《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！",
                    "meta_description": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                    "cover_title": "出國留學",
                    "cover_alt": "出國留學"
                },
                "owner": {
                    "id": "8217810346053632",
                    "account": "david@english.agency",
                    "password": "$2y$10$5zcXhEAiHzYwfy5Vs7m5tObjcu3cgbvciFA5Myz4cmWgpP.ISSPWW",
                    "status": "enable",
                    "token": "070c01d7-a9c5-5614-b9ac-8f14b53ee5a7",
                    "creator_id": "0",
                    "updated_at": "2018-03-23 16:14:38",
                    "created_at": "2018-03-23 16:14:38",
                    "slug": "david_b",
                    "excerpt": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                    "og_title": "\"David B\"",
                    "og_description": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                    "meta_title": "\"David B\"",
                    "meta_description": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                    "avatar_title": "\"David B\"",
                    "avatar_alt": "\"David B\""
                },
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": "/英國留遊學-倫敦大學金匠學院-p8220321387778048"
            },
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.posts | string[] | <p>文章</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "postcategory_id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
## <a name='01.-新增文章'></a> 01. 新增文章
[Back to top](#top)

<p>・ 新增文章</p>

	POST /post

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  title | string | <p>標題</p>|
|  content | string | <p>內容</p>|
|  cover | string | <p>封面</p>|
|  privacy_status | string | **optional**<p>隱私狀態</p>_Default value: public_<br>_Allowed values: public,private_|
|  status | string | **optional**<p>狀態</p>_Default value: draft_<br>_Allowed values: draft,publish,delete,disable_|
|  recommended | string | **optional**<p>被推薦</p>_Default value: false_<br>_Allowed values: false,true_|
|  postcategory_id | string | <p>文章分類 id</p>|
|  owner_id | string | <p>擁有者 id</p>|
|  slug | string | <p>位址</p>|
|  excerpt | string | <p>摘要</p>|
|  canonical_url | string | **optional**<p>來源位址</p>|
|  og_title | string | <p>og 標題</p>|
|  og_description | string | <p>og 描述</p>|
|  meta_title | string | <p>meta 標題</p>|
|  meta_description | string | <p>meta 描述</p>|
|  cover_title | string | <p>封面標題</p>|
|  cover_alt | string | <p>封面 alt</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "create success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "post_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.post_id | string | <p>文章 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "title empty"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "content empty"
}
```
Response: 400.05

```
{
    "status": "fail",
    "code": 400,
    "comment": "cover empty"
}
```
Response: 400.06

```
{
    "status": "fail",
    "code": 400,
    "comment": "postcategory_id empty"
}
```
Response: 400.07

```
{
    "status": "fail",
    "code": 400,
    "comment": "owner_id empty"
}
```
Response: 400.08

```
{
    "status": "fail",
    "code": 400,
    "comment": "slug empty"
}
```
Response: 400.09

```
{
    "status": "fail",
    "code": 400,
    "comment": "excerpt empty"
}
```
Response: 400.10

```
{
    "status": "fail",
    "code": 400,
    "comment": "og_title empty"
}
```
Response: 400.11

```
{
    "status": "fail",
    "code": 400,
    "comment": "og_decription empty"
}
```
Response: 400.12

```
{
    "status": "fail",
    "code": 400,
    "comment": "meta_title empty"
}
```
Response: 400.13

```
{
    "status": "fail",
    "code": 400,
    "comment": "meta_description empty"
}
```
Response: 400.14

```
{
    "status": "fail",
    "code": 400,
    "comment": "cover_title empty"
}
```
Response: 400.15

```
{
    "status": "fail",
    "code": 400,
    "comment": "cover_alt empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "privacy_status error"
}
```
Response: 422.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 422.04

```
{
    "status": "fail",
    "code": 422,
    "comment": "recommended error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "owner error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "postcategory error"
}
```
Response: 409.01

```
{
    "status": "fail",
    "code": 409,
    "comment": "slug error"
}
```
Response: 404.03

```
{
    "status": "fail",
    "code": 404,
    "comment": "cover error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "create error"
}
```
## <a name='10.-上傳封面檔案'></a> 10. 上傳封面檔案
[Back to top](#top)

<p>・ 上傳封面檔案</p>

	POST /post/cover

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  owner_id | string | <p>擁有者</p>|
|  privacy_status | string | **optional**<p>隱私狀態</p>_Default value: public_<br>_Allowed values: public,private_|
|  status | string | **optional**<p>狀態</p>_Default value: disable_<br>_Allowed values: enable,disable,delete_|
|  file | file | <p>檔案</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "upload cover success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "filename": "1517067835270712",
        "cover_links": {
            "c": "gallery/1517067835270712_c.jpeg",
            "z": "gallery/1517067835270712_z.jpeg",
            "n": "gallery/1517067835270712_n.jpeg",
            "q": "gallery/1517067835270712_q.jpeg",
            "sq": "gallery/1517067835270712_sq.jpeg",
            "o": "gallery/1517067835270712_o.jpeg"
        }
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.filename | string | <p>檔案名稱</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.cover_links | string[] | <p>各尺寸圖檔連結</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "owner_id empty"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "file empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "privacy_status error"
}
```
Response: 422.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 422.04

```
{
    "status": "fail",
    "code": 422,
    "comment": "file error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "owner_id error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "create error"
}
```
## <a name='02.-修改文章'></a> 02. 修改文章
[Back to top](#top)

<p>・ 修改文章</p>

	PUT /post/{id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>文章 ID</p>|
|  title | string | **optional**<p>標題</p>|
|  content | string | **optional**<p>內容</p>|
|  cover | string | **optional**<p>封面</p>|
|  privacy_status | string | **optional**<p>隱私狀態</p>_Allowed values: public,private_|
|  status | string | **optional**<p>狀態</p>_Allowed values: draft,publish,delete,disable_|
|  recommended | string | **optional**<p>被推薦</p>_Allowed values: false,true_|
|  postcategory_id | string | **optional**<p>文章分類 id</p>|
|  owner_id | string | **optional**<p>擁有者 id</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "update success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "post_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.post_id | string | <p>文章 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "post error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "cover error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "privacy_status error"
}
```
Response: 422.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 422.04

```
{
    "status": "fail",
    "code": 422,
    "comment": "recommended error"
}
```
Response: 404.03

```
{
    "status": "fail",
    "code": 404,
    "comment": "owner error"
}
```
Response: 404.04

```
{
    "status": "fail",
    "code": 404,
    "comment": "postcategory error"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "updateData empty"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "update error"
}
```
## <a name='11.-修改文章-seo'></a> 11. 修改文章 SEO
[Back to top](#top)

<p>・ 修改文章 SEO</p>

	PUT /post/{id}/seo

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>文章分類 ID</p>|
|  slug | string | **optional**<p>位址</p>|
|  excerpt | string | **optional**<p>摘要</p>|
|  canonical_url | string | **optional**<p>來源位址</p>|
|  og_title | string | **optional**<p>og 標題</p>|
|  og_description | string | **optional**<p>og 描述</p>|
|  meta_title | string | **optional**<p>meta 標題</p>|
|  meta_description | string | **optional**<p>meta 描述</p>|
|  cover_title | string | **optional**<p>封面標題</p>|
|  cover_alt | string | **optional**<p>封面 alt</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "update seo success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "post_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.post_id | string | <p>文章分類 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "post error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "post seo error"
}
```
Response: 409.01

```
{
    "status": "fail",
    "code": 409,
    "comment": "slug error"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "updateData empty"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "update error"
}
```
# <a name='postcategory'></a> Postcategory

## <a name='03.-刪除文章分類'></a> 03. 刪除文章分類
[Back to top](#top)

<p>・ 刪除文章分類</p>

	DELETE /postcategory/{id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>文章分類 ID                                        [slug]             位址</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "delete success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "postcategory_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.postcategory_id | string | <p>分類 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "postcategory error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "delete error"
}
```
## <a name='04.-查詢-單篇文章分類'></a> 04. 查詢 單篇文章分類
[Back to top](#top)

<p>・ 查詢 單篇文章分類</p>

	GET /postcategory/{id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>文章分類 ID</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "postcategory": {
            "id": "7859246544850944",
            "type": "global",
            "name": "加拿大留學專欄",
            "cover": "1519872532384543",
            "status": "enable",
            "num_items": "0",
            "owner_id": "0",
            "creator_id": "0",
            "updated_at": "2018-03-22 16:29:50",
            "created_at": "2018-03-22 16:29:50",
            "slug": "study-aboard-canada",
            "excerpt": "EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。",
            "og_title": "《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！",
            "og_description": "EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。",
            "meta_title": "《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！",
            "meta_description": "study-aboard-canada",
            "cover_title": "加拿大留學",
            "cover_alt": "加拿大留學",
            "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
            "url_path": "/study-aboard-canada-c7859246544850944"
        }
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.postcategory | string | <p>文章分類</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
## <a name='08.-查詢-文章分類-by-ownerid'></a> 08. 查詢 文章分類 By OwnerId
[Back to top](#top)

<p>・ 查詢 所有文章 By OwnerId</p>

	GET /postcategory/owner/{owner_id}/{status?}/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  owner_id | string | <p>文章分類擁有者</p>|
|  status | string | **optional**<p>文章分類狀態</p>_Default value: enable_<br>_Allowed values: all,enable,delete,disable_|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "postategories": [
            {
                "id": "7859246544850944",
                "type": "global",
                "name": "加拿大留學專欄",
                "cover": "1519872532384543",
                "status": "enable",
                "num_items": "0",
                "owner_id": "0",
                "creator_id": "0",
                "updated_at": "2018-03-22 16:29:50",
                "created_at": "2018-03-22 16:29:50",
                "slug": "study-aboard-canada",
                "excerpt": "EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。",
                "og_title": "《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！",
                "og_description": "EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。",
                "meta_title": "《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！",
                "meta_description": "study-aboard-canada",
                "cover_title": "加拿大留學",
                "cover_alt": "加拿大留學",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": "/study-aboard-canada-c7859246544850944"
            },
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.postategories | string[] | <p>文章分類s</p>|

### Error Response

Response: 400

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
## <a name='06.-查詢-單篇文章分類-by-slug'></a> 06. 查詢 單篇文章分類 By Slug
[Back to top](#top)

<p>・ 查詢 單篇文章分類 By Slug</p>

	GET /postcategory/slug/{slug}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  slug | string | <p>文章分類 Slug</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "postcategory": {
            "id": "7859246544850944",
            "type": "global",
            "name": "加拿大留學專欄",
            "cover": "1519872532384543",
            "status": "enable",
            "num_items": "0",
            "owner_id": "0",
            "creator_id": "0",
            "updated_at": "2018-03-22 16:29:50",
            "created_at": "2018-03-22 16:29:50",
            "slug": "study-aboard-canada",
            "excerpt": "EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。",
            "og_title": "《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！",
            "og_description": "EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。",
            "meta_title": "《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！",
            "meta_description": "study-aboard-canada",
            "cover_title": "加拿大留學",
            "cover_alt": "加拿大留學",
            "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
            "url_path": "/study-aboard-canada-c7859246544850944"
        }
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.postcategory | string | <p>文章分類</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "slug empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
## <a name='07.-查詢-文章分類-by-type'></a> 07. 查詢 文章分類 By Type
[Back to top](#top)

<p>・ 查詢 所有文章 By Type</p>

	GET /postcategory/type/{type}/{status?}/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  type | string | <p>文章分類類型</p>_Allowed values: global,user_|
|  status | string | **optional**<p>文章分類狀態</p>_Default value: enable_<br>_Allowed values: all,enable,delete,disable_|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "postategories": [
            {
                "id": "7859246544850944",
                "type": "global",
                "name": "加拿大留學專欄",
                "cover": "1519872532384543",
                "status": "enable",
                "num_items": "0",
                "owner_id": "0",
                "creator_id": "0",
                "updated_at": "2018-03-22 16:29:50",
                "created_at": "2018-03-22 16:29:50",
                "slug": "study-aboard-canada",
                "excerpt": "EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。",
                "og_title": "《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！",
                "og_description": "EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。",
                "meta_title": "《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！",
                "meta_description": "study-aboard-canada",
                "cover_title": "加拿大留學",
                "cover_alt": "加拿大留學",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": "/study-aboard-canada-c7859246544850944"
            },
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.postategories | string[] | <p>文章分類s</p>|

### Error Response

Response: 400

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "type error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 422.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
## <a name='09.-查詢-文章分類-by-type-and-ownerid'></a> 09. 查詢 文章分類 By Type and OwnerId
[Back to top](#top)

<p>・ 查詢 所有文章 By Type and OwnerId</p>

	GET /postcategory/type/{type}/owner/{owner_id}/{status?}/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  type | string | <p>文章分類類型</p>_Allowed values: global,user_|
|  owner_id | string | <p>文章分類擁有者</p>|
|  status | string | **optional**<p>文章分類狀態</p>_Default value: enable_<br>_Allowed values: all,enable,delete,disable_|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "postategories": [
            {
                "id": "7859246544850944",
                "type": "global",
                "name": "加拿大留學專欄",
                "cover": "1519872532384543",
                "status": "enable",
                "num_items": "0",
                "owner_id": "0",
                "creator_id": "0",
                "updated_at": "2018-03-22 16:29:50",
                "created_at": "2018-03-22 16:29:50",
                "slug": "study-aboard-canada",
                "excerpt": "EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。",
                "og_title": "《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！",
                "og_description": "EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。",
                "meta_title": "《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！",
                "meta_description": "study-aboard-canada",
                "cover_title": "加拿大留學",
                "cover_alt": "加拿大留學",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": "/study-aboard-canada-c7859246544850944"
            },
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.postategories | string[] | <p>文章分類s</p>|

### Error Response

Response: 400

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "type error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 422.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
## <a name='05.-查詢-文章分類'></a> 05. 查詢 文章分類
[Back to top](#top)

<p>・ 查詢 所有文章</p>

	GET /postcategory/list/{status?}/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | **optional**<p>文章分類狀態</p>_Default value: enable_<br>_Allowed values: all,enable,delete,disable_|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "postategories": [
            {
                "id": "7859246544850944",
                "type": "global",
                "name": "加拿大留學專欄",
                "cover": "1519872532384543",
                "status": "enable",
                "num_items": "0",
                "owner_id": "0",
                "creator_id": "0",
                "updated_at": "2018-03-22 16:29:50",
                "created_at": "2018-03-22 16:29:50",
                "slug": "study-aboard-canada",
                "excerpt": "EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。",
                "og_title": "《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！",
                "og_description": "EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。",
                "meta_title": "《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！",
                "meta_description": "study-aboard-canada",
                "cover_title": "加拿大留學",
                "cover_alt": "加拿大留學",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": "/study-aboard-canada-c7859246544850944"
            },
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.postategories | string[] | <p>文章分類s</p>|

### Error Response

Response: 400

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
## <a name='01.-新增文章分類'></a> 01. 新增文章分類
[Back to top](#top)

<p>・ 新增文章分類</p>

	POST /postcategory

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  type | string | **optional**<p>類型</p>_Default value: global_<br>_Allowed values: global,user_|
|  name | string | <p>名稱</p>|
|  cover | string | <p>封面</p>|
|  status | string | **optional**<p>狀態</p>_Default value: enable_<br>_Allowed values: enable,delete,disable_|
|  slug | string | <p>位址</p>|
|  excerpt | string | <p>摘要</p>|
|  og_title | string | <p>og 標題</p>|
|  og_description | string | <p>og 描述</p>|
|  meta_title | string | <p>meta 標題</p>|
|  meta_description | string | <p>meta 描述</p>|
|  cover_title | string | <p>封面標題</p>|
|  cover_alt | string | <p>封面 alt</p>|
|  owner_id | string | **optional**<p>擁有者 id</p>_Default value: 0_<br>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "create success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "postcategory_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.postcategory_id | string | <p>文章分類 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "name empty"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "cover empty"
}
```
Response: 400.05

```
{
    "status": "fail",
    "code": 400,
    "comment": "slug empty"
}
```
Response: 400.06

```
{
    "status": "fail",
    "code": 400,
    "comment": "excerpt empty"
}
```
Response: 400.07

```
{
    "status": "fail",
    "code": 400,
    "comment": "og_title empty"
}
```
Response: 400.08

```
{
    "status": "fail",
    "code": 400,
    "comment": "og_decription empty"
}
```
Response: 400.09

```
{
    "status": "fail",
    "code": 400,
    "comment": "meta_title empty"
}
```
Response: 400.10

```
{
    "status": "fail",
    "code": 400,
    "comment": "meta_description empty"
}
```
Response: 400.11

```
{
    "status": "fail",
    "code": 400,
    "comment": "cover_title empty"
}
```
Response: 400.12

```
{
    "status": "fail",
    "code": 400,
    "comment": "cover_alt empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "type error"
}
```
Response: 422.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 409.01

```
{
    "status": "fail",
    "code": 409,
    "comment": "slug error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "cover error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "dir error"
}
```
Response: 409.02

```
{
    "status": "fail",
    "code": 409,
    "comment": "name error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "create error"
}
```
## <a name='10.-上傳封面檔案'></a> 10. 上傳封面檔案
[Back to top](#top)

<p>・ 上傳封面檔案</p>

	POST /postcategory/cover

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  owner_id | string | **optional**<p>擁有者</p>_Default value: 0_<br>|
|  privacy_status | string | **optional**<p>隱私狀態</p>_Default value: public_<br>_Allowed values: public,private_|
|  status | string | **optional**<p>狀態</p>_Default value: disable_<br>_Allowed values: enable,disable,delete_|
|  file | file | <p>檔案</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "upload cover success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "filename": "1517067835270712",
        "cover_links": {
            "c": "gallery/1517067835270712_c.jpeg",
            "z": "gallery/1517067835270712_z.jpeg",
            "n": "gallery/1517067835270712_n.jpeg",
            "q": "gallery/1517067835270712_q.jpeg",
            "sq": "gallery/1517067835270712_sq.jpeg",
            "o": "gallery/1517067835270712_o.jpeg"
        }
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.filename | string | <p>檔案名稱</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.cover_links | string[] | <p>各尺寸圖檔連結</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "file empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "privacy_status error"
}
```
Response: 422.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 422.04

```
{
    "status": "fail",
    "code": 422,
    "comment": "file error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "create error"
}
```
## <a name='02.-修改文章分類'></a> 02. 修改文章分類
[Back to top](#top)

<p>・ 修改文章分類</p>

	PUT /postcategory/{id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>文章分類 ID</p>|
|  type | string | **optional**<p>類型</p>_Allowed values: global,user_|
|  name | string | **optional**<p>名稱</p>|
|  cover | string | **optional**<p>封面</p>|
|  status | string | **optional**<p>狀態</p>_Allowed values: enable,delete,disable_|
|  slug | string | **optional**<p>位址</p>|
|  excerpt | string | **optional**<p>摘要</p>|
|  og_title | string | **optional**<p>og 標題</p>|
|  og_description | string | **optional**<p>og 描述</p>|
|  meta_title | string | **optional**<p>meta 標題</p>|
|  meta_description | string | **optional**<p>meta 描述</p>|
|  cover_title | string | **optional**<p>封面標題</p>|
|  cover_alt | string | **optional**<p>封面 alt</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "update success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "postcategory_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.postcategory_id | string | <p>文章分類 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "postcategory error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "type error"
}
```
Response: 404.03

```
{
    "status": "fail",
    "code": 404,
    "comment": "cover error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 409.01

```
{
    "status": "fail",
    "code": 409,
    "comment": "slug error"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "updateData  / updateSeoData empty"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "update error"
}
```
## <a name='11.-修改文章分類-seo'></a> 11. 修改文章分類 SEO
[Back to top](#top)

<p>・ 修改文章分類 SEO</p>

	PUT /postcategory/{id}/seo

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>文章分類 ID</p>|
|  slug | string | **optional**<p>位址</p>|
|  excerpt | string | **optional**<p>摘要</p>|
|  og_title | string | **optional**<p>og 標題</p>|
|  og_description | string | **optional**<p>og 描述</p>|
|  meta_title | string | **optional**<p>meta 標題</p>|
|  meta_description | string | **optional**<p>meta 描述</p>|
|  cover_title | string | **optional**<p>封面標題</p>|
|  cover_alt | string | **optional**<p>封面 alt</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "update seo success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "postcategory_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.postcategory_id | string | <p>文章分類 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "postcategory error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "postcategory seo error"
}
```
Response: 409.01

```
{
    "status": "fail",
    "code": 409,
    "comment": "slug error"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "updateData empty"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "update error"
}
```
# <a name='register'></a> Register

## <a name='01.-註冊'></a> 01. 註冊
[Back to top](#top)

<p>・ 使用者註冊</p>

	POST /register

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  account | string | <p>使用者帳號</p>|
|  password | string | <p>密碼</p>|
|  password_repeat | string | <p>確認密碼</p>|
|  first_name | string | <p>名</p>|
|  last_name | string | <p>姓</p>|
|  mobile_country_code | string | <p>手機號碼國碼</p>|
|  mobile_phone | string | <p>手機號碼</p>|

### Success Response

Response

```
{
    "status": "success",
    "code": 201,
    "comment": "register success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "token": "8f3e973061800dc6ebcb367079b305d8"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "account is empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "password / password_repeat is empty"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "first_name is empty"
}
```
Response: 400.05

```
{
    "status": "fail",
    "code": 400,
    "comment": "last_name is empty"
}
```
Response: 400.06

```
{
    "status": "fail",
    "code": 400,
    "comment": "mobile_country_code is empty"
}
```
Response: 400.07

```
{
    "status": "fail",
    "code": 400,
    "comment": "mobile_phone is empty"
}
```
Response: 403.01

```
{
    "status": "fail",
    "code": 403,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "account error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "password' length < 8"
}
```
Response: 422.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "password / password_repeat is NOT equal"
}
```
Response: 422.04

```
{
    "status": "fail",
    "code": 422,
    "comment": "mobile_phone format error"
}
```
Response: 409.01

```
{
    "status": "fail",
    "code": 409,
    "comment": "account error"
}
```
Response: 409.02

```
{
    "status": "fail",
    "code": 409,
    "comment": "mobile_phone error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "create user error"
}
```
# <a name='role'></a> Role

## <a name='04.-查詢-單一角色'></a> 04. 查詢 單一角色
[Back to top](#top)

<p>・ 查詢 單一角色</p>

	GET /role/{id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>角色 ID</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "role": {
            "id": "5324389482631168",
            "type": "general",
            "code": "admin",
            "note": "網站最高管理員",
            "creator_id": "0",
            "updated_at": "2018-03-15 16:37:13",
            "created_at": "2018-03-15 16:37:13",
            "permissions": [
                {
                    "id": "5324389491019776",
                    "type": "admin",
                    "code": "role_create",
                    "note": "新增角色",
                    "creator_id": "0",
                    "updated_at": "2018-03-15 16:37:13",
                    "created_at": "2018-03-15 16:37:13"
                },
                {
                    "id": "5324389507796992",
                    "type": "admin",
                    "code": "role_update",
                    "note": "更新角色",
                    "creator_id": "0",
                    "updated_at": "2018-03-15 16:37:13",
                    "created_at": "2018-03-15 16:37:13"
                },
                {
                    "id": "5324389545545728",
                    "type": "admin",
                    "code": "role_delete",
                    "note": "刪除角色",
                    "creator_id": "0",
                    "updated_at": "2018-03-15 16:37:13",
                    "created_at": "2018-03-15 16:37:13"
                },
                {
                    "id": "5324389562322944",
                    "type": "admin",
                    "code": "locales_create",
                    "note": "新增語系",
                    "creator_id": "0",
                    "updated_at": "2018-03-15 16:37:13",
                    "created_at": "2018-03-15 16:37:13"
                },
                {
                    "id": "5324389570711552",
                    "type": "admin",
                    "code": "locales_update",
                    "note": "更新語系",
                    "creator_id": "0",
                    "updated_at": "2018-03-15 16:37:13",
                    "created_at": "2018-03-15 16:37:13"
                },
                {
                    "id": "5324389587488768",
                    "type": "admin",
                    "code": "locales_delete",
                    "note": "刪除語系",
                    "creator_id": "0",
                    "updated_at": "2018-03-15 16:37:13",
                    "created_at": "2018-03-15 16:37:13"
                }
            ]
        }
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.role | string | <p>角色</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
## <a name='07.-查詢-所有角色-by-code'></a> 07. 查詢 所有角色 By Code
[Back to top](#top)

<p>・ 查詢 所有角色 By Code</p>

	GET /role/code/{code}/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  code | string | <p>角色代號</p>|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "roles": [
            {
                "id": "5324389482631168",
                "type": "general",
                "code": "admin",
                "note": "網站最高管理員",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389491019776",
                        "type": "admin",
                        "code": "role_create",
                        "note": "新增角色",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389507796992",
                        "type": "admin",
                        "code": "role_update",
                        "note": "更新角色",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389545545728",
                        "type": "admin",
                        "code": "role_delete",
                        "note": "刪除角色",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389562322944",
                        "type": "admin",
                        "code": "locales_create",
                        "note": "新增語系",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389570711552",
                        "type": "admin",
                        "code": "locales_update",
                        "note": "更新語系",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389587488768",
                        "type": "admin",
                        "code": "locales_delete",
                        "note": "刪除語系",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            },
            {
                "id": "5324389914644480",
                "type": "company",
                "code": "admin",
                "note": "公司管理員",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389918838784",
                        "type": "company",
                        "code": "report_show",
                        "note": "觀看統計數據",
                        "creator_id": "0",
                        "updated_at": "2018-03-22 22:29:58",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389939810304",
                        "type": "company",
                        "code": "quotation_create",
                        "note": "新增學生報名報價單",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389952393216",
                        "type": "company",
                        "code": "quotation_show",
                        "note": "觀看每筆報名的狀態",
                        "creator_id": "0",
                        "updated_at": "2018-03-22 22:29:39",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            },
            {
                "id": "5324389994336256",
                "type": "school",
                "code": "admin",
                "note": "學校管理員",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389998530560",
                        "type": "school",
                        "code": "report_show",
                        "note": "觀看統計數據",
                        "creator_id": "0",
                        "updated_at": "2018-03-22 22:29:39",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324390011113472",
                        "type": "school",
                        "code": "quotation_verify",
                        "note": "審核學生報名報價單",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            }
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.roles | string[] | <p>角色s</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "code empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
## <a name='06.-查詢-所有角色-by-type'></a> 06. 查詢 所有角色 By Type
[Back to top](#top)

<p>・ 查詢 所有角色 By Type</p>

	GET /role/type/{type}/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  type | string | <p>角色類型</p>_Allowed values: general,school,company_|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "roles": [
            {
                "id": "5324389482631168",
                "type": "general",
                "code": "admin",
                "note": "網站最高管理員",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389491019776",
                        "type": "admin",
                        "code": "role_create",
                        "note": "新增角色",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389507796992",
                        "type": "admin",
                        "code": "role_update",
                        "note": "更新角色",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389545545728",
                        "type": "admin",
                        "code": "role_delete",
                        "note": "刪除角色",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389562322944",
                        "type": "admin",
                        "code": "locales_create",
                        "note": "新增語系",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389570711552",
                        "type": "admin",
                        "code": "locales_update",
                        "note": "更新語系",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389587488768",
                        "type": "admin",
                        "code": "locales_delete",
                        "note": "刪除語系",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            },
            {
                "id": "5324389604265984",
                "type": "general",
                "code": "manager",
                "note": "網站管理員",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389625237504",
                        "type": "admin",
                        "code": "user_create",
                        "note": "新增使用者",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389637820416",
                        "type": "admin",
                        "code": "user_update",
                        "note": "更新使用者",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389658791936",
                        "type": "admin",
                        "code": "user_delete",
                        "note": "刪除使用者",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389671374848",
                        "type": "admin",
                        "code": "postcategory_create",
                        "note": "新增文章分類",
                        "creator_id": "0",
                        "updated_at": "2018-03-22 22:28:36",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389688152064",
                        "type": "admin",
                        "code": "postcategory_update",
                        "note": "更新文章分類",
                        "creator_id": "0",
                        "updated_at": "2018-03-22 22:28:36",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389709123584",
                        "type": "admin",
                        "code": "postcategory_delete",
                        "note": "刪除文章分類",
                        "creator_id": "0",
                        "updated_at": "2018-03-22 22:28:36",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            },
            {
                "id": "5324389721706496",
                "type": "general",
                "code": "editor",
                "note": "網站編輯",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389730095104",
                        "type": "admin",
                        "code": "blog_update",
                        "note": "更新網誌資料",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389742678016",
                        "type": "admin",
                        "code": "post_create",
                        "note": "新增文章",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389759455232",
                        "type": "admin",
                        "code": "post_update",
                        "note": "更新文章",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389772038144",
                        "type": "admin",
                        "code": "post_delete",
                        "note": "刪除文章",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            },
            {
                "id": "5324389788815360",
                "type": "general",
                "code": "author",
                "note": "網站作者",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389797203968",
                        "type": "general",
                        "code": "post_create",
                        "note": "新增文章",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389813981184",
                        "type": "general",
                        "code": "post_update",
                        "note": "更新文章",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389830758400",
                        "type": "general",
                        "code": "post_delete",
                        "note": "刪除文章",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            },
            {
                "id": "5324389847535616",
                "type": "general",
                "code": "member",
                "note": "網站會員",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389851729920",
                        "type": "general",
                        "code": "member_update",
                        "note": "更新個人資料",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389868507136",
                        "type": "general",
                        "code": "file_upload",
                        "note": "上傳檔案",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389885284352",
                        "type": "general",
                        "code": "password_change",
                        "note": "變更密碼",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389897867264",
                        "type": "general",
                        "code": "quotation_fill",
                        "note": "填寫報價單",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            }
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.roles | string[] | <p>角色s</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "type empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "type error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
## <a name='08.-查詢-所有角色-by-type-and-code'></a> 08. 查詢 所有角色 By Type And Code
[Back to top](#top)

<p>・ 查詢 所有角色 By Type And Code</p>

	GET /role/type/{type}/code/{code}/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  type | string | <p>角色類型</p>_Allowed values: general,school,company_|
|  code | string | <p>角色代號</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "role": {
            "id": "5324389482631168",
            "type": "general",
            "code": "admin",
            "note": "網站最高管理員",
            "creator_id": "0",
            "updated_at": "2018-03-15 16:37:13",
            "created_at": "2018-03-15 16:37:13",
            "permissions": [
                {
                    "id": "5324389491019776",
                    "type": "admin",
                    "code": "role_create",
                    "note": "新增角色",
                    "creator_id": "0",
                    "updated_at": "2018-03-15 16:37:13",
                    "created_at": "2018-03-15 16:37:13"
                },
                {
                    "id": "5324389507796992",
                    "type": "admin",
                    "code": "role_update",
                    "note": "更新角色",
                    "creator_id": "0",
                    "updated_at": "2018-03-15 16:37:13",
                    "created_at": "2018-03-15 16:37:13"
                },
                {
                    "id": "5324389545545728",
                    "type": "admin",
                    "code": "role_delete",
                    "note": "刪除角色",
                    "creator_id": "0",
                    "updated_at": "2018-03-15 16:37:13",
                    "created_at": "2018-03-15 16:37:13"
                },
                {
                    "id": "5324389562322944",
                    "type": "admin",
                    "code": "locales_create",
                    "note": "新增語系",
                    "creator_id": "0",
                    "updated_at": "2018-03-15 16:37:13",
                    "created_at": "2018-03-15 16:37:13"
                },
                {
                    "id": "5324389570711552",
                    "type": "admin",
                    "code": "locales_update",
                    "note": "更新語系",
                    "creator_id": "0",
                    "updated_at": "2018-03-15 16:37:13",
                    "created_at": "2018-03-15 16:37:13"
                },
                {
                    "id": "5324389587488768",
                    "type": "admin",
                    "code": "locales_delete",
                    "note": "刪除語系",
                    "creator_id": "0",
                    "updated_at": "2018-03-15 16:37:13",
                    "created_at": "2018-03-15 16:37:13"
                }
            ]
        }
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.role | string[] | <p>角色</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "type empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "code empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "type error"
}
```
## <a name='05.-查詢-所有角色'></a> 05. 查詢 所有角色
[Back to top](#top)

<p>・ 查詢 所有角色</p>

	GET /role/list/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "roles": [
            {
                "id": "5324389482631168",
                "type": "general",
                "code": "admin",
                "note": "網站最高管理員",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389491019776",
                        "type": "admin",
                        "code": "role_create",
                        "note": "新增角色",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389507796992",
                        "type": "admin",
                        "code": "role_update",
                        "note": "更新角色",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389545545728",
                        "type": "admin",
                        "code": "role_delete",
                        "note": "刪除角色",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389562322944",
                        "type": "admin",
                        "code": "locales_create",
                        "note": "新增語系",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389570711552",
                        "type": "admin",
                        "code": "locales_update",
                        "note": "更新語系",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389587488768",
                        "type": "admin",
                        "code": "locales_delete",
                        "note": "刪除語系",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            },
            {
                "id": "5324389604265984",
                "type": "general",
                "code": "manager",
                "note": "網站管理員",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389625237504",
                        "type": "admin",
                        "code": "user_create",
                        "note": "新增使用者",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389637820416",
                        "type": "admin",
                        "code": "user_update",
                        "note": "更新使用者",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389658791936",
                        "type": "admin",
                        "code": "user_delete",
                        "note": "刪除使用者",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389671374848",
                        "type": "admin",
                        "code": "postcategory_create",
                        "note": "新增文章分類",
                        "creator_id": "0",
                        "updated_at": "2018-03-22 22:28:36",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389688152064",
                        "type": "admin",
                        "code": "postcategory_update",
                        "note": "更新文章分類",
                        "creator_id": "0",
                        "updated_at": "2018-03-22 22:28:36",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389709123584",
                        "type": "admin",
                        "code": "postcategory_delete",
                        "note": "刪除文章分類",
                        "creator_id": "0",
                        "updated_at": "2018-03-22 22:28:36",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            },
            {
                "id": "5324389721706496",
                "type": "general",
                "code": "editor",
                "note": "網站編輯",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389730095104",
                        "type": "admin",
                        "code": "blog_update",
                        "note": "更新網誌資料",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389742678016",
                        "type": "admin",
                        "code": "post_create",
                        "note": "新增文章",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389759455232",
                        "type": "admin",
                        "code": "post_update",
                        "note": "更新文章",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389772038144",
                        "type": "admin",
                        "code": "post_delete",
                        "note": "刪除文章",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            },
            {
                "id": "5324389788815360",
                "type": "general",
                "code": "author",
                "note": "網站作者",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389797203968",
                        "type": "general",
                        "code": "post_create",
                        "note": "新增文章",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389813981184",
                        "type": "general",
                        "code": "post_update",
                        "note": "更新文章",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389830758400",
                        "type": "general",
                        "code": "post_delete",
                        "note": "刪除文章",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            },
            {
                "id": "5324389847535616",
                "type": "general",
                "code": "member",
                "note": "網站會員",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389851729920",
                        "type": "general",
                        "code": "member_update",
                        "note": "更新個人資料",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389868507136",
                        "type": "general",
                        "code": "file_upload",
                        "note": "上傳檔案",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389885284352",
                        "type": "general",
                        "code": "password_change",
                        "note": "變更密碼",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389897867264",
                        "type": "general",
                        "code": "quotation_fill",
                        "note": "填寫報價單",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            },
            {
                "id": "5324389914644480",
                "type": "company",
                "code": "admin",
                "note": "公司管理員",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389918838784",
                        "type": "company",
                        "code": "report_show",
                        "note": "觀看統計數據",
                        "creator_id": "0",
                        "updated_at": "2018-03-22 22:29:58",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389939810304",
                        "type": "company",
                        "code": "quotation_create",
                        "note": "新增學生報名報價單",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389952393216",
                        "type": "company",
                        "code": "quotation_show",
                        "note": "觀看每筆報名的狀態",
                        "creator_id": "0",
                        "updated_at": "2018-03-22 22:29:39",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            },
            {
                "id": "5324389964976128",
                "type": "company",
                "code": "agent",
                "note": "公司仲介",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389939810304",
                        "type": "company",
                        "code": "quotation_create",
                        "note": "新增學生報名報價單",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389952393216",
                        "type": "company",
                        "code": "quotation_show",
                        "note": "觀看每筆報名的狀態",
                        "creator_id": "0",
                        "updated_at": "2018-03-22 22:29:39",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            },
            {
                "id": "5324389994336256",
                "type": "school",
                "code": "admin",
                "note": "學校管理員",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389998530560",
                        "type": "school",
                        "code": "report_show",
                        "note": "觀看統計數據",
                        "creator_id": "0",
                        "updated_at": "2018-03-22 22:29:39",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324390011113472",
                        "type": "school",
                        "code": "quotation_verify",
                        "note": "審核學生報名報價單",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            },
            {
                "id": "5324390027890688",
                "type": "school",
                "code": "contact",
                "note": "學校經辦員",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324390011113472",
                        "type": "school",
                        "code": "quotation_verify",
                        "note": "審核學生報名報價單",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            }
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.roles | string[] | <p>角色s</p>|

### Error Response

Response: 400

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
# <a name='school'></a> School

## <a name='03.-刪除學校'></a> 03. 刪除學校
[Back to top](#top)

<p>・ 刪除學校</p>

	DELETE /school/{id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>學校 ID                                        [slug]             位址</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "delete success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_id | string | <p>學校 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "school error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "delete error"
}
```
## <a name='15.-刪除學校課程'></a> 15. 刪除學校課程
[Back to top](#top)

<p>・ 刪除學校課程</p>

	DELETE /school/course/{school_course_id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  school_course_id | string | <p>學校課程 ID                                        [slug]             位址</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "delete school course success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_course_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_course_id | string | <p>學校課程 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "school_course_id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "school course error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "delete school course error"
}
```
## <a name='20.-刪除學校宿舍'></a> 20. 刪除學校宿舍
[Back to top](#top)

<p>・ 刪除學校宿舍</p>

	DELETE /school/dorm/{school_dorm_id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  school_dorm_id | string | <p>學校宿舍 ID                                        [slug]             位址</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "delete school dorm success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_dorm_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_dorm_id | string | <p>學校宿舍 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "school_dorm_id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "school dorm error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "delete school dorm error"
}
```
## <a name='31.-刪除學校-filter'></a> 31. 刪除學校 Filter
[Back to top](#top)

<p>・ 刪除學校 Filter</p>

	DELETE /school/{id}/filter

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>學校 ID                                        [slug]             位址</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "delete success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "filter_school_ids": [
            "1522851523251513",
            "1522851523251513",
            "1522851523251513"
        ]
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.filter_school_ids | string | <p>學校 filter IDs</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "school error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "filter school error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "delete error"
}
```
## <a name='25.-刪除學校使用者'></a> 25. 刪除學校使用者
[Back to top](#top)

<p>・ 刪除學校使用者</p>

	DELETE /school/user/{school_user_id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  school_user_id | string | <p>學校使用者 ID                                        [slug]             位址</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "delete school user success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_user_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_user_id | string | <p>學校使用者 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "school_user_id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "school user error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "delete school user error"
}
```
## <a name='04.-查詢-單篇學校'></a> 04. 查詢 單篇學校
[Back to top](#top)

<p>・ 查詢 單篇學校</p>

	GET /school/{id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>學校 ID</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school": {
            "id": "12658531849342976",
            "name": "test777",
            "description": "test9999test9999test9999test9999test9999test9999test9999test9999test9999",
            "facility": "qwerqwerqwerwqerqwerwqer",
            "cover": "1522851523251513",
            "status": "enable",
            "creator_id": "11042496696160256",
            "updated_at": "2018-04-04 22:20:29",
            "created_at": "2018-04-04 22:20:29",
            "slug": "test777",
            "excerpt": "分類三",
            "og_title": "分類三",
            "og_description": "分類三分類三分類三分類三分類三分類三",
            "meta_title": "分類三",
            "meta_description": "分類三分類三分類三分類三分類三分類三",
            "cover_title": "分類三",
            "cover_alt": "分類三",
            "cover_links": {
                "o": "gallery/school_12658531849342976/cover/1522851523251513_o.jpeg"
            },
            "image_links": [
                {
                    "o": "gallery/school_12658531849342976/cover/1522851523251513_o.jpeg"
                }
            ],
            "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
            "url_path": "/test777-s12658531849342976"
        }
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school | string | <p>學校</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
## <a name='06.-查詢-單篇學校-by-slug'></a> 06. 查詢 單篇學校 By Slug
[Back to top](#top)

<p>・ 查詢 單篇學校 By Slug</p>

	GET /school/slug/{slug}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  slug | string | <p>學校 Slug</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school": {
            "id": "12658531849342976",
            "name": "test777",
            "description": "test9999test9999test9999test9999test9999test9999test9999test9999test9999",
            "facility": "qwerqwerqwerwqerqwerwqer",
            "cover": "1522851523251513",
            "status": "enable",
            "creator_id": "11042496696160256",
            "updated_at": "2018-04-04 22:20:29",
            "created_at": "2018-04-04 22:20:29",
            "slug": "test777",
            "excerpt": "分類三",
            "og_title": "分類三",
            "og_description": "分類三分類三分類三分類三分類三分類三",
            "meta_title": "分類三",
            "meta_description": "分類三分類三分類三分類三分類三分類三",
            "cover_title": "分類三",
            "cover_alt": "分類三",
            "cover_links": {
                "o": "gallery/school_12658531849342976/cover/1522851523251513_o.jpeg"
            },
            "image_links": [
                {
                    "o": "gallery/school_12658531849342976/cover/1522851523251513_o.jpeg"
                }
            ],
            "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
            "url_path": "/test777-s12658531849342976"
        }
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school | string | <p>學校</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "slug empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
## <a name='16.-查詢-單篇學校課程'></a> 16. 查詢 單篇學校課程
[Back to top](#top)

<p>・ 查詢 單篇學校課程</p>

	GET /school/course/{school_course_id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  school_course_id | string | <p>學校課程 ID</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch school course success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_course": {
            "id": "12668924801978368",
            "school_id": "12658531849342976",
            "name": "course1",
            "description": "course1course1course1course1course1course1course1course1course1course1course1",
            "ta": "parent-child",
            "updated_at": "2018-04-04 23:01:47",
            "created_at": "2018-04-04 23:01:47"
        }
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_course | string | <p>學校課程</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "school_course_id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
## <a name='17.-查詢-學校課程-by-school-id'></a> 17. 查詢 學校課程 By School Id
[Back to top](#top)

<p>・ 查詢 學校課程 By School Id</p>

	GET /school/{id}/course/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>學校 ID</p>|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch school courses success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_courses": [
            {
                "id": "12668924801978368",
                "school_id": "12658531849342976",
                "name": "course1",
                "description": "course1course1course1course1course1course1course1course1course1course1course1",
                "ta": "parent-child",
                "updated_at": "2018-04-04 23:01:47",
                "created_at": "2018-04-04 23:01:47"
            },
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_courses | string | <p>學校課程s</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
## <a name='21.-查詢-單篇學校宿舍'></a> 21. 查詢 單篇學校宿舍
[Back to top](#top)

<p>・ 查詢 單篇學校宿舍</p>

	GET /school/dorm/{school_dorm_id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  school_dorm_id | string | <p>學校宿舍 ID</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch school dorm success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_dorm": {
            "id": "12679249458761728",
            "school_id": "12658531849342976",
            "type": "dorm",
            "service": "serviceserviceserviceserviceserviceserviceservice",
            "facility": "facilityfacilityfacilityfacilityfacilityfacility",
            "updated_at": "2018-04-04 23:42:48",
            "created_at": "2018-04-04 23:42:48",
            "rooms": [
                {
                    "id": "12679249500704768",
                    "school_id": "12658531849342976",
                    "dorm_id": "12679249458761728",
                    "type": "single",
                    "accessible": "0",
                    "smoking": "0",
                    "updated_at": "2018-04-04 23:42:48",
                    "created_at": "2018-04-04 23:42:48"
                },
                {
                    "id": "12679249513287680",
                    "school_id": "12658531849342976",
                    "dorm_id": "12679249458761728",
                    "type": "twin",
                    "accessible": "0",
                    "smoking": "0",
                    "updated_at": "2018-04-04 23:42:48",
                    "created_at": "2018-04-04 23:42:48"
                }
            ]
        }
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_dorm | string | <p>學校宿舍</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "school_dorm_id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
## <a name='22.-查詢-學校宿舍-by-school-id'></a> 22. 查詢 學校宿舍 By School Id
[Back to top](#top)

<p>・ 查詢 學校宿舍 By School Id</p>

	GET /school/{id}/dorm/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>學校 ID</p>|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch school dorms success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_dorms": [
            {
            "id": "12679249458761728",
            "school_id": "12658531849342976",
            "type": "dorm",
            "service": "serviceserviceserviceserviceserviceserviceservice",
            "facility": "facilityfacilityfacilityfacilityfacilityfacility",
            "updated_at": "2018-04-04 23:42:48",
            "created_at": "2018-04-04 23:42:48",
            "rooms": [
                {
                    "id": "12679249500704768",
                    "school_id": "12658531849342976",
                    "dorm_id": "12679249458761728",
                    "type": "single",
                    "accessible": "0",
                    "smoking": "0",
                    "updated_at": "2018-04-04 23:42:48",
                    "created_at": "2018-04-04 23:42:48"
                },
                {
                    "id": "12679249513287680",
                    "school_id": "12658531849342976",
                    "dorm_id": "12679249458761728",
                    "type": "twin",
                    "accessible": "0",
                    "smoking": "0",
                    "updated_at": "2018-04-04 23:42:48",
                    "created_at": "2018-04-04 23:42:48"
                }
            ]
            },
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_dorms | string | <p>學校宿舍s</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
## <a name='12.-查詢-單篇學校費用'></a> 12. 查詢 單篇學校費用
[Back to top](#top)

<p>・ 查詢 單篇學校費用</p>

	GET /school/fee/{id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>學校 ID</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_fee": {
            "id": "12661029259579392",
            "school_id": "12658531849342976",
            "tuition": "999",
            "dorm": "10",
            "unit": "week",
            "currency": "twd",
            "updated_at": "2018-04-04 22:37:20",
            "created_at": "2018-04-04 22:30:24"
        }
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_fee | string | <p>學校費用</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "school error"
}
```
## <a name='32.-查詢-學校-filter'></a> 32. 查詢 學校 Filter
[Back to top](#top)

<p>・ 查詢 學校 Filter</p>

	GET /school/{id}/filter

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>學校 ID</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "3033560758758fb91623b61b0813475f",
        "filder_ids": [
            "13292119993225216",
            "13292120089694208",
            "13292120169385984",
            "13292120207134720",
            "13292120253272064",
            "13292120349741056"
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.filder_ids | string | <p>學校 Filter IDs</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "school error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "school filter error"
}
```
## <a name='05.-查詢-所有學校'></a> 05. 查詢 所有學校
[Back to top](#top)

<p>・ 查詢 所有學校</p>

	GET /school/list/{status?}/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | **optional**<p>學校狀態</p>_Default value: enable_<br>_Allowed values: all,enable,delete,disable_|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "schools": [
            {
                "id": "12658531849342976",
                "name": "test777",
                "description": "test9999test9999test9999test9999test9999test9999test9999test9999test9999",
                "facility": "qwerqwerqwerwqerqwerwqer",
                "cover": "1522851523251513",
                "status": "enable",
                "creator_id": "11042496696160256",
                "updated_at": "2018-04-04 22:20:29",
                "created_at": "2018-04-04 22:20:29",
                "slug": "test777",
                "excerpt": "分類三",
                "og_title": "分類三",
                "og_description": "分類三分類三分類三分類三分類三分類三",
                "meta_title": "分類三",
                "meta_description": "分類三分類三分類三分類三分類三分類三",
                "cover_title": "分類三",
                "cover_alt": "分類三",
                "cover_links": {
                    "o": "gallery/school_12658531849342976/cover/1522851523251513_o.jpeg"
                },
                "image_links": [
                    {
                        "o": "gallery/school_12658531849342976/cover/1522851523251513_o.jpeg"
                    }
                ],
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": "/test777-s12658531849342976"
            },
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.schools | string[] | <p>學校</p>|

### Error Response

Response: 400

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
## <a name='26.-查詢-單篇學校使用者'></a> 26. 查詢 單篇學校使用者
[Back to top](#top)

<p>・ 查詢 單篇學校使用者</p>

	GET /school/user/{school_user_id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  school_user_id | string | <p>學校使用者 ID</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch school user success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_user": {
            "id": "12691228839776256",
            "school_id": "12658531849342976",
            "user_id": "11042496696160256",
            "role_id": "5324389994336256",
            "type": "school",
            "creator_id": "11042496696160256",
            "updated_at": "2018-04-05 00:30:24",
            "created_at": "2018-04-05 00:30:24"
        }
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_user | string | <p>學校使用者</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "school_user_id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
## <a name='27.-查詢-學校使用者-by-school-id'></a> 27. 查詢 學校使用者 By School Id
[Back to top](#top)

<p>・ 查詢 學校使用者 By School Id</p>

	GET /school/{id}/user/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>學校 ID</p>|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch school users success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_users": [
            {
                "id": "12691228839776256",
                "school_id": "12658531849342976",
                "user_id": "11042496696160256",
                "role_id": "5324389994336256",
                "type": "school",
                "creator_id": "11042496696160256",
                "updated_at": "2018-04-05 00:30:24",
                "created_at": "2018-04-05 00:30:24"
            },
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_users | string | <p>學校使用者s</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
## <a name='28.-查詢-學校使用者-by-school-id-and-type'></a> 28. 查詢 學校使用者 By School Id And Type
[Back to top](#top)

<p>・ 查詢 學校使用者 By School Id And Type</p>

	GET /school/{id}/user/type/{type}/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>學校 ID</p>|
|  type | string | <p>使用者 type</p>_Allowed values: school,company,student_|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch school users success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_users": [
            {
                "id": "12691228839776256",
                "school_id": "12658531849342976",
                "user_id": "11042496696160256",
                "role_id": "5324389994336256",
                "type": "school",
                "creator_id": "11042496696160256",
                "updated_at": "2018-04-05 00:30:24",
                "created_at": "2018-04-05 00:30:24"
            },
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_users | string | <p>學校使用者s</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "type empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "type error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
## <a name='01.-新增學校'></a> 01. 新增學校
[Back to top](#top)

<p>・ 新增學校</p>

	POST /school

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  name | string | <p>名稱</p>|
|  description | string | <p>描述</p>|
|  facility | string | <p>設備</p>|
|  cover | string | <p>封面</p>|
|  status | string | **optional**<p>狀態</p>_Default value: enable_<br>_Allowed values: enable,delete,disable_|
|  slug | string | <p>位址</p>|
|  excerpt | string | <p>摘要</p>|
|  og_title | string | <p>og 標題</p>|
|  og_description | string | <p>og 描述</p>|
|  meta_title | string | <p>meta 標題</p>|
|  meta_description | string | <p>meta 描述</p>|
|  cover_title | string | <p>封面標題</p>|
|  cover_alt | string | <p>封面 alt</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "create success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_id | string | <p>學校 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "name empty"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "cover empty"
}
```
Response: 400.05

```
{
    "status": "fail",
    "code": 400,
    "comment": "slug empty"
}
```
Response: 400.06

```
{
    "status": "fail",
    "code": 400,
    "comment": "excerpt empty"
}
```
Response: 400.07

```
{
    "status": "fail",
    "code": 400,
    "comment": "og_title empty"
}
```
Response: 400.08

```
{
    "status": "fail",
    "code": 400,
    "comment": "og_decription empty"
}
```
Response: 400.09

```
{
    "status": "fail",
    "code": 400,
    "comment": "meta_title empty"
}
```
Response: 400.10

```
{
    "status": "fail",
    "code": 400,
    "comment": "meta_description empty"
}
```
Response: 400.11

```
{
    "status": "fail",
    "code": 400,
    "comment": "cover_title empty"
}
```
Response: 400.12

```
{
    "status": "fail",
    "code": 400,
    "comment": "cover_alt empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 409.01

```
{
    "status": "fail",
    "code": 409,
    "comment": "slug error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "cover error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "dir error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "create error"
}
```
## <a name='13.-新增學校課程'></a> 13. 新增學校課程
[Back to top](#top)

<p>・ 新增學校課程</p>

	POST /school/course

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  school_id | string | <p>學校 ID</p>|
|  name | string | <p>名稱</p>|
|  description | string | <p>描述</p>|
|  ta | string | <p>對象</p>_Allowed values: parent-child,middle,senior,bachelor,master,doctoral,adult_|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "create school course success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_course_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_course_id | string | <p>學校課程 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "school_id empty"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "name empty"
}
```
Response: 400.05

```
{
    "status": "fail",
    "code": 400,
    "comment": "ta empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "ta error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "school error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "create school course error"
}
```
## <a name='18.-新增學校宿舍'></a> 18. 新增學校宿舍
[Back to top](#top)

<p>・ 新增學校宿舍</p>

	POST /school/course

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  school_id | string | <p>學校 ID</p>|
|  type | string | <p>宿舍類型</p>_Allowed values: dorm,hotel,homestay_|
|  service | string | <p>服務</p>|
|  facility | string | <p>設施</p>|
|  rooms | string | <p>房型</p>_Allowed values: []_|
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; rooms.N.type | string | <p>房型類型</p>_Allowed values: 'single','double','triple','quad','twin','double-double','studio','suit','apartment'_|
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; rooms.N.accessible | string | **optional**<p>無障礙設施</p>_Default value: true_<br>_Allowed values: true,false_|
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; rooms.N.smoking | string | **optional**<p>吸菸</p>_Default value: true_<br>_Allowed values: true,false_|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "create school dorm success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_dorm_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_dorm_id | string | <p>學校宿舍 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "school_id empty"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "service empty"
}
```
Response: 400.05

```
{
    "status": "fail",
    "code": 400,
    "comment": "facility empty"
}
```
Response: 400.06

```
{
    "status": "fail",
    "code": 400,
    "comment": "rooms empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "type error"
}
```
Response: 422.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "all room type error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "school error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "school dorm error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "create school dorm error"
}
```
## <a name='10.-新增學校費用'></a> 10. 新增學校費用
[Back to top](#top)

<p>・ 新增學校費用</p>

	POST /school/fee

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  school_id | string | <p>學校 ID</p>|
|  tuition | string | **optional**<p>學費</p>_Default value: 0_<br>|
|  dorm | string | **optional**<p>住宿</p>_Default value: 0_<br>|
|  unit | string | **optional**<p>期間</p>_Default value: week_<br>_Allowed values: week,month,year,acad-term_|
|  currency | string | **optional**<p>幣別</p>_Default value: usd_<br>_Allowed values: usd,gbp,eur,aud,twd,cny_|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "create success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_id | string | <p>學校 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "school_id empty"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "tuition empty"
}
```
Response: 400.05

```
{
    "status": "fail",
    "code": 400,
    "comment": "dorm empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "unit error"
}
```
Response: 422.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "currency error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "school error"
}
```
Response: 409.01

```
{
    "status": "fail",
    "code": 409,
    "comment": "school fee error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "create fee error"
}
```
## <a name='29.-新增學校-filter'></a> 29. 新增學校 Filter
[Back to top](#top)

<p>・ 新增學校 Filter</p>

	POST /school/filter

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  school_id | string | <p>學校 ID</p>|
|  filter_ids | string[] | <p>filter Ids</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "create success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "filter_school_ids": [
            "1522851523251513",
            "1522851523251513",
            "1522851523251513"
        ]
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.filter_school_ids | string | <p>學校 filter IDs</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "school_id empty"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "filter_ids empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "school error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "filter error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "create error"
}
```
## <a name='23.-新增學校使用者'></a> 23. 新增學校使用者
[Back to top](#top)

<p>・ 新增學校使用者</p>

	POST /school/user

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  school_id | string | <p>學校 ID</p>|
|  user_id | string | <p>使用者 ID</p>|
|  role_id | string | <p>角色 ID</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "create school user success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_user_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_user_id | string | <p>學校使用者 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "school_id empty"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "user_id empty"
}
```
Response: 400.05

```
{
    "status": "fail",
    "code": 400,
    "comment": "role_id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "school error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
```
Response: 404.03

```
{
    "status": "fail",
    "code": 404,
    "comment": "role error"
}
```
Response: 409.01

```
{
    "status": "fail",
    "code": 409,
    "comment": "school user error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "create school user error"
}
```
## <a name='33.-搜尋學校-filter'></a> 33. 搜尋學校 Filter
[Back to top](#top)

<p>・ 搜尋學校 Filter</p>

	POST /school/filter/search

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  filter_ids | string[] | <p>filter Ids</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "search success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "schools": [
{
                "id": "12658531849342976",
                "name": "test777",
                "description": "test9999test9999test9999test9999test9999test9999test9999test9999test9999",
                "facility": "qwerqwerqwerwqerqwerwqer",
                "cover": "1522851523251513",
                "status": "enable",
                "creator_id": "11042496696160256",
                "updated_at": "2018-04-04 22:20:29",
                "created_at": "2018-04-04 22:20:29",
                "slug": "test777",
                "excerpt": "分類三",
                "og_title": "分類三",
                "og_description": "分類三分類三分類三分類三分類三分類三",
                "meta_title": "分類三",
                "meta_description": "分類三分類三分類三分類三分類三分類三",
                "cover_title": "分類三",
                "cover_alt": "分類三"
            },
        ]
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.schools | string | <p>學校s</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "filter_ids empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "filter error"
}
```
## <a name='07.-上傳封面檔案'></a> 07. 上傳封面檔案
[Back to top](#top)

<p>・ 上傳封面檔案</p>

	POST /school/cover

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  owner_id | string | **optional**<p>擁有者 ID</p>_Default value: 0_<br>|
|  privacy_status | string | **optional**<p>隱私狀態</p>_Default value: public_<br>_Allowed values: public,private_|
|  status | string | **optional**<p>狀態</p>_Default value: disable_<br>_Allowed values: enable,disable,delete_|
|  file | file | <p>檔案</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "upload cover success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "filename": "1517067835270712",
        "cover_links": {
            "c": "gallery/1517067835270712_c.jpeg",
            "z": "gallery/1517067835270712_z.jpeg",
            "n": "gallery/1517067835270712_n.jpeg",
            "q": "gallery/1517067835270712_q.jpeg",
            "sq": "gallery/1517067835270712_sq.jpeg",
            "o": "gallery/1517067835270712_o.jpeg"
        }
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.filename | string | <p>檔案名稱</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.cover_links | string[] | <p>各尺寸圖檔連結</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "file empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "privacy_status error"
}
```
Response: 422.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 422.04

```
{
    "status": "fail",
    "code": 422,
    "comment": "file error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "create error"
}
```
## <a name='09.-上傳圖片檔案'></a> 09. 上傳圖片檔案
[Back to top](#top)

<p>・ 上傳圖片檔案</p>

	POST /school/{id}/image

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  privacy_status | string | **optional**<p>隱私狀態</p>_Default value: public_<br>_Allowed values: public,private_|
|  status | string | **optional**<p>狀態</p>_Default value: disable_<br>_Allowed values: enable,disable,delete_|
|  file | file | <p>檔案</p>|
|  _method | string | <p>傳輸方法</p>_Allowed values: PUT_|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "upload cover success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "filename": "1517067835270712",
        "image_links": {
            "c": "gallery/1517067835270712_c.jpeg",
            "z": "gallery/1517067835270712_z.jpeg",
            "n": "gallery/1517067835270712_n.jpeg",
            "q": "gallery/1517067835270712_q.jpeg",
            "sq": "gallery/1517067835270712_sq.jpeg",
            "o": "gallery/1517067835270712_o.jpeg"
        }
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.filename | string | <p>檔案名稱</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.image_links | string[] | <p>各尺寸圖檔連結</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "file empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "privacy_status error"
}
```
Response: 422.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 422.04

```
{
    "status": "fail",
    "code": 422,
    "comment": "file error"
}
```
Response: 404.04

```
{
    "status": "fail",
    "code": 404,
    "comment": "owner error"
}
```
Response: 422.04

```
{
    "status": "fail",
    "code": 422,
    "comment": "owner status error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "create error"
}
```
## <a name='02.-修改學校'></a> 02. 修改學校
[Back to top](#top)

<p>・ 修改學校</p>

	PUT /school/{id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>學校 ID</p>|
|  name | string | **optional**<p>名稱</p>|
|  description | string | **optional**<p>描述</p>|
|  facility | string | **optional**<p>設備</p>|
|  cover | string | **optional**<p>封面</p>|
|  status | string | **optional**<p>狀態</p>_Allowed values: enable,delete,disable_|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "update success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_id | string | <p>學校 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "school error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "cover error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "updateData empty"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "update error"
}
```
## <a name='14.-修改學校課程'></a> 14. 修改學校課程
[Back to top](#top)

<p>・ 修改學校課程</p>

	PUT /school/course/{school_course_id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  school_course_id | string | <p>學校課程 ID</p>|
|  name | string | **optional**<p>名稱</p>|
|  description | string | **optional**<p>描述</p>|
|  ta | string | **optional**<p>對象</p>_Allowed values: parent-child,middle,senior,bachelor,master,doctoral,adult_|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "update school course success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_course_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_course_id | string | <p>學校課程 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "school_course_id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "school course error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "ta error"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "updateData empty"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "update school course error"
}
```
## <a name='19.-修改學校宿舍'></a> 19. 修改學校宿舍
[Back to top](#top)

<p>・ 修改學校宿舍</p>

	PUT /school/dorm/{school_dorm_id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  school_dorm_id | string | <p>學校宿舍 ID</p>|
|  type | string | **optional**<p>宿舍類型</p>_Allowed values: dorm,hotel,homestay_|
|  service | string | **optional**<p>服務</p>|
|  facility | string | **optional**<p>設施</p>|
|  rooms | string | **optional**<p>房型</p>_Allowed values: []_|
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; rooms.N.type | string | <p>房型類型</p>_Allowed values: 'single','double','triple','quad','twin','double-double','studio','suit','apartment'_|
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; rooms.N.accessible | string | <p>無障礙設施</p>_Allowed values: true,false_|
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; rooms.N.smoking | string | <p>吸菸</p>_Allowed values: true,false_|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "update school dorm success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_dorm_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_dorm_id | string | <p>學校宿舍 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "school_dorm_id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "school dorm error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "type error"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "updateData / updateRoomData empty"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "update school dorm error"
}
```
## <a name='11.-修改學校費用'></a> 11. 修改學校費用
[Back to top](#top)

<p>・ 修改學校費用</p>

	PUT /school/{id}/fee

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>學校 ID</p>|
|  tuition | string | **optional**<p>學費</p>|
|  dorm | string | **optional**<p>住宿</p>|
|  unit | string | **optional**<p>期間</p>_Allowed values: week,month,year,acad-term_|
|  currency | string | **optional**<p>幣別</p>_Allowed values: usd,gbp,eur,aud,twd,cny_|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "update fee success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_id | string | <p>學校 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "school error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "school fee error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "unit error"
}
```
Response: 422.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "currency error"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "updateData empty"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "update fee error"
}
```
## <a name='30.-修改學校-filter'></a> 30. 修改學校 Filter
[Back to top](#top)

<p>・ 修改學校 Filter</p>

	PUT /school/{id}/filter

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>學校 ID</p>|
|  filter_ids | string[] | <p>filter ID</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "update success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "filter_school_ids": [
            "1522851523251513",
            "1522851523251513",
            "1522851523251513"
        ]
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.filter_school_ids | string | <p>學校 filter IDs</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "filter_ids empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "school error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "filter error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "update error"
}
```
## <a name='08.-修改學校-seo'></a> 08. 修改學校 SEO
[Back to top](#top)

<p>・ 修改學校 SEO</p>

	PUT /school/{id}/seo

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>學校 ID</p>|
|  slug | string | **optional**<p>位址</p>|
|  excerpt | string | **optional**<p>摘要</p>|
|  canonical_url | string | **optional**<p>來源位址</p>|
|  og_title | string | **optional**<p>og 標題</p>|
|  og_description | string | **optional**<p>og 描述</p>|
|  meta_title | string | **optional**<p>meta 標題</p>|
|  meta_description | string | **optional**<p>meta 描述</p>|
|  cover_title | string | **optional**<p>封面標題</p>|
|  cover_alt | string | **optional**<p>封面 alt</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "update seo success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_id | string | <p>學校 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "school error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "school seo error"
}
```
Response: 409.01

```
{
    "status": "fail",
    "code": 409,
    "comment": "slug error"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "updateData empty"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "update error"
}
```
## <a name='24.-修改學校使用者'></a> 24. 修改學校使用者
[Back to top](#top)

<p>・ 修改學校使用者</p>

	PUT /school/user/{school_user_id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  school_user_id | string | <p>學校使用者 ID</p>|
|  user_id | string | **optional**<p>使用者 ID</p>|
|  role_id | string | **optional**<p>角色 ID</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "update school user success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_user_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.school_user_id | string | <p>學校使用者  ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "school_user_id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "school user error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
```
Response: 404.03

```
{
    "status": "fail",
    "code": 404,
    "comment": "role error"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "updateData empty"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "update school user error"
}
```
# <a name='search'></a> Search

## <a name='01.-搜尋'></a> 01. 搜尋
[Back to top](#top)

<p>・ 搜尋 文章 title/slug 及 作者 nickname/slug</p>

	GET /search/{term}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  term | string | <p>搜尋字串 (請先用 urlencode)</p>|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 204

```
{}
```
Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "posts": [],
        "users": [
            {
                "id": "8220321387778048",
                "title": "【英國留遊學】倫敦大學金匠學院",
                "content": "<p>倫敦大學在台灣應該是個耳熟能詳的英國大學，畢竟民進黨的黨主席蔡英文女士就是畢業於倫敦大學的其中一個學院 &ndash; 倫敦政治經濟學院 （London School of Economics and Political Science），但是除了這個幾乎家喻戶曉的學院之外，其實聯邦制體系的倫敦大學還有其他專攻不同領域的院校，而我今天要介紹的就是專攻藝文與傳媒的金匠學院(Goldsmiths)。</p>\n<p>如果你喜歡英國當代藝術，或是你有些許研究的話，你應該會有聽過The Young British Artists（簡稱YBAs）這個名稱，他們是一群崛起於80年代的藝術家們，並且在90年代在藝術界造成了一陣轟動，也深深影響了現在的當代藝術。在這群藝術家中有多位都是畢業於金匠學院的藝術科系，例如，在2007年推出全世界最貴的藝術作品 For the love of God的Damien Hirst跟以諷刺手法來表現兩性觀點的Sarah Lucas，他們都是金匠學院的校友。如果你喜歡時尚的話，你一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，你不知道的是，Vivian Westwood也是畢業於金匠學院的設計科系。另外，金匠學院的大眾傳播學系(Media and Communication)也是國際知名的學校，像韓國的導演也都曾來此就讀。可見金匠學院在藝文界真的是有舉足輕重的地位，也培育出了許多赫赫有名的人才。</p>\n<p>在當年就是衝著這股濃濃藝文味，於是我選擇到了金匠學院，主修大眾傳播媒體，在就讀的期間也感受到了學校的優點與缺點，在這裡就跟可能有計畫來就讀的你分享一下身為校友的個人的經驗吧！</p>\n<p>首先，有一件事你一定要先知道，在英國，大學的<strong>就讀時間大部分為三年</strong>，也就是讀完三年沒被當的話就能夠順利畢業，另外學校採用的是<strong>三學期制</strong>，跟在台灣熟悉的二學期制不太一樣，雖然這樣假期似乎看起來會比較多，但是其實課業量反而更繁重，因為相同的課程就變成要在較短的時間內學會，而且每一次的假期也就代表著一次的報告dead line，所以，想玩就必須好好做時間規劃，把時間規劃好，才能夠順利繳交報告跟同時享有假期啊！</p>\n<p>另外，我覺得倫敦大學的學校，相對於倫敦藝術大學，會顯得比較著重於理論，因為我有認識的朋友是就讀於倫敦藝術大學，他們時常都是忙於繳交作品，個人覺得是比較偏向實作方面，但是在金匠學院他們是採取<strong>理論與實作並重</strong>的方式來教課，也就是說作品量的累積跟實作的經驗比起倫敦藝術大學的同學可能會有些落差，但是在理論上知識的基礎可能會更加深厚。會這樣著重理論，是因為他們認為在知識上有著較深厚的基礎就能夠培養出批判性的思考能力(critical thinking)，創造出來的作品會更有內涵並能利用學到的知識來引導觀者，藉以得到更大的共鳴。</p>\n<p>在大眾傳播的實作課程中，你可以從八種不同的課程來做選擇，分別為：</p>\n<p>Journalism</p>\n<p>Photography</p>\n<p>Filmmaking</p>\n<p>Illustration</p>\n<p>Script and Props</p>\n<p>Animation</p>\n<p>Radio</p>\n<p>Interactive Media</p>\n<p>在一年級的時候你可以選修四種實作課程，但是到了二年級只能選修兩種，到了三年級就必須專精於其中一項，應該是希望透過嘗試讓學生找到自己有興趣或是有潛能的課程，再加以訓練跟培養。</p>\n<p>我一年級時嘗試了animation，photography，filmmaking跟illustration，二年級的時候選擇了動畫跟攝影，到三年級就專精於攝影。其中動畫課在一二年級的時候，我們學習到的是stop-motion的製作方式，還有如何製作story board，但是到了三年級會學習到使用After Effect這個軟體來製作動畫。filmmaking其實很有趣，你會跟著一個團隊一起合作，可以嘗試到像採訪陌生人，或是自己尋找演員、找場地拍攝，甚至是製作配樂等經驗。在攝影課我們使用的幾乎都是底片相機，學校都有設備可以出借，但是有規定的出借跟歸還時間。在課堂上你會學到關於攝影的理論跟實際操作器具的體驗，不只是相機的操作，還有燈光的調整，底片的沖洗等等。基本上每學年你都需要製作一個自己的project，主題很自由，老師可能會給你一個大方向，但是你可以自己決定主題去製作。</p>\n<p>不過，專精於其中一項不代表你只能侷限自己做某一個課程，相反的，學校反而鼓勵學生互相合作、學習，甚至跟不同科系的學生也都能夠互相合作來籌畫活動，推行企劃。但是，這相互間的合作通常是要靠個人去籌劃跟聯繫，學校並不會當中間人或指導人，<strong>主動性的培養</strong>，我覺得這是臺灣跟國外學校的一個最主要的差別吧！老師們，可能會提出你可以籌辦活動的想法，但是做與不做就全權交給你去決定，如果要做了，你有問題都可以主動提出，他們也都會很樂意幫助你，但比較不會主動去督促你的進度。像上必修課也都是一樣的，基本上都是lecture接著上seminar，書籍都是一開學就發放給你，在上課前你必須自己先研讀，到學校講師在講課，講課的內容會以書籍裡的內容為基礎，然後延伸出其他更深更廣的理論，有問題再在seminar上提出討論，老師並不會帶著你唸課文，所以如果你事前預習沒做好，在講師講課的時候可能就已經被搞得一頭霧水了。</p>\n<p>如果你在大學裡期待的是非常多的實作經驗，還有學校幫你選擇好的實習課程，那金匠大學可能不是屬於你的選擇，但是如果你想把底子學好跟喜歡挑戰非商業性的創作，靠自己去尋找實習機會，金匠大學就會是屬於你的不二選擇喔～</p>",
                "cover": "1519267601292148",
                "privacy_status": "public",
                "status": "publish",
                "recommended": "0",
                "blog_id": "8217810404773888",
                "postcategory_id": "7859246410633216",
                "owner_id": "8217810346053632",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:24:37",
                "created_at": "2018-03-23 16:24:37",
                "slug": "英國留遊學-倫敦大學金匠學院",
                "excerpt": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
                "og_title": "【英國留遊學】倫敦大學金匠學院",
                "og_description": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
                "meta_title": "【英國留遊學】倫敦大學金匠學院",
                "meta_description": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
                "cover_title": "倫敦大學金匠學院",
                "cover_alt": "倫敦大學金匠學院",
                "postcategory": {
                    "id": "7859246410633216",
                    "type": "global",
                    "name": "英國留學專欄",
                    "cover": "1519872393361401",
                    "status": "enable",
                    "num_items": "0",
                    "owner_id": "0",
                    "creator_id": "0",
                    "updated_at": "2018-03-22 16:29:50",
                    "created_at": "2018-03-22 16:29:50",
                    "slug": "study-aboard-UK",
                    "excerpt": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                    "og_title": "《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！",
                    "og_description": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                    "meta_title": "《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！",
                    "meta_description": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                    "cover_title": "出國留學",
                    "cover_alt": "出國留學"
                },
                "owner": {
                    "id": "8217810346053632",
                    "account": "david@english.agency",
                    "password": "$2y$10$5zcXhEAiHzYwfy5Vs7m5tObjcu3cgbvciFA5Myz4cmWgpP.ISSPWW",
                    "status": "enable",
                    "token": "070c01d7-a9c5-5614-b9ac-8f14b53ee5a7",
                    "creator_id": "0",
                    "updated_at": "2018-03-23 16:14:38",
                    "created_at": "2018-03-23 16:14:38",
                    "slug": "david_b",
                    "excerpt": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                    "og_title": "\"David B\"",
                    "og_description": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                    "meta_title": "\"David B\"",
                    "meta_description": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                    "avatar_title": "\"David B\"",
                    "avatar_alt": "\"David B\""
                },
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": "/英國留遊學-倫敦大學金匠學院-p8220321387778048"
            }
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.posts | string[] | <p>文章</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.users | string[] | <p>作者有關文章</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "term empty"
}
```
Response: 410

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
# <a name='session'></a> Session

## <a name='02.-用-code-查詢-session'></a> 02. 用 code 查詢 session
[Back to top](#top)

<p>・ 用 code 查詢 session。</p>

	GET /session/{code}





### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  code | string | <p>Session 代碼</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "status": "login"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.status | string | <p>Session 狀態</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "code empty"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "session error"
}
```
## <a name='01.-初始化'></a> 01. 初始化
[Back to top](#top)

<p>・ 用 device_code, device_os, device_type, device_lang, device_token, device_channel 初始化 session<br /> ・ 若不傳 device_code 或為空值，則自行產生<br /> ・ 若不傳 device_channel 或為空值，則依 device_os 產生相對應的值</p>

	POST /session/init





### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  device_code | string | **optional**<p>裝置代碼</p>|
|  device_os | string | **optional**<p>裝置作業系統</p>_Default value: others_<br>_Allowed values: ios,andriod,macos,windows,others_|
|  device_type | string | **optional**<p>裝置型態</p>_Default value: others_<br>_Allowed values: iphone,ipad,mobile,pc,others_|
|  device_lang | string | **optional**<p>裝置語言</p>_Default value: en_<br>_Allowed values: zh-tw,zh-cn,jp,en_|
|  device_token | string | **optional**<p>裝置 token</p>|
|  device_channel | string | **optional**<p>裝置 訊息頻道</p>_Allowed values: apns,gcm,baidu,others_|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "init success",
    "data": {
        "device_code": "aeb0c92d-c9fa-47ea-814e-8e009c1d3056",
        "session": "8f3e973061800dc6ebcb367079b305d8"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.device_code | string | <p>裝置代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|

### Error Response

Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "device os error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "device type error"
}
```
Response: 422.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "device lang error"
}
```
Response: 422.04

```
{
    "status": "fail",
    "code": 422,
    "comment": "device channel error"
}
```
# <a name='subscribe'></a> Subscribe

## <a name='02.-查詢-所有訂閱者'></a> 02. 查詢 所有訂閱者
[Back to top](#top)

<p>・ 查詢 所有訂閱者。</p>

	GET /subscribe/subscribers

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "subscribers": [
            {
                "id": "1",
                "name": "John",
                "email": "jasechen@gmail.com",
                "status": "subscribe",
                "updated_at": "2018-01-22 11:28:55",
                "created_at": "2018-01-22 11:28:55"
            },
            {
                "id": "2",
                "name": "John",
                "email": "jase.chen@gmail.com",
                "status": "subscribe",
                "updated_at": "2018-01-22 11:28:55",
                "created_at": "2018-01-22 11:28:55"
            },
            ...
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.subscribers | string[] | <p>訂閱者</p>|

### Error Response

Response: 400

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 410

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
## <a name='01.-訂閱-edm'></a> 01. 訂閱 EDM
[Back to top](#top)

<p>・ 用 email 訂閱 EDM</p>

	POST /subscribe

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | **optional**<p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  name | string | <p>姓名</p>|
|  email | string | <p>E-Mail</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "create success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "subscriber_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.subscriber_id | string | <p>訂閱 EDM ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "name empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "email empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "email format error"
}
```
Response: 409.01

```
{
    "status": "fail",
    "code": 409,
    "comment": "email already exist"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "create error"
}
```
# <a name='user'></a> User

## <a name='03.-刪除使用者'></a> 03. 刪除使用者
[Back to top](#top)

<p>・ 刪除使用者</p>

	DELETE /user/{id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>使用者 ID                                        [slug]             位址</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "delete success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "user_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.user_id | string | <p>使用者 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "delete error"
}
```
## <a name='04.-查詢-單一使用者'></a> 04. 查詢 單一使用者
[Back to top](#top)

<p>・ 查詢 單一使用者</p>

	GET /user/{id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>使用者 ID</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "user": {
            "id": "8217809167454208",
            "account": "admin@english.agency",
            "password": "$2y$10$yEX7E/.ueop/dbbgBP9qy.4vdobMHcR6diBG.pQKrOBiT61.cADaG",
            "status": "enable",
            "token": "cd315b42-4108-52f2-92ce-86a76007dfe7",
            "creator_id": "0",
            "updated_at": "2018-03-23 16:14:38",
            "created_at": "2018-03-23 16:14:38",
            "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
            "url_path": ""
        }
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.user | string | <p>使用者</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
## <a name='06.-查詢-單一使用者-by-slug'></a> 06. 查詢 單一使用者 By Slug
[Back to top](#top)

<p>・ 查詢 單一使用者 By Slug</p>

	GET /user/slug/{slug}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  slug | string | <p>使用者 Slug</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "user": {
            "id": "8217809167454208",
            "account": "admin@english.agency",
            "password": "$2y$10$yEX7E/.ueop/dbbgBP9qy.4vdobMHcR6diBG.pQKrOBiT61.cADaG",
            "status": "enable",
            "token": "cd315b42-4108-52f2-92ce-86a76007dfe7",
            "creator_id": "0",
            "updated_at": "2018-03-23 16:14:38",
            "created_at": "2018-03-23 16:14:38",
            "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
            "url_path": ""
        }
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.user | string | <p>使用者</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "slug empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
## <a name='07.-查詢-單一使用者-by-token'></a> 07. 查詢 單一使用者 By Token
[Back to top](#top)

<p>・ 查詢 單一使用者 By Token</p>

	GET /user/token/{token}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  token | string | <p>使用者 Token</p>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "user": {
            "id": "8217809167454208",
            "account": "admin@english.agency",
            "password": "$2y$10$yEX7E/.ueop/dbbgBP9qy.4vdobMHcR6diBG.pQKrOBiT61.cADaG",
            "status": "enable",
            "token": "cd315b42-4108-52f2-92ce-86a76007dfe7",
            "creator_id": "0",
            "updated_at": "2018-03-23 16:14:38",
            "created_at": "2018-03-23 16:14:38",
            "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
            "url_path": ""
        }
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.user | string | <p>使用者</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
## <a name='05.-查詢-所有使用者'></a> 05. 查詢 所有使用者
[Back to top](#top)

<p>・ 查詢 所有使用者</p>

	GET /user/list/{status?}/{order_way?}/{page?}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | **optional**<p>使用者狀態</p>_Default value: enable_<br>_Allowed values: all,enable,delete,disable_|
|  order_way | string | **optional**<p>排列升降冪</p>_Default value: ASC_<br>_Allowed values: ASC,DESC_|
|  page | string | **optional**<p>頁數</p>_Default value: -1_<br>|

### Success Response

Response: 200

```
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "users": [
            {
                "id": "8217809167454208",
                "account": "admin@english.agency",
                "password": "$2y$10$yEX7E/.ueop/dbbgBP9qy.4vdobMHcR6diBG.pQKrOBiT61.cADaG",
                "status": "enable",
                "token": "cd315b42-4108-52f2-92ce-86a76007dfe7",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:38",
                "created_at": "2018-03-23 16:14:38",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217809599467520",
                "account": "alexs@english.agency",
                "password": "$2y$10$dTEwxbx0Q5pvkJhjyYndCehQRHk0epqGD/yWMUCcDjmm7HODAJD/q",
                "status": "enable",
                "token": "fcf7727b-1ea5-5549-a8cf-40c6a852c30d",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:38",
                "created_at": "2018-03-23 16:14:38",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217809985343488",
                "account": "magic@english.agency",
                "password": "$2y$10$MRLFkU3Px7ckRBTxwj2DM.xcL70c.PwP2lqZGeYDKFAxL49is7Lzi",
                "status": "enable",
                "token": "5b01417c-522b-5fa4-bf80-1e86c2201832",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:38",
                "created_at": "2018-03-23 16:14:38",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217810346053632",
                "account": "david@english.agency",
                "password": "$2y$10$5zcXhEAiHzYwfy5Vs7m5tObjcu3cgbvciFA5Myz4cmWgpP.ISSPWW",
                "status": "enable",
                "token": "070c01d7-a9c5-5614-b9ac-8f14b53ee5a7",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:38",
                "created_at": "2018-03-23 16:14:38",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217810706763776",
                "account": "vacca@english.agency",
                "password": "$2y$10$GH30Glfuxz0lrQiSemmKjuXHL/ay7TbO/o84bvbwPcdVv7JGdfVH.",
                "status": "enable",
                "token": "22874266-9ad8-5bc3-ae3d-6b20996ac576",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:38",
                "created_at": "2018-03-23 16:14:38",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217811075862528",
                "account": "bacio@english.agency",
                "password": "$2y$10$3oV.jiNtXHnqgds39kaxOeiTNARl0A0Z2mfeEzQeP51krjZBRLTjC",
                "status": "enable",
                "token": "fdfe7acc-8097-519f-a8fd-60c49b5bc189",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:38",
                "created_at": "2018-03-23 16:14:38",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217811432378368",
                "account": "gemora@english.agency",
                "password": "$2y$10$JJrQtFPbZAh.VssdUkchQeaRgWMA62HsTdzg.7eU2VQEPDiUM0yjC",
                "status": "enable",
                "token": "68308d20-f309-5d9f-baa3-528389747854",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:38",
                "created_at": "2018-03-23 16:14:38",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217811793088512",
                "account": "rabo@english.agency",
                "password": "$2y$10$MLqwbWJYndct44a2Ti6Q4eRuwY97B84MG2qtXoDHzwAYvh9t9lG.O",
                "status": "enable",
                "token": "295a5e7a-0e14-5ebd-b8f3-7e84300848ce",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:39",
                "created_at": "2018-03-23 16:14:39",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217812157992960",
                "account": "globaljobs@english.agency",
                "password": "$2y$10$KFhXj9W1nIJWOnkgadlhg.wtUxma0yutBlqFo0RPEtTu7cuAX590q",
                "status": "enable",
                "token": "e5405c9b-8a0e-5332-94de-0208cd92a3e7",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:39",
                "created_at": "2018-03-23 16:14:39",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217812506120192",
                "account": "toeic@english.agency",
                "password": "$2y$10$QuLXGH33wnx6S6s7PzSnw.buhFbDDhAbGq3pZ6K6M0.qD2osH52GS",
                "status": "enable",
                "token": "1a661b2d-db14-5540-ae65-bff88e20e654",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:39",
                "created_at": "2018-03-23 16:14:39",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217812858441728",
                "account": "autumn@english.agency",
                "password": "$2y$10$XIw3Hm9wA.QAUWTpxH1GlOSEUrZcbvRcQ8OObU03YOEGlUn572sQ2",
                "status": "enable",
                "token": "05a12268-f836-502a-901b-fc5f06c2e566",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:39",
                "created_at": "2018-03-23 16:14:39",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217813202374656",
                "account": "alya@english.agency",
                "password": "$2y$10$Fg84g9DsonIJCXpdLHdkC.qddD2jEj/UUzOgLe5kK3Nskx86/BFfC",
                "status": "enable",
                "token": "e2c63cf7-6ddb-5e84-bb32-31a31278cf4b",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:39",
                "created_at": "2018-03-23 16:14:39",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217813571473408",
                "account": "toefl@english.agency",
                "password": "$2y$10$IEB8MrgmPaScblLyxzbLgeCnNOaEMKUBolM32hskDRgJLH91US73a",
                "status": "enable",
                "token": "1212d5ff-993e-5754-a609-0c020f98359c",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:39",
                "created_at": "2018-03-23 16:14:39",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217813932183552",
                "account": "luna@english.agency",
                "password": "$2y$10$S8VRZ0odLFSyQYBKbRjwLO8LTJgEbXMk1LsyBNdNsuODoMX4dBLU6",
                "status": "enable",
                "token": "14bcc8af-eb01-5e81-b0e1-3d3bab4cea2f",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:39",
                "created_at": "2018-03-23 16:14:39",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217814297088000",
                "account": "jasmine@english.agency",
                "password": "$2y$10$4XQn4RQ2N51qzPJ5V.ml1Om9TocKVx9/K/CYyRc4KY8y36xzgjNzO",
                "status": "enable",
                "token": "da6c3c3c-d8ae-56de-9e9f-d0feeb42571a",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:39",
                "created_at": "2018-03-23 16:14:39",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217814678769664",
                "account": "claire@english.agency",
                "password": "$2y$10$b4RqTLd.CA4/0r1LPt5vZOWJ.j61tnh2v5aCi9ZOZNp5K60J2dK2W",
                "status": "enable",
                "token": "45713cc3-a826-5b4b-99f7-042f17c7e817",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:39",
                "created_at": "2018-03-23 16:14:39",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217815043674112",
                "account": "winnie@english.agency",
                "password": "$2y$10$u4AjL6tK4dcwmRjtQHtNCO0XTxESMQXss2UiFuusyVCbwe12NTAna",
                "status": "enable",
                "token": "4d5bff58-b363-5010-88f1-9303e4a6fa40",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:39",
                "created_at": "2018-03-23 16:14:39",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            }
        ]
    }
}
```
Response: 204

```
{}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.users | string[] | <p>使用者s</p>|

### Error Response

Response: 400

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
```
## <a name='13.-重寄-e-mail-驗證信'></a> 13. 重寄 E-Mail 驗證信
[Back to top](#top)

<p>・ 重寄 E-Mail 驗證信</p>

	GET /user/{id}/email/notify

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>使用者 ID</p>|

### Success Response

Response: 202

```
{
    "status": "success",
    "code": 202,
    "comment": "notify success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "user status error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "user email check error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "user check status error"
}
```
## <a name='15.-重寄-mobile-驗證簡訊'></a> 15. 重寄 Mobile 驗證簡訊
[Back to top](#top)

<p>・ 重寄 Mobile 驗證簡訊</p>

	GET /user/{id}/mobile/notify

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>使用者 ID</p>|

### Success Response

Response: 202

```
{
    "status": "success",
    "code": 202,
    "comment": "notify success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "user status error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "user mobile check error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "user check status error"
}
```
## <a name='01.-新增使用者'></a> 01. 新增使用者
[Back to top](#top)

<p>・ 新增使用者</p>

	POST /user

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  account | string | <p>使用者帳號</p>|
|  password | string | <p>密碼</p>|
|  password_repeat | string | <p>確認密碼</p>|
|  status | string | **optional**<p>狀態</p>_Default value: init_<br>_Allowed values: init,enable,disable,delete_|
|  first_name | string | <p>名</p>|
|  last_name | string | <p>姓</p>|
|  mobile_country_code | string | <p>手機號碼國碼</p>|
|  mobile_phone | string | <p>手機號碼</p>|
|  slug | string | <p>位址</p>|
|  excerpt | string | <p>摘要</p>|
|  og_title | string | <p>og 標題</p>|
|  og_description | string | <p>og 描述</p>|
|  meta_title | string | <p>meta 標題</p>|
|  meta_description | string | <p>meta 描述</p>|
|  avatar_title | string | <p>頭像標題</p>|
|  avatar_alt | string | <p>頭像 alt</p>|
|  role_ids | string[] | **optional**<p>角色 IDs (default:type=general,code=mamber)</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "create success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "user_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.user_id | string | <p>使用者 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "account empty"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "password / password_repeat empty"
}
```
Response: 400.05

```
{
    "status": "fail",
    "code": 400,
    "comment": "first_name empty"
}
```
Response: 400.06

```
{
    "status": "fail",
    "code": 400,
    "comment": "last_name empty"
}
```
Response: 400.07

```
{
    "status": "fail",
    "code": 400,
    "comment": "mobile_country_code empty"
}
```
Response: 400.08

```
{
    "status": "fail",
    "code": 400,
    "comment": "mobile_phone empty"
}
```
Response: 400.09

```
{
    "status": "fail",
    "code": 400,
    "comment": "slug empty"
}
```
Response: 400.10

```
{
    "status": "fail",
    "code": 400,
    "comment": "excerpt empty"
}
```
Response: 400.11

```
{
    "status": "fail",
    "code": 400,
    "comment": "og_title empty"
}
```
Response: 400.12

```
{
    "status": "fail",
    "code": 400,
    "comment": "og_decription empty"
}
```
Response: 400.013

```
{
    "status": "fail",
    "code": 400,
    "comment": "meta_title empty"
}
```
Response: 400.14

```
{
    "status": "fail",
    "code": 400,
    "comment": "meta_description empty"
}
```
Response: 400.15

```
{
    "status": "fail",
    "code": 400,
    "comment": "avatar_title empty"
}
```
Response: 400.16

```
{
    "status": "fail",
    "code": 400,
    "comment": "avatar_alt empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "account error"
}
```
Response: 422.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "password' length < 8"
}
```
Response: 422.04

```
{
    "status": "fail",
    "code": 422,
    "comment": "password / password_repeat is NOT equal"
}
```
Response: 422.05

```
{
    "status": "fail",
    "code": 422,
    "comment": "mobile_phone format error"
}
```
Response: 409.01

```
{
    "status": "fail",
    "code": 409,
    "comment": "account error"
}
```
Response: 409.02

```
{
    "status": "fail",
    "code": 409,
    "comment": "mobile_phone error"
}
```
Response: 422.06

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 409.03

```
{
    "status": "fail",
    "code": 409,
    "comment": "slug error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "create user error"
}
```
## <a name='17.-寄-忘記密碼通知信'></a> 17. 寄 忘記密碼通知信
[Back to top](#top)

<p>・ 寄 忘記密碼通知信</p>

	POST /user/password/notify

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  account | string | <p>使用者帳號</p>|

### Success Response

Response: 202

```
{
    "status": "success",
    "code": 202,
    "comment": "notify success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "account empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "account error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "user status error"
}
```
## <a name='10.-上傳使用者頭像'></a> 10. 上傳使用者頭像
[Back to top](#top)

<p>・ 上傳使用者頭像</p>

	POST /user/{id}/avatar

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>使用者 id</p>|
|  privacy_status | string | **optional**<p>隱私狀態</p>_Default value: public_<br>_Allowed values: public,private_|
|  status | string | **optional**<p>狀態</p>_Default value: disable_<br>_Allowed values: enable,disable,delete_|
|  file | file | <p>檔案</p>|
|  _method | string | <p>傳輸方法</p>_Allowed values: PUT_|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "upload avatar success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "filename": "1517067835270712",
        "avatar_links": {
            "c": "gallery/user_10335147731849216/avatar/1517067835270712_c.jpeg",
            "z": "gallery/user_10335147731849216/avatar/1517067835270712_z.jpeg",
            "n": "gallery/user_10335147731849216/avatar/1517067835270712_n.jpeg",
            "q": "gallery/user_10335147731849216/avatar/1517067835270712_q.jpeg",
            "sq": "gallery/user_10335147731849216/avatar/1517067835270712_sq.jpeg",
            "o": "gallery/user_10335147731849216/avatar/1517067835270712_o.jpeg"
        }
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.filename | string | <p>檔案名稱</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.avatar_links | string[] | <p>各尺寸圖檔連結</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "file empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "privacy_status error"
}
```
Response: 422.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 422.04

```
{
    "status": "fail",
    "code": 422,
    "comment": "file error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "owner error"
}
```
Response: 422.05

```
{
    "status": "fail",
    "code": 422,
    "comment": "owner status error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "create error"
}
```
## <a name='18.-重設密碼'></a> 18. 重設密碼
[Back to top](#top)

<p>・ 重設密碼</p>

	PUT /user/{id}/password/reset

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>使用者 ID</p>|
|  account | string | <p>使用者帳號</p>|
|  password | string | <p>密碼</p>|
|  password_repeat | string | <p>重設密碼</p>|
|  check_code | string | <p>驗證碼</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "reset success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "account empty"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "password / password_repeat empty"
}
```
Response: 400.05

```
{
    "status": "fail",
    "code": 400,
    "comment": "check_code empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "account error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "password\' length < 8"
}
```
Response: 422.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "password / password_repeat is NOT equal"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
```
Response: 422.04

```
{
    "status": "fail",
    "code": 422,
    "comment": "user status error"
}
```
Response: 422.05

```
{
    "status": "fail",
    "code": 422,
    "comment": "user account error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "user password check error"
}
```
Response: 422.06

```
{
    "status": "fail",
    "code": 422,
    "comment": "user check status error"
}
```
Response: 422.07

```
{
    "status": "fail",
    "code": 422,
    "comment": "user check code error"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "update check status error"
}
```
Response: 500.02

```
{
    "status": "fail",
    "code": 500,
    "comment": "update password error"
}
```
## <a name='02.-修改使用者'></a> 02. 修改使用者
[Back to top](#top)

<p>・ 修改使用者</p>

	PUT /user/{id}

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>使用者 ID</p>|
|  status | string | <p>狀態</p>_Allowed values: init,enable,delete,disable_|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "update success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "user_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.user_id | string | <p>使用者 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "updateData empty"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "update error"
}
```
## <a name='08.-修改使用者密碼'></a> 08. 修改使用者密碼
[Back to top](#top)

<p>・ 修改使用者密碼</p>

	PUT /user/{id}/password

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>使用者 ID</p>|
|  password | string | <p>密碼</p>|
|  password_repeat | string | <p>確認密碼</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "update success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "user_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.user_id | string | <p>使用者 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "password' length < 8"
}
```
Response: 422.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "password / password_repeat is NOT equal"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "updateData empty"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "update error"
}
```
## <a name='09.-修改使用者資料'></a> 09. 修改使用者資料
[Back to top](#top)

<p>・ 修改使用者資料</p>

	PUT /user/{id}/profile

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>使用者 ID</p>|
|  first_name | string | **optional**<p>名</p>|
|  last_name | string | **optional**<p>姓</p>|
|  nickname | string | **optional**<p>暱稱</p>|
|  email | string | **optional**<p>E-Mail</p>|
|  mobile_country_code | string | **optional**<p>手機號碼國碼</p>|
|  mobile_phone | string | **optional**<p>手機號碼</p>|
|  birth | string | **optional**<p>生日</p>|
|  bio | string | **optional**<p>自我介紹</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "update profile success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "user_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.user_id | string | <p>使用者 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "user profile error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "email error"
}
```
Response: 409.01

```
{
    "status": "fail",
    "code": 409,
    "comment": "mobile_phone error"
}
```
Response: 409.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "mobile_phone format error"
}
```
Response: 409.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "birth error"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "updateData empty"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "update error"
}
```
## <a name='12.-修改使用者角色'></a> 12. 修改使用者角色
[Back to top](#top)

<p>・ 修改使用者角色</p>

	PUT /user/{id}/role

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>使用者 ID</p>|
|  role_ids | string | <p>角色 IDs</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "update user role success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "user_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.user_id | string | <p>使用者 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "role_ids empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
```
Response: 400.05

```
{
    "status": "fail",
    "code": 400,
    "comment": "updateData empty"
}
```
## <a name='11.-修改使用者-seo'></a> 11. 修改使用者 SEO
[Back to top](#top)

<p>・ 修改使用者 SEO</p>

	PUT /user/{id}/seo

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|
| token | string | <p>Token</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>使用者 ID</p>|
|  slug | string | **optional**<p>位址</p>|
|  excerpt | string | **optional**<p>摘要</p>|
|  og_title | string | **optional**<p>og 標題</p>|
|  og_description | string | **optional**<p>og 描述</p>|
|  meta_title | string | **optional**<p>meta 標題</p>|
|  meta_description | string | **optional**<p>meta 描述</p>|
|  avatar_title | string | **optional**<p>頭像標題</p>|
|  avatar_alt | string | **optional**<p>頭像 alt</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "update profile success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "user_id": "36946497446744064"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.user_id | string | <p>使用者 ID</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "user seo error"
}
```
Response: 409.01

```
{
    "status": "fail",
    "code": 409,
    "comment": "slug error"
}
```
Response: 400.04

```
{
    "status": "fail",
    "code": 400,
    "comment": "updateData empty"
}
```
Response: 500.01

```
{
    "status": "fail",
    "code": 500,
    "comment": "update error"
}
```
## <a name='14.-驗證-e-mail'></a> 14. 驗證 E-Mail
[Back to top](#top)

<p>・ 驗證 E-Mail</p>

	PUT /user/{id}/email/verify

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>使用者 ID</p>|
|  check_code | string | <p>驗證碼</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "verify success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "check_code empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "user status error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "user email check error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "user check status error"
}
```
Response: 422.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "user check code error"
}
```
## <a name='16.-驗證-mobile'></a> 16. 驗證 Mobile
[Back to top](#top)

<p>・ 驗證 Mobile</p>

	PUT /user/{id}/mobile/verify

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| session | string | <p>Session 代碼</p>|


### Header Examples

Header

```
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
```



### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | string | <p>使用者 ID</p>|
|  check_code | string | <p>驗證碼</p>|

### Success Response

Response: 201

```
{
    "status": "success",
    "code": 201,
    "comment": "verify success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8"
    }
}
```

### Success

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  status | string | <p>回傳狀態</p>|
|  code | int | <p>回傳代碼</p>|
|  comment | string | <p>回傳訊息</p>|
|  data | object | <p>回傳資訊</p>|
| &nbsp;&nbsp;&nbsp;&nbsp; data.session | string | <p>Session 代碼</p>|

### Error Response

Response: 400.01

```
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
```
Response: 400.02

```
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
```
Response: 400.03

```
{
    "status": "fail",
    "code": 400,
    "comment": "check_code empty"
}
```
Response: 410.01

```
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
```
Response: 404.01

```
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
```
Response: 422.01

```
{
    "status": "fail",
    "code": 422,
    "comment": "user status error"
}
```
Response: 404.02

```
{
    "status": "fail",
    "code": 404,
    "comment": "user mobile check error"
}
```
Response: 422.02

```
{
    "status": "fail",
    "code": 422,
    "comment": "user check status error"
}
```
Response: 422.03

```
{
    "status": "fail",
    "code": 422,
    "comment": "user check code error"
}
```
